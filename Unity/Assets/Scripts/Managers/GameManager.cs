﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Assertions.Comparers;
using UnityEngine.PlayerLoop;
using UnityEngine.Rendering.UI;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.UIElements;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.UIElements;
#endif

public class GameManager : MonoSingleton<GameManager>
{
    [Tooltip("If -1, will start inside the SAS")] public int startingRoomIndex;
    public int currRoomIndex;
    public int preCheckedOutRoom { get; private set; }
    public int prevRoomIndex;
    [HideInInspector] public Dictionary<int, RoomManager> rooms;
    [HideInInspector] public RoomManager sasRoom;
    private bool paused = false;
    private bool leaving = false;
    public MenuNavigator menuNavigator;
    public RoomManager nextRoom
    {
        get {
            return rooms[ (currRoomIndex+1) % rooms.Count ];
        }
    }
    public RoomManager currentRoom
    {
        get {
            return rooms[ currRoomIndex ];
        }
    }
    public RoomManager previousRoom
    {
        get {
            return rooms[ (currRoomIndex == 0) ? rooms.Count-1 : currRoomIndex-1 ];
        }
    }


    private void Start()
    {
        Init();
    }

    private void Init()
    {
        FindRooms();
        //Finding the phone associated with the starting room
        preCheckedOutRoom = -1;
        currRoomIndex = startingRoomIndex;
        prevRoomIndex = (currRoomIndex == 0) ? rooms.Count-1 : currRoomIndex-1;//(currRoomIndex - 1) % rooms.Count;
        //Debug.Log(currRoomIndex + " "  + prevRoomIndex);
        SetupListeners();
        //Disabling unused door managers, optimization
        for (int i = 0; i < rooms.Count; ++i)
        {
            if (i != startingRoomIndex)
            {
                GameObject go = rooms[i].gameObject;
                go.SetActive(false);
                //StartCoroutine(DisableCo(go));
            }
        }
        SoundManager.instance.Init();
        EnterStartRoom();
        menuNavigator.UpdateLanguage();
    }

    private void Update()
    {
        if(paused)
        {
            UnityEngine.Cursor.lockState = CursorLockMode.None;
            UnityEngine.Cursor.visible = true;
        }
        else
        {
            UnityEngine.Cursor.lockState = CursorLockMode.Locked;
            UnityEngine.Cursor.visible = false;
        }
    }

    public void Escape()
    {
        if(running)
        {
            SoundManager.instance.Pause();
            paused = !paused;
            Time.timeScale = paused ? 0f : 1f;
            UnityEngine.Cursor.lockState = CursorLockMode.Confined;
            UnityEngine.Cursor.visible = false;
            menuNavigator.inMenu = paused;
            menuNavigator.gameObject.SetActive(paused);
        }
    }

    public void MainMenu()
    {
        LoadMenu(3f);
    }

    private IEnumerator DisableCo(GameObject go)
    {
        yield return new WaitForEndOfFrame();
        go.SetActive(false);
        yield return null;
    }
    
    private void OnValidate()
    {
        FindRooms();
    }

    private void FindRooms()
    {
        //Creating the room dictionary
        rooms = new Dictionary<int, RoomManager>();
        RoomManager[] roomManagers = FindObjectsOfType<RoomManager>();
        for (int i = 0; i < roomManagers.Length; ++i)
        {
            RoomManager curr = roomManagers[i];
            curr.Init();
            //Considering the room index of the room manager
            rooms.Add(curr.roomIndex, curr);
        }
        //Debug.Log("Nb rooms " + rooms.Count);
    }

    private void SetupListeners()
    {
        foreach (RoomManager room in rooms.Values)
        {
            //room.onTeleport.AddListener(OnRoomTeleport);
            if (room.doorManager)
            {
                room?.doorManager.onDoorInteracted.AddListener(OnDoorOpened);
                room?.doorManager.onDoorClose.AddListener(OnDoorClosed);
                room?.doorManager.onDoorPortalSend.AddListener(OnDoorPassThrough);
            }
            room?.onTeleport.AddListener(OnRoomTeleport);
        }
    }

    //Event triggered when a door is being used in any of the rooms
    private void OnDoorOpened(Door door)
    {
        //If the door is featuring a portal, its components must be enabled
        if (door.travelMode != eDOOR_TRAVELMODE.SimpleSelf && door.travelMode != eDOOR_TRAVELMODE.SimpleToNextRoom)
        {
            if (door.doorManager.loopRoom)
            {
                PreloadRoom(door.doorManager.loopRoom.roomManager.roomIndex);
            }
            else
            {
                switch (door.travelMode)
                {
                    case eDOOR_TRAVELMODE.PortalToNextRoom:
                        //Preloading the next room
                        PreloadRoom(nextRoom.roomIndex);
                        break;
                    case eDOOR_TRAVELMODE.PortalToOtherPortal:
                        break;
                    case eDOOR_TRAVELMODE.PortalToOtherRoom:
                        PreloadRoom(door.otherRoom.roomManager.roomIndex);
                        break;
                }
            }
        }
        else
        {
            nextRoom.PreloadLights(true);
        }
    }

    private void OnDoorPassThrough(Door entrance, Door exit)
    {
        //Debug.Log("Pass through");
        if (entrance.travelMode == eDOOR_TRAVELMODE.SimpleToNextRoom) {
            //TODO : Fix 4th room transition with doors (player may precheckout but go back)
            if(entrance && entrance.portal)
            {
                if(entrance.portal.sign > 0)
                {
                    PreCheckoutRoom(nextRoom.roomIndex);
                    //Debug.Log(preCheckedOutRoom);
                }
                else
                {
                    PreCheckoutRoom(entrance.doorManager.roomManager.roomIndex);
                    //Debug.Log(preCheckedOutRoom);
                }
            }
        } else {
            if (entrance.doorManager.roomManager.roomIndex != exit.doorManager.roomManager.roomIndex)
            {
                RoomManager roomReceiver = exit.doorManager.roomManager;
                PreCheckoutRoom(roomReceiver.roomIndex);
                entrance.doorManager.roomManager.DisablePhone();
                if (entrance.portal.portalType == ePORTAL.Void || entrance.portal.portalType == ePORTAL.VoidMyst)
                {
                    CheckoutRoom(roomReceiver.roomIndex);
                }
            }
        }
    }
    
    //Event triggered when a door is closing itself
    private void OnDoorClosed(Door door)
    {
        //Debug.Log(door.doorManager.name);
        if (door.travelMode == eDOOR_TRAVELMODE.SimpleToNextRoom)
        {
            //Checking if the door that just closed is part of the previous room, so it means the player entered the new room.
            if (door.doorManager.roomManager.roomIndex != preCheckedOutRoom) {
                //Debug.Log(door.doorManager.roomManager.roomIndex + " " + preCheckedOutRoom);
                CheckoutRoom(preCheckedOutRoom);
            }
        }
        else
        {
            //Debug.Log("Door closed");
            //Checking if the door that just closed is part of the pre-checked out room. Also, a model door that is closing is part of the room that the player comes from
            if (!door.isModel && door.doorManager.roomManager.roomIndex == preCheckedOutRoom) {
                //Debug.Log(door.doorManager.roomManager.roomIndex + " " + preCheckedOutRoom);
                CheckoutRoom(preCheckedOutRoom);
            }
        }
    }

    private void OnPortalSend(Portal emitter, Portal receiver)
    {
        
    }

    private void OnPortalReceive(Portal receiver, Portal emitter)
    {
        
    }
    
    public void OnRoomTeleport(int roomIndex)
    {
        
        PreCheckoutRoom(roomIndex);
        PreloadBuffer(roomIndex);
        CheckoutRoom(roomIndex);
    }

    
    public void AskForRoom(int roomIndex = -1)
    {
        //Debug.Log("Asking to enable " + roomIndex + "'s rooms");
        if (roomIndex < 0)
        {
            rooms[(currRoomIndex+1)%rooms.Count].gameObject.SetActive(true);
            //Debug.Log(rooms[(currRoomIndex+1)%rooms.Count].name + " enabling");
        }
        else
        {
            rooms[roomIndex].gameObject.SetActive(true);
            //Debug.Log(rooms[roomIndex].name + " enabling");
        }
    }

    public void EnterStartRoom()
    {
        //Moving controlled pawn to the room's spawn point
        if (rooms[startingRoomIndex].spawnPoint)
        {
            Pawn.controlledPawn.transform.position = rooms[startingRoomIndex].spawnPoint.transform.position;
        }
        PreloadRoom(startingRoomIndex);
        PreCheckoutRoom(startingRoomIndex);
        
        currentRoom.gameObject.SetActive(true);
        currentRoom.Checkout();
        
        prevRoomIndex = currRoomIndex;
        currRoomIndex = startingRoomIndex;
        
        //Debug.Log("<color=yellow>" + rooms[startingRoomIndex].name + "</color><color=green> room is checked out.</color> Exited " + previousRoom.name + " room.");
        //previousRoom.ExitRoom();

        //UnloadBuffer(currRoomIndex);
    }

    private void PreloadRoom(int roomIndex)
    {
        Debug.Log("Preload room " + rooms[roomIndex].name);
        PreloadBuffer(roomIndex);
        rooms[roomIndex].gameObject.SetActive(true);
        rooms[roomIndex].PreloadLights(true);
    }

    public void PreloadBuffer(int roomIndex)
    {
        //Preloading next room considering preload buffer
        if (rooms[roomIndex].preloadBuffer > 0)
        {
            int roomToPreload = roomIndex;
            for (int i = 0; i < rooms[roomIndex].preloadBuffer ; ++i)
            {
                roomToPreload = (roomToPreload + 1) % rooms.Count;
                rooms[roomToPreload].gameObject.SetActive(true);
                rooms[roomToPreload].OptimizeLights();
            }
        }
    }

    public void UnloadBuffer(int roomIndex)
    {
        //Preloading next room considering unload buffer
        if (rooms[roomIndex].unloadBuffer > 0)
        {
            int roomToUnload = roomIndex;
            for (int i = 0; i < rooms[roomIndex].unloadBuffer ; ++i)
            {
                roomToUnload = (roomToUnload == 0 ) ? rooms.Count -1 : roomToUnload -1;
                rooms[roomToUnload].gameObject.SetActive(false);
            }
        }
    }

    //Player is now TOTALLY inside the room (Called when door is closed)
    public void CheckoutRoom(int roomIndex)
    {
        if (roomIndex != currRoomIndex)
        {
            prevRoomIndex = currRoomIndex;
            currRoomIndex = roomIndex;
        
            Debug.Log("<color=yellow>" + rooms[roomIndex].name + "</color><color=green> room is checked out.</color> Exited " + rooms[prevRoomIndex].name + " room.");
            rooms[prevRoomIndex].ExitRoom();

            UnloadBuffer(currRoomIndex);
        
            currentRoom.gameObject.SetActive(true);
            currentRoom.Checkout();
        }
    }

    private bool running = true;

    public void LoadMenu(float duration)
    {
        if (running)
        {
            Time.timeScale = 1f;
            running = false;
            menuNavigator.gameObject.SetActive(false);
            Pawn.controlledPawn.BlockPawn();
            AkSoundEngine.WakeupFromSuspend();
            AkSoundEngine.StopAll();
            EffectManager.instance.FadeIn(duration).onComplete += () => SceneManager.LoadSceneAsync(0);
        }
    }

    //Player entered the room but can still leave it
    public void PreCheckoutRoom(int roomIndex)
    {
        if (roomIndex != preCheckedOutRoom)
        {
            Debug.Log("<color=yellow>" + rooms[roomIndex].name + "</color><color=green> room is </color><color=red>PRE</color><color=green>-checked out</color>");
            preCheckedOutRoom = roomIndex;
            rooms[preCheckedOutRoom].PreCheckout();
        }
    }
    
    public void CheckoutNextRoom(Door entranceDoor, Door exitDoor)
    {
        int nextRoomIndex = (currRoomIndex+1) % rooms.Count;
        CheckoutRoom(nextRoomIndex);
    }

    public void CheckoutPreviousRoom()
    {
        Debug.Log("Checking out previous room");
        CheckoutRoom(prevRoomIndex);
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(GameManager))]
public class EditorGameManager : Editor
{
    /*
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        GameManager script = (GameManager) target;
        
        foreach (PhoneHelper phoneHelper in script.phoneHelpers)
        {
            phoneHelper.leaveMessage = EditorGUILayout.Toggle("Does leave a message", phoneHelper.leaveMessage);
            if (phoneHelper.leaveMessage)
            {
                //phoneHelper.messageLeft = EditorGUILayout.ObjectField(phoneHelper.leaveMessage, typeof(AK.Wwise.Event), true);
                //script.doorManagers[i] = (DoorManager)EditorGUILayout.ObjectField(script.doorManagers[i], typeof(DoorManager), true);
            }
        }
    }*/
    
}
#endif
