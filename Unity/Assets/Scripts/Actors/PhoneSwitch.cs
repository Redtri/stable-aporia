﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoneSwitch : MonoBehaviour
{
    public List<PhoneMessage> messages;

    public void SwitchMessages(RoomManager phoneRoom)
    {
        for(int i = 0; i < phoneRoom.phoneHelper.messages.Count; ++i)
        {
            PhoneMessage oldMessage = phoneRoom.phoneHelper.messages[i];
            PhoneMessage newMessage = messages[i];

            phoneRoom.phoneHelper.messages[i] = newMessage;
        }
        phoneRoom.phone.currMessIndex = 0;
    }
}
