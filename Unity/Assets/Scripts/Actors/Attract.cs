﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.PlayerLoop;

[System.Serializable]
public class PawnEvent : UnityEvent<Pawn>{}

public class Attract : MonoBehaviour
{
    public Pawn pawn;
    public bool active;
    public float extraSize;
    private bool attracting;
    public PawnEvent onLookCatch;

    private void Start()
    {
        if (!pawn)
            pawn = Pawn.controlledPawn;
    }

    public static bool VisibleByCamera(Vector3 pos, float size, Camera camera)
    {
        Vector3 maxPos = camera.WorldToViewportPoint(pos + new Vector3(size, size, size)); 
        Vector3 minPos = camera.WorldToViewportPoint(pos - new Vector3(size, size, size));

        //Debug.Log(maxPos);
        
        return (maxPos.x >= 0f && maxPos.x <= 1f && maxPos.y >= 0f && maxPos.y <= 1f) ||
               (minPos.x >= 0f && minPos.x <= 1f && minPos.y >= 0f && minPos.y <= 1f);
    }

    public void Activate()
    {
        active = true;
    }

    public void Disable()
    {
        active = false;
        attracting = false;
        pawn.Attract(null);
    }
    
    private void FixedUpdate()
    {
        if (active && !attracting && VisibleByCamera(transform.position, extraSize, Pawn.controlledPawn.cam))
        {
            onLookCatch?.Invoke(pawn);
            AttractLook();
        }
    }
    
    public void AttractLook()
    {
        pawn.Attract(transform);
        attracting = true;
    }

    public void Stop()
    {
        pawn.Attract(null);
        Disable();
    }
}
