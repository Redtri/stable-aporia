﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using Random = System.Random;
#if UNITY_EDITOR
using UnityEditor;

#endif

public class DoorManager : MonoBehaviour
{
    [HideInInspector] public bool autoBind;
    [HideInInspector] public Door mainSasDoor;
    [HideInInspector] public List<Door> roomDoors;
    [HideInInspector] public List<Portal> portals;
    public bool loopingDoors;
    public DoorManager loopRoom;
    public int maxLoops;
    private int nbLoops;
    public Door overrideEntranceDoor;
    [HideInInspector] public DoorEvent onDoorInteracted;
    [HideInInspector] public DoorEvent onDoorClose;
    [HideInInspector] public RoomManager roomManager;
    public Door2Event onBeginSelfEntrance;
    public Door2Event onBeginExternalEntrance;
    public Door2Event onDoorPortalSend;
    public Door2Event onDoorPortalReceive;
    public PortalEvent onPortalSend;
    public PortalEvent onPortalReceive;
    public int maxExternalLoops;
    public UnityEvent onLoopReached;
    protected int externalLoops;
    private List<Door> knockingDoors;

    [HideInInspector] public Door doorToAvoid;
    private void OnEnable()
    {
        if (roomDoors != null)
        {
            foreach (Door door in roomDoors)
            {
                door.onSuccesInteract.AddListener(OnDoorUsed);
                door.onFinished.AddListener(OnDoorClosed);
                door.onPortalSend.AddListener(OnDoorPortalSend);
                door.onPortalReceive.AddListener(OnDoorPortalReceive);
                door.doorManager = this;
            }
            //Array.ForEach(SasManager.instance.sasDoors.ToArray(), x => x.onInteract += OnDoorUsed);
        }
    }
    private void OnDisable()
    {
        if (roomDoors != null)
        {
            foreach (Door door in roomDoors)
            {
                door.onSuccesInteract.RemoveListener(OnDoorUsed);
                door.onPortalSend.RemoveListener(OnDoorPortalSend);
                door.onPortalReceive.RemoveListener(OnDoorPortalReceive);
            }
        }
    }
    private void Awake()
    {
        roomManager = GetComponent<RoomManager>();
    }
    private void OnValidate()
    {
        if (autoBind)
        {
            BindDoors();
        }
    }
    void Start()
    {
        Init();
    }

    private void Init()
    {
        if(roomDoors == null)
            roomDoors = new List<Door>();
        nbLoops = 0;
    }

    public bool IsLooping()
    {
        return loopingDoors && nbLoops < maxLoops;
    }

    public void UnloopRoom()
    {
        loopingDoors = false;
        loopRoom = null;
    }

    //Asking entrance for the room, will return an entrance door
    public Door AskEntranceDoor(Door originDoor = null)
    {
        if (overrideEntranceDoor) {
            if(originDoor?.doorSide != overrideEntranceDoor.doorSide)
                return overrideEntranceDoor;
            
                //If the entrance door hasn't already been used, its side is changed to be compatible
                overrideEntranceDoor.SetSide((originDoor.doorSide == eDOOR_SIDE.Left) ? eDOOR_SIDE.Right : eDOOR_SIDE.Left);
                
                return overrideEntranceDoor;
        }

        Door returnDoor = GetCompatibleDoor(originDoor);

        return returnDoor;
    }

    public void PickKnockingDoors(int count)
    {
        knockingDoors = new List<Door>();
        if (count > roomDoors.Count)
            count = roomDoors.Count;
        for (int i = 0; i < count; ++i)
        {
            Door newDoor = roomDoors[UnityEngine.Random.Range(0, roomDoors.Count)];
            
            while(knockingDoors.Contains(newDoor))
                newDoor = roomDoors[UnityEngine.Random.Range(0, roomDoors.Count)];
            
            knockingDoors.Add(newDoor);
        }
    }

    public void KnockDoors(bool start)
    {
        foreach (Door door in knockingDoors)
        {
            door.Knock(start);
        }
    }
    
    public void OnDoorUsed(Interactable interactable)
    {
        //Debug.Log("Door used " + ((loopingDoors) ? " will loop into the same room " : " will lead the player to the SAS"));
        Door door = (Door) interactable;
        onDoorInteracted?.Invoke(door);
        Portal portal = null;

        if (roomDoors.Contains(door))
        {
            if (door.TryGetComponent(out portal))
            {
                if (!door.overrideLoop) {
                    if (loopingDoors && (nbLoops < maxLoops && roomDoors.Count > 1 || maxLoops == 0)) {
                        LoopInsideRoom(door);
                        return;
                    }
                    
                    if (loopRoom)
                    {
                        loopRoom.loopRoom = this;
                        Door otherDoor = loopRoom.AskEntranceDoor(door);

                        if (otherDoor.doorSide != door.doorSide)
                        {
                            //Starting travel
                            DoorTravel(door, otherDoor.portal.GetComponent<Door>());
                            if (externalLoops >= maxExternalLoops)
                            {
                                onLoopReached?.Invoke();
                            }
                            ++externalLoops;
                            ++otherDoor.doorManager.externalLoops;
                        } else {
                            Debug.LogError("You are trying to travel to a door using the same pivot side than the one you are coming from");
                        }

                        return;
                    }
                    
                }
                    switch (door.travelMode)
                    {
                        case eDOOR_TRAVELMODE.SimpleSelf:
                            DoorSimpleTravel(door);
                            break;
                        case eDOOR_TRAVELMODE.SimpleToNextRoom:
                            DoorSimpleTravel(door);
                            break;
                        case eDOOR_TRAVELMODE.PortalToOtherPortal:
                            //DIRECTLY TRAVELLING TO ANOTHER DOOR
                            Door otherDoor = door.portal.otherPortal.GetComponent<Door>();

                            if (otherDoor.doorSide != door.doorSide) {
                                //Starting travel
                                DoorTravel(door, door.portal.otherPortal.GetComponent<Door>());
                            } else {
                                Debug.LogError("You are trying to travel to a door using the same pivot side than the one you are coming from");
                            }
                            break;
                        case eDOOR_TRAVELMODE.PortalToNextRoom:
                            //The door is binded to a room, the entrance door is defined by the AskEntranceDoor function
                            //Debug.Log(GameManager.instance.nextRoom);
                            DoorTravel(door, GameManager.instance.nextRoom.doorManager.AskEntranceDoor(door));
                            break;
                        case eDOOR_TRAVELMODE.PortalToOtherRoom:
                            Door nextDoor = door.otherRoom.AskEntranceDoor(door);
                            if (nextDoor.doorSide != door.doorSide) {
                                DoorTravel(door, nextDoor);
                            } else {
                                Debug.LogError("You are trying to travel to a door using the same pivot side than the one you are coming from");
                            }
                            //Debug.LogError("Error trying to use PortalToOtherRoom feature, not implemented yet");
                            break;
                        default:
                            if (loopingDoors && nbLoops < maxLoops && roomDoors.Count > 1 || loopingDoors && maxLoops == 0) {
                                LoopInsideRoom(door);
                            }
                            break;
                    }
            }
        }
    }

    public void OnDoorClosed(Interactable interactable)
    {
        Door door = (Door) interactable;

        onDoorClose?.Invoke(door);
    }
    private void LoopInsideRoom(Door door)
    {
        Debug.Log("Looping inside the current room");
        //LOOPING INSIDE THE ROOM
        ++nbLoops;
        Door nextDoor = AskEntranceDoor(door);
        
        onBeginSelfEntrance?.Invoke(door, nextDoor);
        //Starting travel
        DoorTravel(door, nextDoor);
    }

    public void OnDoorPortalSend(Door emitter, Door receiver)
    {
        //Debug.Log("Send door manager");
        onDoorPortalSend?.Invoke(emitter, receiver);
    }
    public void OnDoorPortalReceive(Door receiver, Door emitter)
    {
        //Debug.Log("Receive door manager");
        onDoorPortalReceive?.Invoke(receiver, emitter);
        //roomManager.TeleportInto();
    }

    private void OnPortalSend(Portal emitter, Portal receiver)
    {
        
    }

    private void OnPortalReceive(Portal receiver, Portal emitter)
    {
        
    }

    public void WhisperDoors()
    {
        foreach (Door door in roomDoors)
        {
            door.Breath();
        }
    }
    
    public void FlipDoor(Interactable interactable)
    {
        Door door = (Door) interactable;
        Portal portal = door.GetComponent<Portal>();
        
        if(!door.trapped)
            door.transform.Rotate(door.transform.up * 180f);
        door.doorParent.gameObject.SetActive(true);
        portal.Activate(false);
        door.onFinished.RemoveListener(FlipDoor);
    }

    public void SimpleFlipDoor(Interactable interactable)
    {
        Door door = (Door) interactable;
        if (door.portal)
        {
            door.portal.enabled = true;
            door.portal.portalScreen.gameObject.SetActive(true);
        }
        if(!door.trapped)
            door.transform.Rotate(door.transform.up * 180f);
        
        door.onFinished.RemoveListener(FlipDoor);
    }

    public void BindDoors()
    {
        roomDoors = new List<Door>();
        portals = new List<Portal>();
        foreach (Portal portal in GetComponentsInChildren<Portal>())
        {
            if(portal.GetComponent<Door>())
                roomDoors.Add(portal.GetComponent<Door>());
            else
                portals.Add(portal);
        }
    }

    private void DoorTravel(Door doorUsed, Door nextDoor)
    {
        Portal portalUsed = doorUsed.GetComponent<Portal>();
        Portal nextDoorPortal = nextDoor.GetComponent<Portal>();

        //Setting the target of the selected door to the next door
        portalUsed.otherPortal = nextDoorPortal;
        nextDoorPortal.otherPortal = portalUsed;
        
        //nextDoorPortal.transform.Rotate(portalUsed.otherPortal.transform.up * 180f);
        //nextDoor.doorMesh.gameObject.SetActive(false);
        doorUsed.replicateDoor = nextDoor;
        
        if(portalUsed && nextDoorPortal && portalUsed.portalType != ePORTAL.Void && nextDoorPortal.portalType != ePORTAL.Void)
            doorUsed.onFinished.AddListener(OnOver);
        portalUsed.Activate(true);
        nextDoorPortal.Activate(true);
    }

    private void OnOver(Interactable interactable)
    {
        Door door = (Door) interactable;

        Portal portal = door.GetComponent<Portal>();
        
        portal.otherPortal.Activate(false);
        portal.Activate(false);
        
        door.onFinished.RemoveListener(OnOver);
    }

    private void DoorSimpleTravel(Door doorUsed)
    {
        if (doorUsed.portal)
        {
            doorUsed.portal.Activate(false);
        }
    }

    public void ChangeTargetRoom(Door targetRoom)
    {
        
    }
    
    public void Unloop()
    {
        loopingDoors = false;
    }

    public void FlipDoors(Door except)
    {
        Debug.Log("Flipping doors");
        except?.transform.Rotate(except.transform.up * 180f);
        foreach (Door door in roomDoors)
        {
            door.transform.Rotate(except.transform.up * 180f);
        }
    }

    public void LockDoors()
    {
        foreach (Door door in roomDoors)
        {
            door.Lock();
        }
    }
    
    public void LockDoors(Door exception = null)
    {
        foreach (Door door in roomDoors)
        {
            if(!exception || (exception && exception != door))
                door.Lock();
        }
    }

    public void UnlockDoors()
    {
        foreach (Door door in roomDoors)
        {
            door.Lock(true);
        }
    }
    
    public void AddDoor(Door newDoor)
    {
        if (!roomDoors.Contains(newDoor))
        {
            roomDoors.Add(newDoor);
            newDoor.onSuccesInteract.AddListener(OnDoorUsed);
            newDoor.onFinished.AddListener(OnDoorClosed);
            newDoor.onPortalSend.AddListener(OnDoorPortalSend);
            newDoor.onPortalReceive.AddListener(OnDoorPortalReceive);
            newDoor.doorManager = this;
        }
    }

    public Door GetCompatibleDoor(Door door)
    {
        //If there is a compatible door for the one that is entered in this door manager
        int maxCount = 0;
        if (roomDoors.Any(x => x.doorSide != door.doorSide && x != doorToAvoid))
        {
            List<Door> doors = new List<Door>(roomDoors);
            Door attemptDoor = doors[UnityEngine.Random.Range(0, doors.Count)];

            //Searching the a door with the opposite side
            while(attemptDoor.doorSide == door.doorSide || attemptDoor == doorToAvoid || door == attemptDoor || maxCount > 20) {
                doors.Remove(attemptDoor);
                attemptDoor = doors[UnityEngine.Random.Range(0, doors.Count)];
                ++maxCount;
            }

            return attemptDoor;
        }
        Debug.LogError("No compatible door found");

        return null;
    }
}

#if UNITY_EDITOR

[CustomEditor(typeof(DoorManager))]
public class EditorDoorManager : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
     
        DoorManager script = (DoorManager)target;
        
        script.autoBind = EditorGUILayout.Toggle("Autobind", script.autoBind);
        if (!script.autoBind) // if bool is true, show other fields
        {
            int newCount = Mathf.Max(0, EditorGUILayout.IntField("size", script.roomDoors.Count));
            while (newCount < script.roomDoors.Count)
                script.roomDoors.RemoveAt( script.roomDoors.Count - 1 );
            while (newCount > script.roomDoors.Count)
                script.roomDoors.Add(null);
 
            for(int i = 0; i < script.roomDoors.Count; i++)
            {
                script.roomDoors[i] = (Door)EditorGUILayout.ObjectField(script.roomDoors[i], typeof(Door), true);
            }
            script.mainSasDoor = EditorGUILayout.ObjectField("Main Sas Door", script.mainSasDoor, typeof(Door), true) as Door;
        
            if(GUILayout.Button("Rebind"))
            {
                script.BindDoors();
            }
        }
    }
}
#endif