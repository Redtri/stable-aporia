﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

[System.Serializable]
public class FurnitureInfos
{
    public GameObject gameObject;
    public Vector3 startOffset;

    public FurnitureInfos(GameObject go, Vector3 centerPos)
    {
        gameObject = go;
        startOffset = gameObject.transform.position - centerPos;
    }
}
public class Furniture : MonoBehaviour
{
    public Vector3 startOffset;
    public Vector3 danceRotation;
    public Vector3 danceAxis = Vector3.one;
    private bool dancing;
    private bool danced;
    private Vector3 startRotation;
    public float danceSpeed = 3f;

    private void Awake()
    {
        startRotation = transform.localRotation.eulerAngles;
    }

    public void Dance()
    {
        if (!dancing) {
            dancing = true;
            if (!danced) {
                danced = true;
                transform.DORotate(transform.localRotation.eulerAngles + danceRotation, 1f).onComplete += () => StartCoroutine(DanceCo());
            }
            else {
                StartCoroutine(DanceCo());
            }
        }
    }

    public void StopDancing()
    {
        dancing = false;
    }

    private IEnumerator DanceCo()
    {
        while (dancing) {
            transform.Rotate(danceAxis * Time.deltaTime * danceSpeed);
            yield return new WaitForEndOfFrame();
        }

        float stopDanceTime = Time.time;

        while (Time.time - stopDanceTime < 2f) {
            transform.Rotate(danceAxis * danceSpeed * Time.deltaTime * (1f - ((Time.time - stopDanceTime) / 2f)));
            yield return new WaitForEndOfFrame();
        }

        yield return null;
    }
}
