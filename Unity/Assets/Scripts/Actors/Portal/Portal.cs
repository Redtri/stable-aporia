﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
//using UnityEngine.Rendering.Universal;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.VFX;

public enum ePORTAL
{
    Standard, Entrance, Void, VoidMyst, Trapped
}

[System.Serializable]
public class PortalEvent : UnityEvent<Portal, Portal> {}

public class Portal : MonoBehaviour
{
    [Header("PARAMETERS")]
    public Camera cam;
    public MeshRenderer portalScreen;
    public int camResDivision = 2;

    public ePORTAL portalType;
    
    public bool flipCam;
    private bool activated;

    public MaterialArray materialArrayUnlit;
    public MaterialArray materialArrayLit;

    public bool lit;
    
    public bool clipProtection = true;
    
    [Header("COMPONENTS")]
    public Portal otherPortal;
    public Transform flipped;
    public Transform blackHoleParent;
    public VisualEffectAsset blackHoleVisualEffect;
    public VisualEffect blackHoleInstanceVFX;
    public BoxCollider trigger;

    [HideInInspector] public HDAdditionalCameraData camData;
    private Camera mainCam;
    private RenderTexture camRenderTexture;
    private Vector3 startLocalPos;
    private Vector3 startLocalScale;
    public TravelEntity traveller;
    public PortalEvent onSend;
    public PortalEvent onReceive;

    public EffectProfile blackholeProfile;
    public EffectProfile noBlackholeProfile;

    public AK.Wwise.Event blackholeEvent;
    [HideInInspector] public int sign;

    [HideInInspector] public Door door;

    void Start()
    {
        Init();
    }

    private void Init()
    {
        if(!materialArrayLit)
            materialArrayLit = Resources.Load<MaterialArray>("PortalLit");
        
        if(!materialArrayUnlit)
            materialArrayUnlit = Resources.Load<MaterialArray>("PortalUnlit");
        mainCam = Pawn.controlledPawn.cam;
        startLocalPos = portalScreen.transform.localPosition;
        startLocalScale = portalScreen.transform.localScale;
        door = GetComponent<Door>();
        if (door && door.trapped)
            portalType = ePORTAL.Trapped;
        RefreshType();
    }

    private void OnDestroy()
    {
        cam.targetTexture?.Release();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        UpdateCam();
        if(clipProtection)
            ProtectScreenFromClipping(mainCam.transform.position);
    }


    protected virtual void LateUpdate()
    {
        if (traveller)
        {
            Transform entityT = traveller.transform;

            Vector3 newOffset = entityT.position - transform.position;

            int oldSign = Math.Sign(Vector3.Dot(traveller.prevOffsetToPortal, transform.forward));
            int newSign = Math.Sign(Vector3.Dot(newOffset, transform.forward));

            if (oldSign != newSign)
            {
                sign = newSign;
                if (otherPortal)
                {
                    if(portalType == ePORTAL.Void) {
                        blackholeEvent?.Post(gameObject);
                        otherPortal.blackholeEvent?.Post(otherPortal.gameObject);
                    }
                    if (portalType != ePORTAL.Standard) {

                        if (otherPortal.door && otherPortal.portalType != ePORTAL.Void) {
                            otherPortal.door.Reactive(.15f);
                        }
                        EffectManager.instance.FadeOut(2f);
                        PostProcessManager.instance.Effect(noBlackholeProfile);
                    }
                    //Debug.Log("Old was : " + oldSign + ". New is " + newSign);
                    Matrix4x4 mat = (flipCam ? otherPortal.flipped.localToWorldMatrix : otherPortal.transform.localToWorldMatrix) * transform.worldToLocalMatrix * entityT.localToWorldMatrix;
                    traveller.Teleport(transform, otherPortal.transform, mat.GetColumn(3), mat.rotation);
                    

                    otherPortal.OnTravellerEnter(traveller);
                }

                if(door)
                {
                    if(door.travelMode != eDOOR_TRAVELMODE.SimpleToNextRoom)
                    {
                        traveller = null;
                    }
                }
                else
                {
                    traveller = null;
                }
                onSend?.Invoke(this, otherPortal);
                otherPortal?.onReceive?.Invoke(otherPortal, this);
                //StartCoroutine(Activate());
            }

            if(traveller)
                traveller.prevOffsetToPortal = newOffset;
        }
    }

    public bool HasTraveller(Transform trsf, float dist)
    {
        return traveller || (transform.position -trsf.position).magnitude <= dist;
    }

    protected IEnumerator Activate()
    {
        otherPortal.portalScreen.gameObject.SetActive(false);
        yield return new WaitForEndOfFrame();
        otherPortal.portalScreen.gameObject.SetActive(true);
        
        yield return null;
    }

    static bool VisibleFromCamera(Renderer renderer, Camera cam)
    {
        Plane[] frustumPlanes = GeometryUtility.CalculateFrustumPlanes(cam);
        return GeometryUtility.TestPlanesAABB(frustumPlanes, renderer.bounds);
    }
    
    protected void UpdateCam()
    {
        if (otherPortal)
        {
            bool visible = VisibleFromCamera(otherPortal.portalScreen, mainCam);
            if (visible)
            {
                if (otherPortal.portalType == ePORTAL.Standard || otherPortal.portalType == ePORTAL.Entrance)
                {
                    if (!otherPortal.door || (otherPortal.door && otherPortal.door.doorState != eDOOR_STATE.CLOSED))
                    {
                        if (!camRenderTexture || camRenderTexture.width != Screen.width/3 || camRenderTexture.height != Screen.height/3)
                        {
                            if (camRenderTexture != null)
                            {
                                camRenderTexture.Release();
                            }
                            camRenderTexture = new RenderTexture(Screen.width/camResDivision, Screen.height/camResDivision, 16);
                            cam.targetTexture = camRenderTexture;
                        }
            
                        if(!cam.gameObject.activeInHierarchy)
                            cam.gameObject.SetActive(true);
                        Matrix4x4 mat = transform.localToWorldMatrix  * ((flipCam) ? otherPortal.flipped.worldToLocalMatrix : otherPortal.transform.worldToLocalMatrix) * mainCam.transform.localToWorldMatrix;
                        cam.transform.SetPositionAndRotation(mat.GetColumn(3), mat.rotation);
                        BindCamTexture();
                    }
                    else
                    {
                        if(cam.gameObject.activeInHierarchy)
                            cam.gameObject.SetActive(false);
                    }
                }
            }
            else
            {
                if (otherPortal != null || portalType == ePORTAL.Trapped)
                {
                    if(cam.gameObject.activeInHierarchy)
                        cam.gameObject.SetActive(false);
                }
            }
        }
    }

    // Sets the thickness of the portal screen so as not to clip with camera near plane when player goes through
    float ProtectScreenFromClipping (Vector3 viewPoint) {
        float halfHeight = mainCam.nearClipPlane * Mathf.Tan (mainCam.fieldOfView * 0.5f * Mathf.Deg2Rad);
        float halfWidth = halfHeight * mainCam.aspect;
        float dstToNearClipPlaneCorner = new Vector3 (halfWidth, halfHeight, mainCam.nearClipPlane).magnitude;
        
        Transform screenT = portalScreen.transform;
        bool camFacingSameDirAsPortal = Vector3.Dot (transform.forward, transform.position - viewPoint) > 0;
        screenT.localScale = new Vector3 (screenT.localScale.x, screenT.localScale.y, dstToNearClipPlaneCorner);
        screenT.localPosition = startLocalPos + Vector3.forward * dstToNearClipPlaneCorner * ((camFacingSameDirAsPortal) ? 0.5f : -0.5f);
        return dstToNearClipPlaneCorner;
    }

    public void SetOtherPortal(Portal newPortal)
    {
        otherPortal = newPortal;
    }

    public void Flip()
    {
        flipCam = !flipCam;
    }

    public virtual void OnTravellerEnter(TravelEntity entity)
    {
        if (!traveller)
        {
            traveller = entity;
            entity.prevOffsetToPortal = entity.transform.position - transform.position;
            if (portalType == ePORTAL.Void)
            {
                //traveller.GetComponent<Pawn>().VertigoOut(true);
                PostProcessManager.instance.Effect(blackholeProfile);
                if(door)
                    door.onSuccesInteract?.Invoke(door);
            }
        }
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        TravelEntity entity = other.GetComponent<TravelEntity>();
        if (entity != null) {
            entity.GetComponent<Pawn>().portaled = true;
            OnTravellerEnter(entity);
        }
    }

    protected virtual void OnTriggerExit(Collider other)
    {
        TravelEntity entity = other.GetComponent<TravelEntity>();

        if (entity && traveller == entity)  {
            if (portalType == ePORTAL.Void)
            {
                PostProcessManager.instance.Effect(noBlackholeProfile);
            }
            traveller.GetComponent<Pawn>().portaled = false;
            traveller = null;
        }
    }


    private void BindCamTexture()
    {
        //Rendering the view form the other portal on the mesh
        if (otherPortal)
        {
            otherPortal.portalScreen.material.SetTexture("CameraTexture", camRenderTexture);
        }
    }

    public void RefreshType()
    {
        if (lit)
            portalScreen.material = materialArrayLit.materials[(int)portalType];
        else
            portalScreen.material = materialArrayUnlit.materials[(int)portalType];
        
        switch (portalType)
        {
            case ePORTAL.Standard:
                if(blackHoleInstanceVFX != null)
                    blackHoleInstanceVFX.Stop();
                if(trigger)
                    trigger.size = Vector3.one;
                break;
            case ePORTAL.Entrance:
                //Debug.Log(materialArrayLit.materials[(int) portalType].name);
                //portalScreen.material.SetFloat("Percent", 0f);
                break;
            case ePORTAL.Void:
                if(trigger)
                    trigger.size = Vector3.one * 3f;
                CommonVoid();
                
                door.doorParent.gameObject.SetActive(false);
                break;
            case ePORTAL.VoidMyst:
                //if(trigger)
                //trigger.size = Vector3.one * 3f;
                CommonVoid();
                    
                break;
            case ePORTAL.Trapped:
                break;
        }
    }

    public void Activate(bool activate)
    {
        activated = activate;
        if (!activate)
        {
            otherPortal = null;
        }
        
        switch (portalType)
        {
            case ePORTAL.Standard:
                cam.gameObject.SetActive(activated);
                portalScreen.gameObject.SetActive(activate);
                door?.doorParent.gameObject.SetActive(true);
                break;
            case ePORTAL.Void:
                CommonVoid();
                
                blackholeEvent?.Post(gameObject);
                break;
            case ePORTAL.VoidMyst:
                CommonVoid();
                
                break;
        }
    }

    public void SetLit(bool isUnlit)
    {
        lit = !isUnlit;
        RefreshType();
    }

    private void CommonVoid()
    {
        if (door)
        {
            door.doorParent.gameObject.SetActive(false);
        }
        portalScreen.gameObject.SetActive(true);
        cam.gameObject.SetActive(false);
        
        if (!blackHoleInstanceVFX && blackHoleParent)
        {
            blackHoleInstanceVFX = blackHoleParent.gameObject.AddComponent<VisualEffect>();
            //blackHoleInstanceVFX.SetInt("_TransparentSortPriority", 5);
            blackHoleInstanceVFX.visualEffectAsset = blackHoleVisualEffect;
        }
        blackHoleInstanceVFX.SendEvent("Play");
    }
}
