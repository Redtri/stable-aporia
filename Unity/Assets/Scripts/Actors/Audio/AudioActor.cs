﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class AudioEvent
{
    public AK.Wwise.Event wwEvent;
    public float delay;
    public bool triggerOnce;
    public UnityEvent onEventCallback;
    [HideInInspector] public bool alreadyTriggered = false;

    public void Play(GameObject go, AkCallbackManager.EventCallback callback = null)
    {
        if (!triggerOnce || !alreadyTriggered)
        {
            if (!SoundManager.instance.EventPlaying(wwEvent.Id, go))
            {
                if(!go.GetComponent<AkGameObj>()) {
                    go.AddComponent<AkGameObj>();
                }
                alreadyTriggered = true;
                if (onEventCallback.GetPersistentEventCount() > 0)
                {
                    if (callback == null)
                    {
                        callback = AudioActor.EventCallback;
                    }
                    DialogsManager.instance.NotifyWwiseEvent(wwEvent);
                    wwEvent.Post(go, (uint)AkCallbackType.AK_Duration | (uint)AkCallbackType.AK_Marker, callback, this);
                }
                else
                {
                    DialogsManager.instance.NotifyWwiseEvent(wwEvent);
                    wwEvent.Post(go);
                }
            }
        }
    }
}

public class AudioActor : MonoBehaviour
{
    public AudioEvent[] startEvents;
    public AudioEvent[] stdEvents;
    
    void OnEnable()
    {
        PlayStart();
    }

    private void OnDisable()
    {
        Stop();
    }

    public void PlayStart()
    {
        foreach (AudioEvent audioEvent in startEvents)
        {
            StartCoroutine(EventCo(audioEvent));
        }
    }

    public void PlayStd()
    {
        foreach (AudioEvent audioEvent in stdEvents)
        {
            StartCoroutine(EventCo(audioEvent));
        }
    }
    
    private IEnumerator EventCo(AudioEvent audioEvent)
    {
        yield return new WaitForSeconds(audioEvent.delay);
        audioEvent.Play(gameObject, EventCallback);
        yield return null;
    }
    
    public static void  EventCallback(object in_cookie, AkCallbackType in_type, object in_info)
    {
        AudioEvent audioEvent = in_cookie as AudioEvent;
        audioEvent.onEventCallback?.Invoke();
        if(in_type == AkCallbackType.AK_Duration)
        {
            AkDurationCallbackInfo info = (AkDurationCallbackInfo)in_info;
            DialogsManager.instance.NotifyMorphEvent(audioEvent.wwEvent, info.fDuration);
        }
    }

    public void PostEvent(int index)
    {
        StartCoroutine(EventCo(stdEvents[index]));
    }

    public void StopEvent(int index)
    {
        stdEvents[index].wwEvent.Stop(gameObject);
    }

    public void Stop()
    {
        foreach (AudioEvent audioEvent in startEvents)
        {
            if(SoundManager.instance.EventPlaying(audioEvent.wwEvent.Id, gameObject))
                audioEvent.wwEvent.Stop(gameObject);
        }

        foreach (AudioEvent audioEvent in stdEvents)
        {
            if(SoundManager.instance.EventPlaying(audioEvent.wwEvent.Id, gameObject))
                audioEvent.wwEvent.Stop(gameObject);
        }
    }

    public void FadeStop()
    {
        foreach (AudioEvent audioEvent in stdEvents)
        {
            audioEvent.wwEvent.Stop(gameObject, 3000, AkCurveInterpolation.AkCurveInterpolation_Exp1);
        }
        foreach (AudioEvent audioEvent in startEvents)
        {
            audioEvent.wwEvent.Stop(gameObject, 3000, AkCurveInterpolation.AkCurveInterpolation_Exp1);
        }
    }
}
