﻿using AK.Wwise;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Json;
using UnityEngine;

public class Rubix : MonoBehaviour
{
    public Vector3 scaleMin;
    public Vector3 scaleMax;
    public Vector2 duration;
    public AK.Wwise.Event brickEvent;
    public void Trigger()
    {
        for(int i = 0; i < transform.childCount; ++i) {
            if(duration.y > 0.0f) {
                transform.GetChild(i).DOScale(Vector3.Lerp(scaleMin, scaleMax, Random.Range(0f, 1f)), Random.Range(duration.x, duration.y));
                brickEvent?.Post(transform.GetChild(i).gameObject);
            }
            else {
                transform.GetChild(i).localScale = Vector3.Lerp(scaleMin, scaleMax, Random.Range(0f, 1f));
            }
        }
    }
}
