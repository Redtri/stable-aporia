﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimEventHandler : MonoBehaviour
{
    public UnityEvent markerEvent;

    public void Awake()
    {
        
    }

    public void InvokeEvent()
    {
        markerEvent?.Invoke();
    }
}
