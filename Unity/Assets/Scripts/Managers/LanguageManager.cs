﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum eLanguage { English, French }

public static class LanguageManager
{
    public static eLanguage stLanguage;

    public static UnityEvent onLanguageUpdate;

    static LanguageManager()
    {
        onLanguageUpdate = new UnityEvent();
    }

    public static void UpdateLanguage(eLanguage newLanguage)
    {
        stLanguage = newLanguage;
        onLanguageUpdate?.Invoke();
    }
}

public static class InputDevice
{
    public static bool usingKeyboard;
    public static UnityEvent onInputUpdate;

    static InputDevice()
    {
        onInputUpdate = new UnityEvent();
    }

    public static void UpdateDevice(bool keyboard)
    {
        usingKeyboard = keyboard;
        onInputUpdate?.Invoke();
    }
}
