﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEditor;
using UnityEngine;

public class AttractPortal : Portal
{
    public AnimationCurve speedUpCurve;
    public float baseAttractForce;
    public bool slowingEntity;
    private float triggerEnterDistance;
    [Range(0f, 1f)] public float percentToBlockEntity;
    private bool blockEntity;
    [Range(0f, 1f)] public float percentToOpenDoor;
    private bool openDoor;
    [Range(0f, 1f)] public float percentToFadeScreen;
    private bool fadeScreen;
    [Range(0f, 1f)] public float percentToRetrieve;
    private bool retrieving;
    public Transform retrieveTarget;
    public EffectProfile cutGlitch;
    public EffectProfile glitch;
    public bool useForce = true;
    public bool active = true;
    public AK.Wwise.RTPC rtpcDistance;
    protected override void Update()
    {
        UpdateCam();
    }

    public void Enable()
    {
        active = true;
    }
    
    public void Disable()
    {
        active = false;
    }

    protected override void LateUpdate()
    {
        if (traveller)
        {
            Transform entityT = traveller.transform;

            Vector3 newOffset = entityT.position - transform.position;

            int oldSign = Math.Sign(Vector3.Dot(traveller.prevOffsetToPortal, transform.forward));
            int newSign = Math.Sign(Vector3.Dot(newOffset, transform.forward));
        
            if (oldSign != newSign)
            {
                if (otherPortal)
                {
                    //Debug.Log("Old was : " + oldSign + ". New is " + newSign);
                    Matrix4x4 mat = (flipCam ? otherPortal.flipped.localToWorldMatrix : otherPortal.transform.localToWorldMatrix) * transform.worldToLocalMatrix * entityT.localToWorldMatrix;
                    traveller.Teleport(transform, otherPortal.transform, mat.GetColumn(3), mat.rotation);
            
                    otherPortal.OnTravellerEnter(traveller);
                }

                if (useForce)
                    traveller.GetComponent<Rigidbody>().velocity = (-otherPortal.transform.forward) * (traveller.GetComponent<Rigidbody>().velocity.magnitude / 2f);
                else
                {
                    traveller.GetComponent<Rigidbody>().DOMove(otherPortal.transform.position-otherPortal.transform.forward, 1f);
                }

                traveller = null;
                onSend?.Invoke(this, otherPortal);
                otherPortal?.onReceive?.Invoke(otherPortal, this);
                //StartCoroutine(Activate());
            }else
                traveller.prevOffsetToPortal = newOffset;
        }
    }

    public void RetrieveEntityForTransition()
    {
        if (traveller)
        {
            //traveller.transform.position 
            traveller.GetComponent<Pawn>().MoveForward(1f, 2f, false);
            //traveller.GetComponent<Pawn>().blackScreen.DOFade(0f, 1f);
        }
    }

    protected void FixedUpdate()
    {
        if (traveller && active)
        {
            float percent = (transform.position - traveller.transform.position).magnitude / triggerEnterDistance;
            float forceMultiplier = speedUpCurve.Evaluate(1f- percent);

            if (rtpcDistance.IsValid())
            {
                rtpcDistance.SetGlobalValue((1f - percent) * 100f);
            }
            
            if(percentToOpenDoor > 0f && !openDoor){
                if (percent < percentToOpenDoor)
                {
                    openDoor = true;
                    GetComponent<Door>().Interact(traveller.GetComponent<Pawn>(), eINTERACT_TYPE.Manual);
                }
            }
            
            if (percentToBlockEntity > 0f && !blockEntity)
            {
                if (percent < percentToBlockEntity)
                {
                    blockEntity = true;
                    traveller.GetComponent<Pawn>().BlockPawn();
                }
            }

            if (percentToFadeScreen > 0f && !fadeScreen)
            {
                if (percent < percentToFadeScreen)
                {
                    fadeScreen = true;
                    EffectManager.instance.CutGlitch(cutGlitch);
                    PostProcessManager.instance.Effect(glitch);
                }
            }

            if (percentToRetrieve > 0f)
            {
                if(!retrieving)
                {
                    if (percent < percentToRetrieve)
                    {
                        retrieving = true;
                        RetrieveEntityForTransition();
                        Collider[] colliders = GetComponents<Collider>();
                        foreach (Collider col in colliders)
                        {
                            if(!col.isTrigger)
                                Destroy(col);
                        }
                    }
                    
                    if(useForce)
                        traveller.GetComponent<Rigidbody>().AddForce((transform.position - traveller.transform.position).normalized * forceMultiplier * baseAttractForce);
                    else
                    {
                        //EffectManager.instance.UpdateVertigoOut(1f-percent);
                        Vector3 vel = traveller.transform.position + ((transform.position+transform.forward*5f) - traveller.transform.position).normalized * Time.fixedDeltaTime * forceMultiplier * baseAttractForce;

                        Vector3 directionLine = ((transform.position - transform.forward * triggerEnterDistance) - transform.position).normalized;
                        Debug.DrawLine((transform.position - transform.forward * triggerEnterDistance), (transform.position - transform.forward * triggerEnterDistance) + Vector3.up * 10f, Color.magenta);
                        Debug.DrawLine(transform.transform.position, transform.transform.position + Vector3.up * 10f, Color.magenta);
                        Vector3 pointOnLine = ((transform.position - transform.forward * triggerEnterDistance) + ((percent) * directionLine));
                        vel.z = 0f;
                        pointOnLine.x = traveller.transform.position.x;
                        pointOnLine.y = traveller.transform.position.y;
                
                        traveller.GetComponent<Rigidbody>().MovePosition(vel + new Vector3(0f, 0f, Mathf.Lerp(traveller.transform.position.z, pointOnLine.z, 0.1f)));

                        Debug.DrawLine(pointOnLine, pointOnLine + Vector3.up * 10f, Color.magenta);
                
                    }
                }
            }
            else
            {
                if(useForce)
                    traveller.GetComponent<Rigidbody>().AddForce((transform.position - traveller.transform.position).normalized * forceMultiplier * baseAttractForce);
                else
                {
                    //EffectManager.instance.UpdateVertigoOut(1f-percent);
                    Vector3 vel = traveller.transform.position + (transform.position - traveller.transform.position).normalized * Time.fixedDeltaTime * forceMultiplier * baseAttractForce;

                    Vector3 directionLine = ((transform.position - transform.forward * triggerEnterDistance) - transform.position).normalized;
                    Debug.DrawLine((transform.position - transform.forward * triggerEnterDistance), (transform.position - transform.forward * triggerEnterDistance) + Vector3.up * 10f, Color.magenta);
                    Debug.DrawLine(transform.transform.position, transform.transform.position + Vector3.up * 10f, Color.magenta);
                    Vector3 pointOnLine = ((transform.position - transform.forward * triggerEnterDistance) + ((percent) * directionLine));
                    vel.z = 0f;
                    pointOnLine.x = traveller.transform.position.x;
                    pointOnLine.y = traveller.transform.position.y;
                
                    traveller.GetComponent<Rigidbody>().MovePosition(vel + new Vector3(0f, 0f, Mathf.Lerp(traveller.transform.position.z, pointOnLine.z, 0.1f)));

                    Debug.DrawLine(pointOnLine, pointOnLine + Vector3.up * 10f, Color.magenta);
                
                }
            }
            //Debug.Log(forceMultiplier);
            if(slowingEntity)
                traveller.GetComponent<Pawn>().slowMultiplier = speedUpCurve.Evaluate(percent);
            
            
        }
    }

    public override void OnTravellerEnter(TravelEntity entity)
    {
        base.OnTravellerEnter(entity);
        entity.GetComponent<Pawn>().Attract(transform);
        triggerEnterDistance = (transform.position - entity.transform.position).magnitude;
    }

    public void EnterTraveller(Pawn entity)
    {
        OnTravellerEnter(entity.GetComponent<TravelEntity>());
    }

    protected override void OnTriggerExit(Collider other)
    {
        TravelEntity entity = other.GetComponent<TravelEntity>();

        if (entity && traveller == entity)  {
            traveller.GetComponent<Pawn>().Attract(null);
            traveller = null;
        }
    }
}
