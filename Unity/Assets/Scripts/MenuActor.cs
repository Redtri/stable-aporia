﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
[System.Serializable]
public class MenuActorEvent : UnityEvent<MenuActor> { }

public class MenuActor : MonoBehaviour
{
    [System.Serializable]
    public struct MenuAnim
    {
        public Transform body;
        public Transform localTarget;
        public float duration;
        public Transform localSource;
    };

    public MenuAnim[] animations;
    public float checkInterval;
    public float fadeDuration;
    public Graphic graphic;
    public int menuHandler;
    public MenuActorEvent onInteract;

    public AK.Wwise.Event wwOnInteract;
    public AK.Wwise.Event wwOnCancel;

    private PlayerController controller;
    private bool wasInteracting;
    private bool focused;

    private void Start()
    {
        controller = PlayerController.instance;
    }

    private void Fade(bool fadeOut)
    {
        StopAllCoroutines();
        focused = !fadeOut;
        graphic.DOFade((fadeOut) ? 0f : 1f, fadeDuration);
        if (!fadeOut) {
            StartCoroutine(CheckForInput());
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("CamRay")) {
            Fade(false);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("CamRay")) {
            Fade(true);
        }
    }

    private IEnumerator CheckForInput()
    {
        while (focused) {
            if (MenuCam.currentCam.ready) {
                if (controller.interactInput) {
                    if (!wasInteracting && !MenuCam.currentCam.focusing) {
                        wasInteracting = true;
                        //First frame interact
                        onInteract?.Invoke(this);
                        //Animations(true);
                    }

                }
                else {
                    if (wasInteracting)
                        wasInteracting = false;
                }
            }
            yield return new WaitForEndOfFrame();
        }
        yield return null;
    }

    public Sequence Animations(bool toTarget, float duration = 0f)
    {
        Sequence sequence = DOTween.Sequence();
        foreach (MenuAnim anim in animations)
        {
            Vector3 endPos = (toTarget) ? anim.localTarget.localPosition : anim.localSource.localPosition;
            Vector3 endRot = (toTarget) ? anim.localTarget.localEulerAngles : anim.localSource.localEulerAngles;



            sequence.Join(anim.body.DOLocalMove(endPos, (duration == 0f) ? anim.duration : duration));
            sequence.Join(anim.body.DOLocalRotate(endRot, (duration == 0f) ? anim.duration : duration));

        }
        if (toTarget) {
            wwOnInteract?.Post(gameObject);
        }
        else {
            sequence.AppendCallback(() => wwOnCancel?.Post(gameObject));
        }
        return sequence;
    }


}
