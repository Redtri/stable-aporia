﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.VFX;


public class LightActor : MonoBehaviour
{
    private HDAdditionalLightData light;
    public float intensity;
    public float range;
    public bool seqShutdown;
    public bool blitz;
    private bool blitzing;
    public AK.Wwise.Event blitzWW;
    [HideInInspector] public float startIntensity;
    [HideInInspector] public float startRange;
    public bool shut;
    public bool optimize;
    public int roomIndex;
    public int currRoomIndex;
    private VisualEffect vfx;
    public VisualEffectAsset vfxAsset;
    public Renderer render;
    public bool silent;

    public void OnEnable()
    {
        light = GetComponent<HDAdditionalLightData>();
        vfxAsset = Resources.Load<VisualEffectAsset>("Sparks");
        if (!shut)
        {
            startIntensity = light.intensity;
            startRange = light.range;
        }
        else
        {
            startIntensity = intensity;
            startRange = range;
        }
    }

    private float storedRange;
    private Vector3[] corners = new Vector3[8];
    
    /*
    private void Update()
    {
        //ScriptableCullingParameters cullingParameters;
        //Pawn.controlledPawn.cam.
        CullingResults cullingResults;
        
        if (optimize)
        {
            //Debug.DrawRay(transform.position, (Pawn.controlledPawn.cam.transform.position-transform.position), Color.cyan);
            float rg = startRange ;
            corners[0] = transform.position + new Vector3(-rg, rg, -rg);
            corners[1] = transform.position + new Vector3(rg, rg, -rg);
            corners[2] = transform.position + new Vector3(-rg, -rg, -rg);
            corners[3] = transform.position + new Vector3(rg, -rg, -rg);
            corners[4] = transform.position + new Vector3(-rg, rg, rg);
            corners[5] = transform.position + new Vector3(rg, rg, -rg);
            corners[6] = transform.position + new Vector3(-rg, -rg, -rg);
            corners[7] = transform.position + new Vector3(rg, -rg, -rg);
            
            if (Visible() && currRoomIndex == roomIndex)
            {
                bool oneUnblocked = false;
            
                if (light.intensity != startIntensity)
                {
                    light.intensity = startIntensity;
                    light.range = startRange;
                }
                return;
            }
            light.intensity = 0f;
            light.range = 0f;
        }
        
    }
    */

    private void OnBecameVisible()
    {
        light.intensity = startIntensity;
        light.range = startRange;
    }

    private void OnBecameInvisible()
    {
        light.intensity = 0f;
        light.range = 0f;
    }

    bool Visible()
    {
        Camera cam = Pawn.controlledPawn.cam;
        Bounds bounds = new Bounds(transform.position, Vector3.one * (startRange));
        Plane[] frustumPlanes = GeometryUtility.CalculateFrustumPlanes(cam);
        
        
        return GeometryUtility.TestPlanesAABB(frustumPlanes, bounds);
    }

    private void OnDrawGizmos()
    {
        if (optimize)
        {
            Gizmos.color = Color.red;
            for (int i = 0; i < 8; ++i)
            {
                Gizmos.DrawCube(corners[i], Vector3.one* 0.1f);
            }
        }
    }

    public void Blitz()
    {
        if (!blitzing)
        {
            blitzing = true;
            StartCoroutine(BlitzCo());
        }
    }
    
    private IEnumerator BlitzCo()
    {
        bool lighted = !shut;
        while (blitzing)
        {
            float delay = UnityEngine.Random.Range(0f, .25f);

            DOVirtual.Float((lighted) ? startIntensity : 0.1f, (lighted) ? 0.1f : startIntensity, delay, arg0 =>
            {
                light.intensity = arg0;
                //render?.material.SetFloat("_EmissiveIntensity", arg0);
            });
            DOVirtual.Float((lighted) ? 0.0f : 1.0f, (lighted) ? 1.0f : 0.0f, delay, arg0 =>
            {
                render?.material.SetFloat("_EmissiveExposureWeight", arg0);
            });
            blitzWW?.Post(gameObject);
            lighted = !lighted;
            //Debug.Log("Blitz");
            yield return new WaitForSeconds(delay);
        }

        yield return null;
    }

    public void Load()
    {
        if (!light)
        {
            light = GetComponent<HDAdditionalLightData>();
        }
        shut = true;
        light.intensity = intensity;
    }
    
    public void Optimize()
    {
        if (!light)
        {
            light = GetComponent<HDAdditionalLightData>();
        }
        shut = false;
        light.intensity = 0f;
    }
    
    public void Trigger(float duration)
    {
        if (!light)
        {
            light = GetComponent<HDAdditionalLightData>();
        }
        //Debug.Log(transform.parent.name + " light triggered");
        if (duration > 0f) {
            float currIntensity = light.intensity;
            float currRange = light.range;
            
            if (shut) {
                shut = false;
                
                DOVirtual.Float(currIntensity, startIntensity, duration, (float x) => light.intensity = x);
                DOVirtual.Float(currRange, startRange, duration, (float x) => light.range = x);
            }else {
                shut = true;
                if(!silent)
                    SoundManager.instance.ShutLights(false, gameObject);
                DOVirtual.Float(currIntensity, 0f, duration, (float x) => light.intensity = x);
                DOVirtual.Float(currRange, 0f, duration, (float x) => light.range = x);
            }
        } else {
            if (shut)
            {
                if (light)
                {
                    //Debug.Log("shouldn't");
                    light.intensity = startIntensity;
                    light.range = startRange;
                }
            }
            else
            {
                if (light)
                {
                    if (!silent)
                        SoundManager.instance.ShutLights(true, gameObject);
                    light.intensity = 0f;
                    light.range = 0f;
                }
            }

            shut = !shut;
        }
    }
}
