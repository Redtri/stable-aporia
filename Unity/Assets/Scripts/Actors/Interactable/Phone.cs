﻿using System;
using System.Collections;
using System.Collections.Generic;
using AK.Wwise;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.Events;

using UnityEngine.PlayerLoop;
using UnityEngine.UI;

public enum eTRIGGER
{
    OnCheckout, Event
}

public enum eMESSAGE_TYPE
{
    Ring, OnPull, Auto
}

public enum ePHONE_STATE
{
    Neutral, WaitFree, WaitPull, WaitTime, WaitFreeTime, Ring, SpeakLive, SpeakHP
}

[System.Serializable]
public class PhoneMessage
{
    public bool messageSent;
    public eMESSAGE_TYPE messageType;
    public AK.Wwise.Event messageEvent;
    public RingInfo ringInfo;
    public bool cancellable;
    public bool disabled;

    public bool autoNext;
    public int nextPhoneIndex;

    //TODO : Fill variable with time on callback
    [HideInInspector] public float stopTime;
    [HideInInspector] public float speakTime;

    public bool useInvokeFromDuration;
    public float invokeDuration;
    public int phoneIndexAboveDuration;

    public UnityEvent onMessageStart;
    public UnityEvent onMessageEnd;
    
    [System.Serializable]
    public class RingInfo
    {
        public float delay;
        public bool ringUntilPulled;
        public float duration;
        [HideInInspector] public float ringTime;
    }

    private PhoneMessage reference;

    public PhoneMessage(PhoneMessage messageCpy)
    {
        reference = messageCpy;

        messageSent = messageCpy.messageSent;
        messageType = messageCpy.messageType;
        messageEvent = messageCpy.messageEvent;
        ringInfo = messageCpy.ringInfo;
        cancellable = messageCpy.cancellable;
        disabled = messageCpy.disabled;
        autoNext = messageCpy.autoNext;
        nextPhoneIndex = messageCpy.nextPhoneIndex;
        stopTime = messageCpy.stopTime;
        speakTime = messageCpy.speakTime;
        useInvokeFromDuration = messageCpy.useInvokeFromDuration;
        invokeDuration = messageCpy.invokeDuration;
        phoneIndexAboveDuration = messageCpy.phoneIndexAboveDuration;
        onMessageStart = new UnityEvent();
        onMessageStart.AddListener(messageCpy.onMessageStart.Invoke);
        onMessageEnd = new UnityEvent();
        onMessageEnd.AddListener(messageCpy.onMessageEnd.Invoke);
    }
}

[System.Serializable]
public class PhoneHelper
{
    public eTRIGGER trigger;
    public System.Collections.Generic.List<PhoneMessage> messages;
}

public class Phone : Interactable
{
    public bool blockPawn = true;
    public float putStillAfterDuration = 0.0f;
    public AK.Wwise.RTPC reverbRTPC;
    public AK.Wwise.Event ring;
    public AK.Wwise.Event pull;
    public AK.Wwise.Event pullBuzz;
    public AK.Wwise.Event pullBuzzExtra;
    public AK.Wwise.Event stop;
    public UnityEvent onStill;
    public UnityEvent onPulled;

    public Image circle;
    public Image imgState;
    
    public PhoneData data;
    
    public Transform speaker;
    private Vector3 startPos;
    private Vector3 startRotEuler;

    [HideInInspector] public bool isCheckedOut = false;
    [HideInInspector] public int roomIndex;

    [HideInInspector] public PhoneHelper helper;
    public PhoneMessage currentMessage;
    public PhoneMessage previousMessage;
    public int currMessIndex;
    
    public bool isPulled;
    public ePHONE_STATE phoneState;
    public System.Collections.Generic.List<PhoneMessage> awaitingMessages;
    
    public int nextMessage
    {
        get
        {
            return (currentMessage.autoNext) ? ((currMessIndex < helper.messages.Count-1) ? (currMessIndex+1)%helper.messages.Count : -1) : currentMessage.nextPhoneIndex;
        }
    }

    private void Start()
    {
        //imgState.enabled = false;
        startPos = speaker.position;
        startRotEuler = speaker.rotation.eulerAngles;
    }

    public void Init(PhoneHelper phoneHelper, int index)
    {
        helper.trigger = phoneHelper.trigger;
        helper.messages = new List<PhoneMessage>();
        roomIndex = index;
        foreach (PhoneMessage message in phoneHelper.messages)
        {
            helper.messages.Add(new PhoneMessage(message));
        }
    }

    public void ValidateMessage(int index)
    {
        Debug.Log("Message " + index + " activated");
        helper.messages[index].messageSent = true;
    }
    
    public void CheckoutPhone()
    {
        enabled = true;
        //Debug.Log("Checking out " + helper.roomIndex + "'s room phone");
        isCheckedOut = true;
        imgState.enabled = true;
        phoneState = ePHONE_STATE.Neutral;
        
        if (helper.trigger == eTRIGGER.OnCheckout) {
            TriggerPhone();
        }
    }
    
    public void TriggerPhone()
    {
        //Will update its new current message
        if (currMessIndex < helper.messages.Count)
        {
            previousMessage = currentMessage;
            currentMessage = helper.messages[currMessIndex];
            if (!currentMessage.autoNext) {
                currMessIndex = currentMessage.nextPhoneIndex;
            } else {
                ++currMessIndex;
            }
            //Debug.Log("Previous : " + previousMessage.messageEvent + ". New " + currentMessage.messageEvent + ". Index : " + currMessIndex);
            StartMessage();
        }
    }

    private void ResetMessage()
    {
        if(!currentMessage.messageSent && currentMessage.messageType != eMESSAGE_TYPE.OnPull) {
            currMessIndex = (helper.messages.IndexOf(currentMessage) > 0) ? currMessIndex - 1 : 0;
            Debug.Log("Resetting current message to " + currMessIndex);
        }
    }

    private void Callback(object in_cookie, AkCallbackType in_type, object in_info)
    {
        //Debug.Log("EndMessage " + helper.messages.IndexOf(currentMessage));
        if(in_type != AkCallbackType.AK_Duration)
        {
            currentMessage.onMessageEnd?.Invoke();
            if (isPulled)
                phoneState = ePHONE_STATE.SpeakLive;
            else
                phoneState = ePHONE_STATE.Neutral;

            if (!currentMessage.useInvokeFromDuration)
                TriggerPhone();
        }
        else
        {
            AK.Wwise.Event wwEvent = (AK.Wwise.Event )in_cookie;
            AkDurationCallbackInfo durationInfo = (AkDurationCallbackInfo)in_info;

            DialogsManager.instance.NotifyMorphEvent(wwEvent, durationInfo.fDuration);
        }
    }

    public void StartMessage()
    {
        phoneState = ePHONE_STATE.Neutral;
        switch (currentMessage.messageType)
        {
            case eMESSAGE_TYPE.Auto:
                Speak();
                break;
            case eMESSAGE_TYPE.Ring:
                if (isPulled)
                {
                    if (previousMessage.cancellable)
                        Speak();
                }
                else
                {
                    Ring();
                }
                break;
            case eMESSAGE_TYPE.OnPull:
                if (isPulled)
                    Speak();
                break;
        }
    }

    public void EnableMessage(int index)
    {
        helper.messages[index].disabled = false;
        //awaitingMessages[index].disabled = false;
    }

    private void Update()
    {
        //Debug.Log(phoneState);
        CheckCurrentMessage();
    }

    private void CheckCurrentMessage()
    {
        if (GameManager.instance.currentRoom.roomIndex == roomIndex || GameManager.instance.preCheckedOutRoom == roomIndex) {
            switch (phoneState) {
                case ePHONE_STATE.Ring:
                    //Stops the alarm to speak
                    if (!currentMessage.ringInfo.ringUntilPulled) {
                        //Waits until max ring duration is reached
                        if (Time.time - currentMessage.ringInfo.ringTime > currentMessage.ringInfo.duration) {
                            Speak();
                        }
                    }
                    break;
                case ePHONE_STATE.SpeakLive:
                    if (!isPulled)
                        phoneState = ePHONE_STATE.SpeakHP;
                    break;
                case ePHONE_STATE.SpeakHP:
                case ePHONE_STATE.Neutral:
                    if (isPulled) {
                        if (currentMessage.messageSent && helper.messages[currMessIndex].messageType == eMESSAGE_TYPE.OnPull) {
                            TriggerPhone();
                        }
                    }
                    //TODO : Change conditions (Use wwise callback on message end)
                    if (currentMessage.useInvokeFromDuration) {
                        if (Time.time - currentMessage.speakTime > currentMessage.invokeDuration) {
                            currMessIndex = currentMessage.phoneIndexAboveDuration;
                            //TODO: should be triggered whenever callback end is triggered, so checked elsewhere
                            Shut();
                            TriggerPhone();
                        }
                    }

                    break;
            }
        }
    }

    public void Ring()
    {
        if (isPulled) {
            if(previousMessage.cancellable)
                Speak();
        } else {
            ring?.Post(speaker.gameObject);
            phoneState = ePHONE_STATE.Ring;
            currentMessage.ringInfo.ringTime = Time.time;
        }
    }
    
    public void Speak()
    {
        if(!currentMessage.disabled)
        {
            currentMessage.onMessageStart?.Invoke();
            if(putStillAfterDuration > 0.0f) {
                StartCoroutine(StillAfterDuration());
            }
            currentMessage.messageSent = true;
            phoneState = (isPulled) ? ePHONE_STATE.SpeakLive : ePHONE_STATE.SpeakHP;
            currentMessage.speakTime = Time.time;

            ring?.Stop(speaker.gameObject);

            if (previousMessage != null)
            {
                previousMessage.messageEvent?.Stop(speaker.gameObject);
            }
            DialogsManager.instance.NotifyWwiseEvent(currentMessage.messageEvent);
            currentMessage.messageEvent?.Post(speaker.gameObject, (uint)AkCallbackType.AK_Marker | (uint)AkCallbackType.AK_Duration, Callback, currentMessage.messageEvent);
        }
    }

    private IEnumerator StillAfterDuration()
    {
        yield return new WaitForSeconds(putStillAfterDuration);
        ForceStill();
        yield return null;
    }

    private void ForceStill()
    {
        if(isPulled) {
            Pawn.controlledPawn.interactable = null;
            Interact(Pawn.controlledPawn, eINTERACT_TYPE.Manual);
        }
    }

    public void Shut()
    {
        currentMessage.messageEvent?.Stop(speaker.gameObject);
        phoneState = ePHONE_STATE.Neutral;
    }
    
    public void LeavePhone()
    {
        isCheckedOut = false;
        ring.Stop(speaker.gameObject);
        currentMessage.messageEvent.Stop(speaker.gameObject, 1000);
        DialogsManager.instance.UnsubEvent(currentMessage.messageEvent);
        ResetMessage();
        enabled = false;
    }
    
    public override bool Interact(Pawn pawnUser, eINTERACT_TYPE interactType, bool start = true)
    {
        //TODO : check here phone states
        if (base.Interact(pawnUser, interactType, start))
        {
            if(isPulled){
                isPulled = !isPulled;
                //Phone was being used
                pawnUser.FreePawn();
                speaker.DOKill();
                speaker.DOMove(startPos, 1f);
                speaker.DORotate(startRotEuler, 1f).onComplete += PutPhoneStill;
                DOVirtual.Float(0f, 100f, 1f, value => reverbRTPC.SetValue(speaker.gameObject, value));
            } else {
                isPulled = !isPulled;
                //Phone gets pulled
                if (blockPawn) {
                    pawnUser.BlockPawn();
                    speaker.GetComponent<Renderer>().material.SetInt("_ZTestTransparent", 8);
                    speaker.GetComponent<Renderer>().material.SetInt("_ZTestGBuffer", 8);
                    speaker.GetComponent<Renderer>().material.SetInt("_ZTestDepthEqualForOpaque", 8);
                }
                else
                    pawnUser.interactable = null;
                pull.Post(speaker.gameObject);
                pullBuzz.Post(speaker.gameObject);
                speaker.DOKill();
                speaker.DOMove(pawnUser.phoneHandle.position, 1f).onComplete += (() => onPulled?.Invoke());
                DOVirtual.Float(100f, 0f, 1f, value => reverbRTPC.SetValue(speaker.gameObject, value));
                speaker.DORotate(pawnUser.phoneHandle.rotation.eulerAngles, 1f);
                if (phoneState != ePHONE_STATE.Ring && (currentMessage.messageType != eMESSAGE_TYPE.OnPull && !currentMessage.messageSent))
                    pullBuzzExtra.Post(speaker.gameObject);
                switch (phoneState)
                {
                    case ePHONE_STATE.Ring:
                        Speak();
                        break;
                    case ePHONE_STATE.SpeakHP:
                        if (currentMessage.cancellable) {
                            Shut();
                            TriggerPhone();
                        } else {
                            phoneState = ePHONE_STATE.SpeakLive;
                        }
                        break;
                    case ePHONE_STATE.Neutral:
                        if (!currentMessage.messageSent) {
                            if (currentMessage.messageType == eMESSAGE_TYPE.OnPull)
                                Speak();
                        }
                        break;
                }
                //fsm.ActionEvent();
            }
            return true;
        }

        return false;
    }

    private void PutPhoneStill()
    {
        onStill?.Invoke();
        switch (phoneState)
        {
            case ePHONE_STATE.SpeakLive:
                phoneState = ePHONE_STATE.SpeakHP;
            break;
        }

        stop.Post(speaker.gameObject);
        speaker.GetComponent<Renderer>().material.SetInt("_ZTestTransparent", 4);
        speaker.GetComponent<Renderer>().material.SetInt("_ZTestGBuffer", 4);
        speaker.GetComponent<Renderer>().material.SetInt("_ZTestDepthEqualForOpaque", 4);
        //phoneState = ePHONE_STATE.Neutral;


        //if(currentMessage.messageType == eMESSAGE_TYPE.OnPull)
        //TriggerPhone();
    }
}
