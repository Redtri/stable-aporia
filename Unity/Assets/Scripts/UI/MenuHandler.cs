﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.SceneManagement;
using UnityEngine.UI;



public class MenuHandler : MonoBehaviour
{
    public SplashIntro splasher;
    public MenuNavigator navigator;
    public AK.Wwise.RTPC masterRTPC;
    public AK.Wwise.Event menuEvent;
    public AK.Wwise.Event clicEvent;
    public AK.Wwise.Event slideBegin;
    public AK.Wwise.Event creditsAmb;
    public Image blackScreen;
    public Transform mainMenu;
    public Transform mainCamSpot;
    public Transform options;
    public Transform optionsCamSpot;

    public Transform controls;
    public Transform credits;
    public Transform creditsCamSpot;
    public Transform creditsLookCamSpot;

    private Transform current;
    public Transform camParent;
    public Travelling travelling;
    public Volume ppVolume;
    public AudioActor phonographe;

    private void Start()
    {
        //AkSoundEngine.StopAll();
        current = mainMenu;
        blackScreen.DOFade(0f, 2f).onComplete += () => blackScreen.gameObject.SetActive(false);
        ChangeMasterVolume(.7f);
        DisplayChilds(options, 0.1f, false);
        DisplayChilds(controls, 0.1f, false);
        DisplayChilds(credits, 0.1f, false);
        MenuCam.currentCam.SetReady(true);
        menuEvent?.Post(gameObject);
        splasher.onSplashEnd.AddListener(Play);
    }

    private void Update()
    {
        /*
        */
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public void SplashText()
    {
        menuEvent.Stop(gameObject, splasher.GetTotalDurationMillis());
        MenuCam.currentCam.Block();
        splasher.Trigger();
    }

    public void Play()
    {
        blackScreen.gameObject.SetActive(true);
        blackScreen.DOFade(1f, 2f).onComplete += () => LoadGameScene();
        phonographe.FadeStop();
    }

    public void Menu(bool fromCredits)
    {
        navigator.Activate(false);

        Sequence seq1 = DOTween.Sequence();

        MenuCam.currentCam.SetStaticRotation(mainCamSpot.rotation);
        MenuCam.currentCam.SetReady(false);

        if (fromCredits) {
            travelling.Stop();
            seq1.Append(blackScreen.DOFade(1f, 2f));
        }


        if (fromCredits)
        {
            creditsAmb?.Stop(gameObject, 2000);
            menuEvent?.Post(gameObject);
            seq1.Append(camParent.DOMove(creditsLookCamSpot.position, 1f).SetEase(Ease.OutQuint));
            seq1.Append(blackScreen.DOFade(0f, 1f));
            seq1.Join(camParent.DOMove(mainCamSpot.position, 2f));
        }
        else {
            seq1.Append(camParent.DOMove(creditsLookCamSpot.position, 1f));
        }

        seq1.AppendCallback(() => navigator.Activate(false));
        seq1.AppendCallback(() => MenuCam.currentCam.Free());
        seq1.AppendCallback(() => MenuCam.currentCam.SetReady(true));

        DisplayChilds(current, 1f, false);
        current = mainMenu;
        DisplayChilds(mainMenu, 1f, true);
    }

    public void Menu(MenuActor menuActor)
    {
        Sequence seq1 = DOTween.Sequence();

        seq1.Join(camParent.DOMove(mainCamSpot.position, 2f));
        MenuCam.currentCam.SetStaticRotation(mainCamSpot.rotation);
        MenuCam.currentCam.SetReady(false);

        seq1.AppendCallback(() => navigator.Activate(false));
        seq1.AppendCallback(() => options.GetComponent<TranslateActor>().Translate());
        seq1.AppendCallback(() => MenuCam.currentCam.Free());
        seq1.AppendCallback(() => MenuCam.currentCam.SetReady(true));

        menuActor.Animations(false, 1f);

        DisplayChilds(current, 1f, false);
        current = mainMenu;
        DisplayChilds(mainMenu, 1f, true);
    }

    public void Options(MenuActor menuActor)
    {
        Sequence seq1 = DOTween.Sequence();

        MenuCam.currentCam.Block();
        MenuCam.currentCam.SetStaticRotation(optionsCamSpot.rotation);
        MenuCam.currentCam.SetReady(false);

        seq1.Join(camParent.DOMove(optionsCamSpot.position, 2f)).onComplete += () => navigator.Activate(true);
        navigator.SetCurrentHandler(0);


        //clicEvent?.Post(gameObject);
        DisplayChilds(current, 1f, false);
        current = options;

        if (menuActor) {
            seq1.AppendCallback(() => options.GetComponent<TranslateActor>().Translate());
            seq1.AppendCallback(() => DisplayChilds(options, 1f, true));
            seq1.AppendCallback(() => menuActor.Animations(true, 1f));
        }
        seq1.AppendCallback(() => MenuCam.currentCam.SetReady(true));
    }
    public void Options()
    {
        Sequence seq1 = DOTween.Sequence();

        MenuCam.currentCam.Block();
        MenuCam.currentCam.SetStaticRotation(optionsCamSpot.rotation);
        MenuCam.currentCam.SetReady(false);

        seq1.Join(camParent.DOMove(optionsCamSpot.position, 2f)).onComplete += () => navigator.Activate(true);
        //seq1.AppendCallback(() => MenuCam.currentCam.SetReady(true));

        navigator.SetCurrentHandler(0);
        DisplayChilds(options, 1f, true);

        //clicEvent?.Post(gameObject);
        DisplayChilds(current, 1f, false);
        current = options;
        seq1.AppendCallback(() => MenuCam.currentCam.SetReady(true));
    }

    public void Controls()
    {
        MenuCam.currentCam.SetReady(false);
        navigator.SetCurrentHandler(1);
        clicEvent?.Post(gameObject);
        DisplayChilds(current, 1f, false);
        current = controls;
        DisplayChilds(controls, 1f, true).AppendCallback(() => MenuCam.currentCam.SetReady(true));
    }
    
    public void Credits()
    {
        menuEvent?.Stop(gameObject, 2000);
        blackScreen.gameObject.SetActive(true);
        navigator.SetCurrentHandler(2);

        MenuCam.currentCam.Block();
        MenuCam.currentCam.SetStaticRotation(creditsLookCamSpot.rotation);
        MenuCam.currentCam.SetReady(false);

        Sequence sequence = DOTween.Sequence();

        sequence.Append(blackScreen.DOFade(1f, 2f));
        sequence.Join(camParent.DOMove(creditsLookCamSpot.position, 2f));
        sequence.AppendCallback(() => MenuCam.currentCam.SetStaticRotation(creditsCamSpot.rotation));

        sequence.Append(camParent.DOMove(creditsCamSpot.position, 2f));
        sequence.AppendCallback(() => DisplayChilds(credits, 1f, true));
        sequence.AppendCallback(() => travelling.BeginTravelling(camParent));
        sequence.Append(blackScreen.DOFade(0f, 2f));
        sequence.AppendCallback(() => navigator.Activate(true));
        sequence.AppendCallback(() => creditsAmb?.Post(gameObject));

        sequence.AppendCallback(() => MenuCam.currentCam.SetReady(true));

        DisplayChilds(current, 1f, false);
        current = credits;
    }

    public void StartSlide()
    {
        slideBegin?.Post(gameObject);
    }

    public void EndSlide()
    {
        slideBegin?.Stop(gameObject);
    }
    
    public void Exit()
    {
        clicEvent?.Post(gameObject);
        Application.Quit();
    }

    private void LoadGameScene()
    {
        SceneManager.LoadSceneAsync(1);
    }

    public void ChangeMasterVolume(float value)
    {
        masterRTPC.SetGlobalValue(value * 100f);
    }
    
    public void ChangeLift(float value)
    {
        LiftGammaGain liftGammaGain;
        ppVolume.profile.TryGet(out liftGammaGain);
        if (liftGammaGain)
        {
            liftGammaGain.gain.value = new Vector4(1f, 1f, 1f, value);
        }
    }

    private Sequence DisplayChilds(Transform container, float duration, bool show = true)
    {
        if(show)
            container.gameObject.SetActive(true);
        
        Sequence tmpSequence = DOTween.Sequence();
        foreach (MaskableGraphic mask in container.GetComponentsInChildren<MaskableGraphic>())
        {
            tmpSequence.Join(mask.DOFade(show ? 1f : 0f, duration));
        }

        if (!show)
            tmpSequence.AppendCallback(() => container.gameObject.SetActive(false));

        return tmpSequence;
    }
}
