﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Dialog Morph", menuName = "Scriptables/DialogMorph", order =1)]
public class DialogMorph : Dialog
{
    [System.Serializable]
    public class DialogMorphInfo
    {
        public List<Dialog.LineInfo> lineInfosMorph;
        public int durationToExceed;
    }

    public DialogMorphInfo GetDiagMorph(float duration)
    {
        DialogMorphInfo foundDiag = null;

        foreach(DialogMorphInfo dialogMorphInfo in dialogMorphs)
        {
            if (duration > dialogMorphInfo.durationToExceed)
            {
                if (foundDiag != null)
                {
                    if (duration > foundDiag.durationToExceed)
                        foundDiag = dialogMorphInfo;
                }
                else
                {
                    foundDiag = dialogMorphInfo;
                }
            }
        }
        return foundDiag;
    }

    new private List<LineInfo> lineInfos;
    public List<DialogMorphInfo> dialogMorphs;
}
