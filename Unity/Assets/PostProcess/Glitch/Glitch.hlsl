#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
#include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderVariables.hlsl"

struct Attributes
{
    uint vertexID : SV_VertexID;
    UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct Varyings
{
    float4 positionCS : SV_POSITION;
    float2 texcoord   : TEXCOORD0;
    UNITY_VERTEX_OUTPUT_STEREO
};

Varyings Vertex(Attributes input)
{
    Varyings output;
    UNITY_SETUP_INSTANCE_ID(input);
    UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);
    output.positionCS = GetFullScreenTriangleVertexPosition(input.vertexID);
    output.texcoord = GetFullScreenTriangleTexCoord(input.vertexID);
    return output;
}

uint _Seed;

float _BlockStrength;
uint _BlockStride;
uint _BlockSeed1;
uint _BlockSeed2;

float2 _Drift;
float2 _Jitter;
float2 _Destruct;
float _DestructAbsolute;
float _Roots;
float2 _Jump;
float _Shake;
float _EditTime;
float3 _PawnPosition;

TEXTURE2D_X(_InputTexture);

uniform float2 u_resolution;
uniform float2 u_mouse;
uniform float u_time;

float2 random2( float2 p ) {
    float3 a = frac(p.xyx*float3(123.34, 234.34, 345.65));
    a += dot(a, a+34.45);
    
    return frac(sin(float2(a.x * a.y, a.y * a.z)));
}

float rand2dTo1d(float2 value, float2 dotDir = float2(12.9898, 78.233)){
    //make value smaller to avoid artefacts
    float2 smallValue = sin(value);
    //get scalar value from 3d vector
    float random = dot(smallValue, dotDir);
    //make value more random by making it bigger and then taking teh factional part
    random = frac(sin(random) * 143758.5453);
    return random;
}

float random (in float2 st) {
    return frac(sin(dot(st.xy,
                         float2(12.9898,78.233)))
                 * 43758.5453123);
}

float noise (in float2 st) {
    float2 i = floor(st);
    float2 f = frac(st);

    // Four corners in 2D of a tile
    float a = random(i);
    float b = random(i + float2(1.0, 0.0));
    float c = random(i + float2(0.0, 1.0));
    float d = random(i + float2(1.0, 1.0));

    // Smooth Interpolation

    // Cubic Hermine Curve.  Same as SmoothStep()
    float2 u = f*f*(3.0-2.0*f);
    // u = smoothstep(0.,1.,f);

    // Mix 4 coorners percentages
    return lerp(a, b, u.x) +
            (c - a)* u.y * (1.0 - u.x) +
            (d - b) * u.x * u.y;
}

float FRandom(uint seed)
{
    return GenerateHashedRandomFloat(seed);
}

float4 Fragment(Varyings input) : SV_Target
{
    UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

    float2 uv = input.texcoord;

    #if defined(GLITCH_BLOCK)

    //
    // Block glitch
    //

    uint block_size = 32;
    uint columns = _ScreenSize.x / block_size;

    // Block index
    uint2 block_xy = input.texcoord * _ScreenSize.xy / block_size;
    uint block = block_xy.y * columns + block_xy.x;

    // Segment index
    uint segment = block / _BlockStride;

    // Per-block random number
    float r1 = FRandom(block     + _BlockSeed1);
    float r3 = FRandom(block / 3 + _BlockSeed2);
    uint seed = (r1 + r3) < 1 ? _BlockSeed1 : _BlockSeed2;
    float rand = FRandom(segment + seed);

    // Block damage (offsetting)
    block += rand * 20000 * (rand < _BlockStrength);

    // Screen space position reconstruction
    uint2 ssp = uint2(block % columns, block / columns) * block_size;
    ssp += (uint2)(input.texcoord * _ScreenSize.xy) % block_size;

    // UV recalculation
    uv = frac((ssp + 0.5) / _ScreenSize.xy);

    #endif

    #if defined(GLITCH_BASIC)

    //
    // Basic glitch effects
    //

    // Texture space position
    float tx = uv.x;
    float ty = uv.y;

    // Jump
    //ty = lerp(ty, frac(ty + _Jump.x), _Jump.y);

    // Screen space Y coordinate
    uint sy = ty * _ScreenSize.y;
    uint sx = tx * _ScreenSize.x;
    
    
    //float jitterY = cos(tx * _EditTime) * _Jitter;
    //ty += jitterY;

    // Shake
    tx = frac(tx + (Hash(_Seed) - 0.5) * _Shake);
    //ty = frac(ty + (Hash(_Seed) - 0.5) * _Shake);

    // Drift
    float drift = sin(ty * 2 + _Drift.x) * _Drift.y;
    
    
    float4 colob = float4(0,0,0,1);
    uv *= 20.0; //Scaling amount (larger number more cells can be seen)
    
    float2 gv = frac(uv) - 0.5;
    float2 id = floor(uv);
    float2 cid = 0.;
    float minDist = 5.0;//abs(sin(_EditTime));  // minimun distance
    
    //colob.rg = id*.1;
    
    for(float y = -1.; y <= 1.; ++y)
    {
        for(float x = -1.; x <= 1.; ++x)
        {
            float2 offs = float2(x,y);
            float2 neighbour = random2(id + offs);
            
            float2 pointv = offs + sin(_EditTime * neighbour * 0.5) * .5;//each point moves in a certain way
            
            float dist = length(gv-pointv);
            
            if(dist < minDist)
            {
                minDist = dist;
                cid = id + offs;
            }
        }
    }
    float2 st = input.texcoord.xy / _ScreenSize.xy;
    
    float2 simplexNoise = noise(uv + _EditTime) + random(uv + _EditTime) * 0.25;
    
    float coll = float2(minDist, rand2dTo1d(cid)).y * 0.5;
    
    float layeredNoise = (simplexNoise.xy ) + coll;
    
    //colob.rgb = layeredNoise;
    
    //return colob;
    
    const float pi = 3.141592653589793238462;
    
    // Destruction
    float destruct = _Destruct.y * 2.0 * layeredNoise;
    destruct *= sin(_EditTime) * (_DestructAbsolute);
    ty += destruct;
    tx += destruct * (-1.0);
    
    // Jitter
    float jitter = Hash(sy + _Seed) * 2 - 1;
    tx += jitter * (_Jitter.x < abs(jitter)) * _Jitter.y;
    
    tx += _Roots/4.0 * 2.0 * coll;
    
    //float3 test = SHADER_GRAPH_SAMPLE_SCENE_COLOR(uv);

    // Source sample
    uint sx1 = (tx        ) * _ScreenSize.x;
    uint sx2 = (tx + drift) * _ScreenSize.x;
    float4 c1 = LOAD_TEXTURE2D_X(_InputTexture, uint2(sx1, ty * _ScreenSize.y));
    float4 c2 = LOAD_TEXTURE2D_X(_InputTexture, uint2(sx2, ty * _ScreenSize.y));
    float4 c = float4(c1.r, c2.g, c1.b, c1.a);
    #else

    float4 c = LOAD_TEXTURE2D_X(_InputTexture, uv * _ScreenSize.xy);

    #endif

    #if defined(GLITCH_BLOCK)

    // Block damage (color mixing)
    if (frac(rand * 1234) < _BlockStrength * 0.1)
    {
        float3 hsv = RgbToHsv(c.rgb);
        hsv = hsv * float3(-1, 1, 0) + float3(0.5, 0, 0.9);
        c.rgb = HsvToRgb(hsv);
    }

    #endif

    return c;
}
