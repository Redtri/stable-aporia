﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine.PlayerLoop;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.Rendering.UI;
using UnityEditor;
using UnityEngine.Events;

[System.Serializable]
public class FloatEvent : UnityEvent<float>
{
};

[ExecuteInEditMode]
public class RoomShrinker : MonoBehaviour
{
    [Range(1, 20)] public int shrinkScale = 1;
    public float shrinkDuration;

    public RoomGenerator roomGenerator;
    public FloatEvent onShrinkStart;
    public FloatEvent onShrinkEnd;
    public FloatEvent onShrinkUpdate;
    
    private bool shrinked;
    private float srcDistToCenter;

    private void OnEnable()
    {
        roomGenerator = this.GetComponent<RoomGenerator>();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.DrawLine(northWallParent.position, northWallParent.position + ((roomCenter.position - northWallParent.position) * ));
    }

    private float shrinkTime;
    public void ShrinkRoom()
    {
        shrinkTime = Time.time;
        onShrinkStart?.Invoke(shrinkDuration);
        Sequence shrinkSequence = DOTween.Sequence();

        
        WallHandler[] wallHandlers = new[] {roomGenerator.nort, roomGenerator.east, roomGenerator.south, roomGenerator.west};
        for (int i = 0; i < Enum.GetValues(typeof(WallHandler.eCAPE)).Length; ++i)
        {
            shrinkSequence.Join(MoveWallParent(wallHandlers[i]));
        }
        
        MoveFurnitures(roomGenerator.nort.wallDimensions.x);
        
        shrinkSequence.AppendCallback(ShrinkStop);

        shrinkSequence.onUpdate += OnShrinkUpdate;
    }

    public void OnShrinkUpdate()
    {
        float percent = (Time.time - shrinkTime) / shrinkDuration;
        onShrinkUpdate?.Invoke(percent*100f);
    }

    public void ShrinkStop()
    {
        shrinked = !shrinked;
        onShrinkEnd?.Invoke(shrinkDuration);
    }

    private TweenerCore<Vector3, Vector3, VectorOptions> MoveWallParent(WallHandler handler)
    {
        //Choosing the direction from the handler regarding if we are shrinking or expanding from the center of the room
        Vector3 direction = handler.startOffset.normalized;
        float distFromCenter = Vector3.Distance(handler.parent.position, roomGenerator.roomCenter.position);
        float distance = handler.wallDimensions.x * ((shrinkScale%2==0) ? 1f : 0.5f) * shrinkScale;
        //float scaleMultiplier = (shrinked) ? currentScale : finalScale;

        return handler.parent.DOMove(roomGenerator.roomCenter.position + direction * (distance), shrinkDuration);
    }

    private void MoveFurnitures(float wallWidth)
    {
        for (int i = 0; i < roomGenerator.furnitures.Count; ++i)
        {
            FurnitureInfos furniture = roomGenerator.furnitures[i];
            
            float distance = wallWidth * ((shrinkScale%2==0) ? 1f : 0.5f) * shrinkScale;
            Vector3 finalPos = roomGenerator.furnitureParent.position + furniture.startOffset / distance;
            finalPos.y = furniture.gameObject.transform.position.y;
            /*
            Vector3 finalPos = (shrinked)
                ? roomGenerator.furnitureParent.position + furniture.startOffset
                : roomGenerator.furnitureParent.position + furniture.startOffset / shrinkScale;
            */

            roomGenerator.furnitures[i].gameObject.transform.DOMove(finalPos, shrinkDuration);
        }
    }

    /*
    private Transform[] GetCapeNeighbours(WallHandler.eCAPE cape)
    {
        Transform[] tmp = new Transform[2];

        switch (cape)
        {
            case WallHandler.eCAPE.EAST:
            case WallHandler.eCAPE.WEST:
                tmp[0] = roomGenerator.wallHandlers[(int)WallHandler.eCAPE.NORTH].parent;
                tmp[1] = roomGenerator.wallHandlers[(int)WallHandler.eCAPE.SOUTH].parent;
                break;
            case WallHandler.eCAPE.NORTH:
            case WallHandler.eCAPE.SOUTH:
                tmp[0] = roomGenerator.wallHandlers[(int)WallHandler.eCAPE.EAST].parent;
                tmp[1] = roomGenerator.wallHandlers[(int)WallHandler.eCAPE.WEST].parent;
                break;
        }

        return tmp;
    }
    */
}
