﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
{
    private static T _instance;
    public static T instance
    {
        get
        {
            if (_instance = null)
            {
                return _instance;
            }
            else
            {
                _instance = FindObjectOfType<T>();
                if (!_instance) Debug.Log("Guillaume tu as oublié de mettre ton singleton : " + _instance.GetType().ToString());
                    return _instance;
            }
        }
    }
}
