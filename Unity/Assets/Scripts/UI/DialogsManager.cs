﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogsManager : MonoSingleton<DialogsManager>
{

    public struct MorphHandler
    {
        public int index;
        public DialogMorph dialog;
    }

    private List<Dialog> beingTold;
    public List<Dialog> toTell;
    private List<MorphHandler> tempMorphHandlers;
    private List<Dialog> told;

    public Transform dialogParent;
    public Text textArea;

    // Start is called before the first frame update
    void Start()
    {
        told = new List<Dialog>();
        beingTold = new List<Dialog>();
        tempMorphHandlers = new List<MorphHandler>();
        SetVisibility(false);
    }

    public void SetVisibility(bool visible = true)
    {
        dialogParent.gameObject.SetActive(visible);
    }

    public void NotifyWwiseEvent(AK.Wwise.Event wwEvent)
    {
        if (LanguageManager.stLanguage == eLanguage.English)
        {
            Dialog dialog = toTell.Find(arg0 => arg0 && arg0.wwEvent.Id == wwEvent.Id && arg0.GetType() != typeof(DialogMorph));
            if(dialog)
                StartCoroutine(DialogCo(dialog));
        }
    }

    public void UnsubEvent(AK.Wwise.Event wwEvent)
    {
        if (LanguageManager.stLanguage == eLanguage.English)
        {
            Dialog dialog = beingTold.Find(arg0 => arg0 && arg0.wwEvent.Id == wwEvent.Id && arg0.GetType() != typeof(DialogMorph));
            if (dialog) {
                beingTold.Remove(dialog);
            }
        }
    }

    public void NotifyMorphEvent(AK.Wwise.Event wwEvent, float duration)
    {
        if (LanguageManager.stLanguage == eLanguage.English)
        {
	        foreach(Dialog diag in toTell)
	        {
		        if(diag && diag.wwEvent.Id == wwEvent.Id)
		        {
                	if(diag.GetType() == typeof(DialogMorph))
                	{
                        DialogMorph morphCast = (DialogMorph)diag;
                        morphCast.approxDuration = duration;
                        StartCoroutine(DialogMorphCo(morphCast));
                	}
		        }
		    }
        }
    }

    private IEnumerator DialogCo(Dialog dialogData)
    {
        int currentLine = 0;
        beingTold.Add(dialogData);
        while (currentLine < dialogData.lineInfos.Count && beingTold.Contains(dialogData))
        {
            Dialog.LineInfo lineInfo = dialogData.lineInfos[currentLine];

            yield return new WaitForSeconds(lineInfo.delay);
            textArea.text = lineInfo.text;
            SetVisibility(true);
            yield return new WaitForSeconds(lineInfo.duration);
            if (currentLine + 1 < dialogData.lineInfos.Count && dialogData.lineInfos[currentLine + 1].delay > 1f )
            {
                SetVisibility(false);
            }
            textArea.text = "";
            ++currentLine;
        }
        yield return null;
        SetVisibility(false);

        beingTold.Remove(dialogData);
        toTell.Remove(dialogData);
        told.Add(dialogData);
    }

    private IEnumerator DialogMorphCo(DialogMorph dialogMorph)
    {
        int currentLine = 0;
        DialogMorph.DialogMorphInfo dialog = dialogMorph.GetDiagMorph(dialogMorph.approxDuration);

        while (currentLine < dialog.lineInfosMorph.Count)
        {
            Dialog.LineInfo lineInfo = dialog.lineInfosMorph[currentLine];

            yield return new WaitForSeconds(lineInfo.delay);
            textArea.text = lineInfo.text;
            SetVisibility(true);
            yield return new WaitForSeconds(lineInfo.duration);
            if (currentLine + 1 < dialog.lineInfosMorph.Count)
            {
                if (dialog.lineInfosMorph[currentLine + 1].delay > 1f)
                {
                    SetVisibility(false);
                }
            }
            else
            {
                SetVisibility(false);
            }
            textArea.text = "";
            ++currentLine;
        }
        yield return null;
        SetVisibility(false);

        toTell.Remove(dialogMorph);
        told.Add(dialogMorph);
    }
}
