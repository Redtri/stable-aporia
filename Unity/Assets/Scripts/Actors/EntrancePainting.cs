﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class EntrancePainting : MonoBehaviour
{
    public float duration;
    public Transform finalSpot;
    public Transform entranceSpot;
    public MeshRenderer fakeRender;
    public AnimationCurve fakeAppearCurve;
    public AnimationCurve targetCurve;
    public LightActor[] lights;

    public Material fakeMat;
    public Material frameMat;

    public MeshRenderer frameRender;
    public float backoffDist;
    public Transform canvas;
    public MeshRenderer render;
    public Transform hider;

    public Transform frame;
    public Transform target;
    public Transform blackhole;
    public Door entranceDoor;
    private Sequence sequence;
    public UnityEvent onStop;
    public AK.Wwise.Event shutLightEvent;
    public AK.Wwise.Event canvasEvent;
    public AK.Wwise.Event glitchAmb;
    public AK.Wwise.Event doorOpenEvent;
    
    
    public EffectProfile cutGlitchProfile;
    public EffectProfile glitchProfile;
    
    public void Backoff()
    {
        Sequence entranceSequence = DOTween.Sequence();

        shutLightEvent?.Post(gameObject);
        
        target.GetComponent<Pawn>().StopMove();
        Destroy(GetComponent<Collider>());
        //target.GetComponent<Rigidbody>().useGravity = false;
        canvasEvent?.Post(gameObject);
        render.material.SetFloat("Activated", 1f);
        render.enabled = true;
        hider.gameObject.SetActive(false);
        fakeRender.material = fakeMat;
        frameRender.material = frameMat;
        entranceSequence.Append(canvas.DOMove(canvas.position - transform.forward * backoffDist, duration/2f));

        entranceSequence.Join(fakeRender.material.DOFloat(1f, "Percent", duration/2f).SetEase(fakeAppearCurve));
        //fakeRender.gameObject.SetActive(false);
        entranceSequence.Join(frame.DOMove(frame.position - transform.forward * backoffDist, duration/2f));
        entranceSequence.Join(frame.DOScaleZ(frame.localScale.z + backoffDist * 5.55f, duration/2f));
        entranceSequence.Join(target.DOMove(entranceSpot.position, duration/2f));
        entranceSequence.Join(render.material.DOFloat(1f, "Percent", duration/2f));
        entranceSequence.Join(blackhole.DOScale(new Vector3(1f, 1f, 1f), duration / 2f));
        //Dissolve first canvas material
        //entranceSequence.Append()
        
        //Attract player
        
        entranceSequence.AppendCallback(() => entranceDoor.duration = duration/6f);
        entranceSequence.AppendCallback(() => entranceDoor.ExceptionOpen());
        entranceSequence.AppendCallback(() => doorOpenEvent?.Post(gameObject));
        entranceSequence.AppendCallback(() => EffectManager.instance.CutGlitch(cutGlitchProfile));
        entranceSequence.AppendCallback(() => glitchAmb?.Post(gameObject));
        entranceSequence.AppendCallback(() => PostProcessManager.instance.Effect(glitchProfile));
        entranceSequence.Append(frameRender.material.DOFloat(1f, "Percent", duration / 4f));
        entranceSequence.Join(target.DOMove(new Vector3(finalSpot.position.x, target.position.y, finalSpot.position.z), duration / 4f).SetEase(targetCurve)).onComplete += OnStop;
    
        }

    public void FadeInFakeCanvas(float tDuration)
    {
        //fakeRender.material.DOFloat(0f, "Percent", duration).SetEase(fakeAppearCurve);
        //frameRender.material.DOFloat(0f, "Percent", duration).SetEase(fakeAppearCurve);
        foreach (LightActor light in lights) {
            light.Trigger(tDuration);
        }
    }

    public void OnStop()
    {
        onStop?.Invoke();
        glitchAmb?.Stop(gameObject);
        canvasEvent?.Stop(gameObject);
    }
    
    public void Kill()
    {
        sequence?.Kill();
        transform.DOKill();
    }
}
