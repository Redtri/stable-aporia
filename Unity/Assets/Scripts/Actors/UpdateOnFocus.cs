﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum eUpdateHandlerType { Text, Renderer};
public enum eUpdateHandlerParamType { Color, Float};

[System.Serializable]
public class UpdateHandler
{
    public eUpdateHandlerType renderType;
    public eUpdateHandlerParamType paramType;
    public GameObject gameObject;
    public string paramName;
    public bool reversedPercent;
    [HideInInspector] public Material mat;
}

public class UpdateOnFocus : MonoBehaviour
{
    public UpdateHandler[] handlers;

    private void Start()
    {
        foreach (UpdateHandler handler in handlers) {
            Material newMat = null;

            switch (handler.renderType) {
                case eUpdateHandlerType.Renderer:
                    Renderer render = handler.gameObject.GetComponent<Renderer>();
                    newMat = Instantiate<Material>(render.material);
                    render.material = newMat;
                    break;
                case eUpdateHandlerType.Text:
                    Text txt = handler.gameObject.GetComponent<Text>();
                    newMat = Instantiate<Material>(txt.material);
                    txt.material = newMat;
                    break;
            }
            handler.mat = newMat;
        }
    }

    public void UpdateAlpha(float percent)
    {
        foreach(UpdateHandler handler in handlers) {
            if(handler.mat) {
                switch (handler.paramType) {
                    case eUpdateHandlerParamType.Color:
                        handler.mat.SetColor(handler.paramName, new Color(handler.mat.color.r, handler.mat.color.g, handler.mat.color.b, (handler.reversedPercent) ? 1f-percent : percent ));
                        break;
                    case eUpdateHandlerParamType.Float:
                        handler.mat.SetFloat(handler.paramName, (handler.reversedPercent) ? 1f - percent : percent);
                        break;
                }
            }
        }
    }
}
