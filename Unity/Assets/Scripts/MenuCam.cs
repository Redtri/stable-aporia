﻿using Cinemachine;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(PlayerController))]
public class MenuCam : MonoBehaviour
{
    public static MenuCam currentCam;

    // Start is called before the first frame update
    public Transform body;
    public Transform bodyPredict;
    public Transform cam;

    public AK.Wwise.Event focusEvent;
    public AK.Wwise.Event focusState;
    public AK.Wwise.Switch focusSuccess;
    public AK.Wwise.Switch focusFail;
    public FloatEvent vertigoUpdate;

    public float smooth;
    public Vector3 minClamp;
    public Vector3 maxClamp;
    public PlayerController controller;// { get; private set; }
    private Quaternion staticRotation;
    public bool focusing { get; private set; }
    public bool ready { get; private set; }
    public bool focusSuccessful { get; private set; }

    public PlayerData data;
    public PlayerData.InputInfos currInputInfos;

    private float xRotation = 0f;
    private float yRotation = 0f;
    private bool blocked;
    private bool splashScreen;

    private void Awake()
    {
        if (currentCam)
            Destroy(gameObject);
        else
            currentCam = this;
    }

    void Start()
    {
        controller = GetComponent<PlayerController>();
        currInputInfos = data.keyboard;
        Init();
    }

    private void Init()
    {
        SetStaticRotation(cam.rotation);
        splashScreen = true;
        controller.focusInput.onInvoke.AddListener(FocusEvents);
        controller.focusInput.onStart.AddListener(FocusEvents);
        controller.focusInput.onRelease.AddListener(FocusEvents);
    }

    private void OnDisable()
    {
        controller.focusInput.onInvoke.RemoveAllListeners();
        controller.focusInput.onStart.RemoveAllListeners();
        controller.focusInput.onRelease.RemoveAllListeners();
    }

    // Update is called once per frame
    void Update()
    {
        if (controller.usingKeyboard && currInputInfos != data.keyboard)
            currInputInfos = data.keyboard;
        else if (!controller.usingKeyboard && currInputInfos != data.gamepad)
            currInputInfos = data.gamepad;

        if(!blocked && !splashScreen) {
            Look();
        }else {
            cam.rotation = Quaternion.Slerp(cam.rotation, staticRotation, smooth * 0.25f);
        }
    }

    private bool focusPerformed;
    private void FocusEvents(InputActionPhase phase)
    { 
        if(!blocked) {

            switch (phase) {
                case InputActionPhase.Started:
                    //PostProcessManager.instance.ProcessEffect(1, 2f);
                    Vertigo(true);
                    focusEvent?.Post(gameObject);
                    break;
                case InputActionPhase.Performed:
                    RaycastHit hit;
                    focusPerformed = true;
                    if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, data.focusRange)) {
                        Interactable interactable = null;
                        if (hit.collider.TryGetComponent(out interactable) && interactable.Interact(null, eINTERACT_TYPE.Focus)) {
                            if (interactable.name == "Play")
                                focusSuccessful = true;
                            focusSuccess?.SetValue(gameObject);
                            break;
                        }
                    }

                    focusFail.SetValue(gameObject);
                    focusState?.Post(gameObject);
                    break;
                case InputActionPhase.Canceled:
                    //PostProcessManager.instance.ProcessEffect(0, 2f);
                    Vertigo(false);
                    focusEvent?.Stop(gameObject);
                    if (!focusPerformed) {
                        //Debug.Log("Fail");
                        focusFail?.SetValue(gameObject);
                        focusState?.Post(gameObject);
                    }
                    //Debug.Log("canceled");
                    focusPerformed = false;
                    break;
            }
        }
    }

    private void UpdateVertigo(Sequence sequence, bool minus)
    {
        float percent = cam.transform.localPosition.z / .5f;//sequence.ElapsedPercentage();
        vertigoUpdate?.Invoke(percent);
    }

    private void Vertigo(bool on)
    {
        Sequence seq = DOTween.Sequence();
        float currentFOV = cam.GetComponent<CinemachineVirtualCamera>().m_Lens.FieldOfView;
        focusing = true;
        if (on)
        {
            cam.transform.DOKill();
            cam.transform.DOLocalMove(new Vector3(cam.transform.localPosition.x, cam.transform.localPosition.y, .5f), 2f);
            seq.Join(DOVirtual.Float(currentFOV, 85f, 2f, value => cam.GetComponent<CinemachineVirtualCamera>().m_Lens.FieldOfView = value)).onUpdate += () => UpdateVertigo(seq, false);
        }
        else if(!focusSuccessful)
        {
            cam.transform.DOKill();
            cam.transform.DOLocalMove(new Vector3(cam.transform.localPosition.x, cam.transform.localPosition.y, 0f), 2f);
            seq.Join(DOVirtual.Float(currentFOV, 60f, 2f, value => cam.GetComponent<CinemachineVirtualCamera>().m_Lens.FieldOfView = value)).onUpdate += () => UpdateVertigo(seq, true);
        }
        seq.onComplete += () => focusing = false;
    }

    private void Look()
    {
        float xLook = controller.lookInput.x * currInputInfos.lookSensitivity * Time.deltaTime;
        float yLook = controller.lookInput.y * currInputInfos.lookSensitivity * Time.deltaTime;

        xRotation -= yLook;
        xRotation = Mathf.Clamp(xRotation, currInputInfos.clampLook.x, currInputInfos.clampLook.y);

        Quaternion bodyRotation = Quaternion.identity;
        Quaternion camRotation = Quaternion.identity;

        camRotation = Quaternion.Slerp(cam.transform.localRotation, Quaternion.Euler(xRotation, 0f, 0f), smooth);

        yRotation += xLook;
        yRotation = Mathf.Clamp(yRotation, -90f, 90f);

        bodyRotation = Quaternion.Slerp(body.transform.rotation, Quaternion.Euler(0f,yRotation,0f), smooth);

        cam.transform.localRotation = camRotation;
        bodyPredict.Rotate(Vector3.up, xLook);
        body.transform.rotation = bodyRotation;
    }
    public void Block()
    {
        blocked = true;
    }

    public void UnSplash()
    {
        splashScreen = false;
        ready = true;
    }

    public void SetReady(bool inReady)
    {
        ready = inReady;
    }

    public void Free()
    {
        blocked = false;
    }

    public void SetStaticRotation(Quaternion rotation)
    {
        staticRotation = rotation;
    }
}
