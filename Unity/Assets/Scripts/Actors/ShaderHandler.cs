﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ShaderHandler : MonoBehaviour
{
    [System.Serializable]
    public class MatHandler
    {
        public string valueName;
        public Renderer render;
        public float finalValue;
    }

    public MatHandler[] matHandlers;
    
    public void Interp(float duration)
    {
        foreach (MatHandler matHandler in matHandlers)
        {
            if (!matHandler.render)
                matHandler.render = GetComponent<Renderer>();
            matHandler.render.material.EnableKeyword(matHandler.valueName);
            matHandler.render.material.DOFloat(matHandler.finalValue, matHandler.valueName, duration);
        }
    }
}
