﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Translator : MonoBehaviour
{
    public List<TranslateActor> translateActors;

    public void TranslateActors()
    {
        foreach (TranslateActor translateActor in translateActors)
        {
            translateActor.Translate();
        }
    }
}
