﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Events;

public class SplashIntro : MonoBehaviour
{
    public PlayerInput playerInput;
    public Image blackScreen;
    public Image background;
    public FadeablePara[] paragraphs;
    public bool triggerOnStart;
    [System.Serializable]
    public class FadeablePara
	{
        public Text txtObject;
        public float fadeDuration;
	}

    [SerializeField] float endFadeDuration;
    [SerializeField] AnimationCurve fadeCurve;
    [SerializeField] float startScale;
    [SerializeField] float comeInDuration;
    public UnityEvent onSplashEnd;

    private int currentParagraph;

    private void Awake()
    {
        foreach(FadeablePara para in paragraphs) {
            para.txtObject.color = new Color(para.txtObject.color.r, para.txtObject.color.g, para.txtObject.color.b, 0f);
        }
        currentParagraph = 0;
    }

    // Start is called before the first frame update
    void Start()
    {

        if(triggerOnStart) {
            Trigger();
        }
        playerInput = FindObjectOfType<PlayerInput>();
        playerInput.currentActionMap["Interact"].performed += OnInteract;
        playerInput.currentActionMap["Esc"].performed += OnEsc;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public int GetTotalDurationMillis()
	{
        int count = 0;

        foreach (FadeablePara para in paragraphs)
            count += (int)(para.fadeDuration * 1000);
        return count;
	}

    private void OnInteract(InputAction.CallbackContext context)
	{
        SkipCurrent();
	}

    private void OnEsc(InputAction.CallbackContext context)
	{
        Application.Quit();
	}

    Sequence paraSequence = null;

    public void Trigger()
    {
        if(currentParagraph < paragraphs.Length) {
            if (currentParagraph == 0)
                background.DOFade(1f, 2f);
            FadeablePara para = paragraphs[currentParagraph];

            paraSequence = DOTween.Sequence();
            para.txtObject.transform.localScale = Vector3.one * startScale;
            paraSequence.Join(para.txtObject.transform.DOScale(1f, comeInDuration));
            Tween fadeTween = para.txtObject.DOFade(1f, para.fadeDuration).SetEase(fadeCurve);

            if(para.txtObject.text.Contains("<color"))
            {
                fadeTween.onUpdate += () =>
                {
                    int subId = para.txtObject.text.IndexOf("<color");
                    string extracted = para.txtObject.text.Substring(subId, 17);
                    float percent = fadeCurve.Evaluate((fadeTween.Elapsed() / fadeTween.Duration()));
                    para.txtObject.text = para.txtObject.text.Replace(extracted, "<color=#" + ColorUtility.ToHtmlStringRGBA(new Color(1f, 0f, 0f, percent)) + ">");// )) + ">");
                };
            }


            paraSequence.Join(fadeTween);
            paraSequence.AppendCallback(() => currentParagraph++);
            paraSequence.AppendCallback(() => Trigger());
        }
        else {
            Debug.Log("Finished splash intro");
            blackScreen.DOFade(1f, endFadeDuration).SetEase(fadeCurve).onComplete += ( () => onSplashEnd.Invoke() );
        }
    }

    private void SkipCurrent()
	{
        paraSequence.Complete(true);
	}
}
