﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct DeviceResponsive
{
    public Sprite[] values;
}

[System.Serializable]
public class ImageLanguageResponsive : MonoBehaviour
{
    public DeviceResponsive[] values;
    private int index1;
    private int index2;
    private Image img;

    private void Start()
    {
        img = GetComponent<Image>();
    }

    public void SetImage(int index)
    {
        if(img && index < values.Length)
        {
            index1 = index;
            img.sprite = values[index2].values[index];
        }
    }
    public void SetDevice(int index)
    {
        if(img && index < values.Length)
        {
            index2 = index;
            img.sprite = values[index].values[index1];
        }
    }
}
