﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEditor;
using System;

[System.Serializable]
public class InteractEvent : UnityEvent<Interactable> {}

[Flags]
public enum eINTERACT_TYPE
{
    None = 0,
    Focus = 1,
    Manual = 2,
    Everything = Focus | Manual
}


public class Interactable : MonoBehaviour
{
    [HideInInspector] public Pawn user;
    public eINTERACT_TYPE useModality;
    public int maxUse;
    protected int nbUse;

    public InteractEvent onTryInteract;
    [SerializeField] protected bool onTryEnabled = true;
    public AK.Wwise.Event tryInteractEvent;
    public InteractEvent onSuccesInteract;
    [SerializeField] protected bool onSuccesEnabled = true;
    public AK.Wwise.Event successInteractEvent;
    public InteractEvent onFailInteract;
    [SerializeField] protected bool onFailEnabled = true;
    public AK.Wwise.Event failInteractEvent;

    private void Awake()
    {
        if (useModality.ToString() == eINTERACT_TYPE.None.ToString())
            useModality = eINTERACT_TYPE.Manual;
    }

    public virtual bool Interact(Pawn pawnUser, eINTERACT_TYPE interactType, bool start = true)
    {
        if ((interactType & useModality) != eINTERACT_TYPE.None)
        {
            if(onTryEnabled)
                onTryInteract?.Invoke(this);
            tryInteractEvent?.Post(gameObject);
            if (maxUse > 0) {
                if (nbUse < maxUse) {
                    if(onSuccesEnabled)
                        onSuccesInteract?.Invoke(this);
                    successInteractEvent?.Post(gameObject);
                    ++nbUse;
                    return true;
                }
                if(onFailEnabled)
                    onFailInteract?.Invoke(this);
                failInteractEvent?.Post(gameObject);
                return false;
            }else if(maxUse < 0) {
                if (onFailEnabled)
                    onFailInteract?.Invoke(this);
                failInteractEvent?.Post(gameObject);
                return false;
            }
            if(onSuccesEnabled)
                onSuccesInteract?.Invoke(this);
            successInteractEvent?.Post(gameObject);
            return true;
        }

        return false;
    }

    public void ActivateOnSuccess(bool active)
    {
        onSuccesEnabled = active;
    }

    public void ActivateOnFailure(bool active)
    {
        onFailEnabled = active;
    }
    
    public void ActivateOnTry(bool active)
    {
        onTryEnabled = active;
    }
}