﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.Assertions.Comparers;
using System.Linq;
using DG.Tweening;
using UnityEditor;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using Cinemachine;
using UnityEngine.VFX;

public enum eFOOTSTEP{ Wood, Carpet, Concrete, Reverse, Liquid}

public class Pawn : MonoBehaviour
{
    [Header("EDITOR")]
    public bool debugLines;

    [Header("PARAMETERS")]
    public PlayerData data;
    public PlayerData.InputInfos currInputInfos;

    public Action focus;

    private bool blocked;
    private bool floating;

    [Header("COMPONENTS")] public PlayerController controller;
    public Rigidbody rigidBody;
    public Camera cam;
    public Transform phoneHandle;
    public Image bar;
    public Image blackScreen;
    public Transform rotationPredict;

    [Header("WWISE")]
    public AK.Wwise.Event breath;
    public AK.Wwise.Event[] footsteps;
    public AK.Wwise.Event gravity;
    public AK.Wwise.Event focusEvent;
    public AK.Wwise.Event focusState;
    public AK.Wwise.Switch focusSuccess;
    public AK.Wwise.Switch focusFail;
    public AK.Wwise.Event cutIn;

    private Pickable pickable;

    private float xRotation = 0f;
    private Transform fromPortal;
    private Transform toPortal;

    [HideInInspector] public Interactable interactable;

    private bool wasInteracting;
    private eFOOTSTEP prevFootstepType;
    private eFOOTSTEP footstepType;

    public static Pawn controlledPawn;

    
    private void Awake()
    {
        if (controlledPawn)
            Destroy(gameObject);
        else
            controlledPawn = this;
    }

    public void Control()
    {
        if (controlledPawn != this)
        {
            if (controlledPawn)
                controlledPawn.gameObject.SetActive(false);
            controlledPawn = this;
            controlledPawn.gameObject.SetActive(true);
        }
    }

    private void OnDisable()
    {
        controller.focusInput.onInvoke.RemoveAllListeners();
    }
    
    private void Start()
    {
        Init();
    }

    private void Init()
    {
        wasInteracting = false;
        alive = true;
        controller.focusInput.onInvoke.AddListener(FocusEvents);
        controller.focusInput.onStart.AddListener(FocusEvents);
        controller.focusInput.onRelease.AddListener(FocusEvents);

        breath.Post(gameObject);
    }

    private bool alive = true;
    private bool canMove = true;

    private bool wasEscape = false;
    
    // Update is called once per frame
    void Update()
    {
        if(controller.escapeInput)
        {
            if (!wasEscape)
            {
                wasEscape = true;
                GameManager.instance.Escape();
            }
        }
        else
        {
            if (wasEscape)
                wasEscape = false;

            if (controller.usingKeyboard && currInputInfos != data.keyboard)
                currInputInfos = data.keyboard;
            else if (!controller.usingKeyboard && currInputInfos != data.gamepad)
                currInputInfos = data.gamepad;

            Look();
            if (!blocked)
            {
                if (canMove)
                    Move();
                Focus();
            }
            else if(!lookAt)
            {
                //Debug.Log("blocked");
                cam.GetComponent<Animator>().SetFloat("speed", 0f);
                if (!floating && !attractor)
                {
                    cam.transform.localRotation = Quaternion.Slerp(cam.transform.localRotation, Quaternion.Euler(xRotation, 0f, 0f), smoothstep);
                }
            }

            if (alive)
                Interact();
        }
    }

    #region UI

    public void FadeIn(float duration)
    {
        
        blackScreen.DOFade(1f, duration);
    }

    public void FadeOut(float duration)
    {
        EffectManager.instance.FadeOut(duration);
    }


    public void CutIn()
    {
        cutIn?.Post(gameObject);
        blackScreen.color = new Color(0,0,0,1);
    }
    #endregion
    
    private void Interact()
    {
        if (controller.interactInput)
        {
            if (!wasInteracting)
            {
                wasInteracting = true;
                RaycastHit hit;
                Debug.DrawLine(cam.transform.position, cam.transform.position + cam.transform.forward * data.interactRange, Color.cyan);
                if (interactable && interactable.GetComponent<Phone>())
                {
                    interactable.Interact(this, eINTERACT_TYPE.Manual);
                    interactable = null;
                } else {
                    if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, data.interactRange))
                    {
                        //Debug.Log(hit.collider.gameObject.name);
                        Interactable tmpInteractable = hit.collider.transform.GetComponentInParent<Interactable>();
                        //hit.collider.TryGetComponent(out tmpInteractable);

                        if (tmpInteractable)
                        {
                            interactable = tmpInteractable;
                            //Debug.Log("Interact with object");
                            interactable.Interact(this, eINTERACT_TYPE.Manual);
                        }else if ((tmpInteractable = hit.collider.transform.GetComponent<Interactable>()) != null)
                        {
                            //Debug.Log("Interact with object");
                            interactable = tmpInteractable;
                            interactable.Interact(this, eINTERACT_TYPE.Manual);
                        }
                    }
                }
            }
        }
        else
        {
            wasInteracting = false;
        }
    }

    public void SetFootstep(int newFootstepType)
    {
        prevFootstepType = footstepType;
        footstepType = (eFOOTSTEP)newFootstepType;
    }

    public void RevertFootstep()
    {
        eFOOTSTEP oldFootstep = prevFootstepType;
        prevFootstepType = footstepType;
        footstepType = oldFootstep;
    }

    private float interpTime;
    private Vector3 lastNormal;
    private bool wasMoving;
    private Quaternion baseRotation;
    public float slowMultiplier;
    private void Move()
    {
        float xMove = controller.movementInput.x;
        float yMove = controller.movementInput.y;

        if (!attractor)
        {
            if (floating)
            {
                yMove *= 0.5f;
                rigidBody.velocity = Vector3.Lerp(rigidBody.velocity, new Vector3(0f, 0.24f, 0f), 0.125f);
            }

            Vector3 inputMovement = transform.right * xMove + transform.forward * yMove;
            Debug.DrawLine(transform.position, transform.position + transform.right, Color.red);
            Vector3 movement = inputMovement *
                               ((floating) ? currInputInfos.floatSpeed : currInputInfos.moveSpeed * slowMultiplier) *
                               Time.fixedDeltaTime;

            Vector3 newVelocity = transform.TransformDirection(movement);
            
            if (!floating)
            {
                RaycastHit wallHit;
                if (Physics.Raycast(cam.transform.position, cam.transform.forward, out wallHit, data.wallWalkRange))
                {
                    if (!wasJumping && wallHit.collider.CompareTag("Walkable"))
                    {
                        StartCoroutine(WalkOnWall());
                    }
                }

                RaycastHit carpetHit;
                if (Physics.Raycast(transform.position, -transform.up, out carpetHit, 1.5f))
                {
                    if (carpetHit.collider.CompareTag("Carpet"))
                    {
                        if (footstepType != eFOOTSTEP.Carpet)
                            SetFootstep((int) eFOOTSTEP.Carpet);
                    }
                    else
                    {
                        if (footstepType == eFOOTSTEP.Carpet)
                            RevertFootstep();
                    }
                }

                if (newVelocity.magnitude > 0f && movement.magnitude > 0f)
                {
                    //Debug.Log(newVelocity.magnitude);
                    cam.GetComponent<Animator>().SetFloat("speed", inputMovement.magnitude);
                }
                else
                {
                    cam.GetComponent<Animator>().SetFloat("speed", 0f);
                }
            }

            newVelocity = new Vector3(Mathf.Clamp(movement.x, -currInputInfos.moveSpeed, currInputInfos.moveSpeed),
                Mathf.Clamp(movement.y, -currInputInfos.moveSpeed, currInputInfos.moveSpeed),
                Mathf.Clamp(movement.z, -currInputInfos.moveSpeed, currInputInfos.moveSpeed));
            Debug.DrawLine(transform.position, transform.position + newVelocity, Color.black);
            if (newVelocity != Vector3.zero)
            {
                rigidBody.MovePosition(transform.position + newVelocity);
            }
        }
    }

    public void Footstep()
    {
        footsteps[(int) footstepType].Post(gameObject);
    }

    private bool wasJumping;

    private IEnumerator WalkOnWall()
    {
        RaycastHit hit;
        Debug.DrawLine(transform.position, transform.position +(-transform.up + transform.forward*2f) * 4f, Color.red);
        if (Physics.Raycast(transform.position, transform.forward, out hit, data.wallWalkRange))
        {
            BoxCollider boxCollider = hit.collider as BoxCollider;
            if (boxCollider && hit.collider.CompareTag("Walkable") && !wasJumping)
            {
                wasJumping = true;
                gravity?.Post(gameObject);
                BlockPawn();
                Vector3 forward = hit.normal;
                if (forward != lastNormal)
                {
                    interpTime = Time.time;
                    baseRotation = transform.rotation;
                    lastNormal = forward;
                }
                Physics.gravity = - hit.normal * 10f;
                Quaternion rotation;
                while (Time.time - interpTime < currInputInfos.normalInterp)
                {
                    rotation = Quaternion.Lerp(baseRotation, Quaternion.FromToRotation(transform.up, hit.normal) * transform.rotation, (Time.time-interpTime)/currInputInfos.normalInterp);

                    transform.rotation = rotation;
                    yield return new WaitForEndOfFrame();
                }
                rotation = Quaternion.FromToRotation(transform.up, hit.normal) * transform.rotation;
                transform.rotation = rotation;
                rotationPredict.localRotation = transform.rotation;
                FreePawn();
            }
        }

        wasJumping = false;
        yield return null;
    }
    
    public float smoothstep;
    public bool portaled;
    public float attractorSmooth;
    
    private void Look()
    {
        float xLook = controller.lookInput.x * currInputInfos.lookSensitivity * Time.deltaTime;
        float yLook = controller.lookInput.y * currInputInfos.lookSensitivity * Time.deltaTime;

        if (!lookAt)
        {
            if (floating && attractor)
            {
                CinemachineVirtualCamera virtualCam = cam.GetComponent<CinemachineVirtualCamera>();
                float currentFOV = virtualCam.m_Lens.FieldOfView;

                cam.transform.localPosition = Vector3.Lerp(cam.transform.localPosition, new Vector3(0f, 0.5f, 0f), smoothstep);
                virtualCam.m_Lens.FieldOfView = Mathf.Lerp(currentFOV, 60f, smoothstep);
            } else
            {
                if (!blocked)
                {
                    xRotation -= yLook;
                    xRotation = Mathf.Clamp(xRotation, currInputInfos.clampLook.x, currInputInfos.clampLook.y);

                    Quaternion bodyRotation = Quaternion.identity;
                    Quaternion camRotation = Quaternion.identity;

                    camRotation = Quaternion.Slerp(cam.transform.localRotation, Quaternion.Euler(xRotation, 0f, 0f),
                        smoothstep);

                    bodyRotation = Quaternion.Slerp(transform.rotation, rotationPredict.localRotation, smoothstep);
            
                    if (!portaled)
                    {
                        rotationPredict.Rotate(Vector3.up, xLook);
                        if (!attractor)
                        {
                            transform.rotation = bodyRotation;
                        }

                    } else //Not smoothing up camera movements when overlapping portals
                    {
                        //Debug.Log("Portaled");
                        transform.Rotate(Vector3.up, xLook);
                        rotationPredict.localRotation = transform.rotation;
                    }
                    cam.transform.localRotation = camRotation;
                }
        
                if(attractor && !floating)
                {
                    //Debug.Log("being attracted " + blocked);
                    Vector3 rotation = Quaternion.LookRotation(attractor.position - transform.position).eulerAngles;
                    rotation.x = rotation.z = 0f;
                 
                    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(rotation), smoothstep);
                    cam.transform.localRotation = Quaternion.Slerp(cam.transform.localRotation, Quaternion.Euler(Vector3.zero), smoothstep);
                    //transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.eulerAngles.x, cam.transform.localRotation.eulerAngles.y, transform.localRotation.eulerAngles.z));
                }
            }
        }
        else {
            cam.transform.rotation = Quaternion.Slerp(cam.transform.rotation, Quaternion.LookRotation(lookAt.position - cam.transform.position), smoothstep*0.25f);
        }
    }

    private void Focus()
    {
        float alpha = 0f;
        if (controller.focusInput.enabled)
        {
            switch (controller.focusInput.phase)
            {
                case InputActionPhase.Canceled:
                    alpha = 1f-controller.focusInput.releasePercent();
                    break;
                case InputActionPhase.Started:
                    alpha = controller.focusInput.invokePercent();
                    break;
                case InputActionPhase.Performed:
                    alpha = controller.focusInput.invokePercent();
                    break;
                default:
                    //alpha = controller.focusInput.getPercent(controller.focusInput.phase);
                    break;
            }
            
            //bar.fillAmount = Mathf.Clamp(alpha, 0f, 1f);
        }
    }

    private bool focusPerformed;

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<Portal>() && (controller.focusInput.phase == InputActionPhase.Started || controller.focusInput.phase == InputActionPhase.Performed)) {
            controller.Interrupt();
        }
    }

    private void FocusEvents(InputActionPhase phase)
    {
        if (!blocked && !portaled)
        {
            switch (phase)
            {
                case InputActionPhase.Started:
                    PostProcessManager.instance.ProcessEffect(1, 2f);
                    Vertigo(true);
                    focusEvent?.Post(gameObject);
                    break;
                case InputActionPhase.Performed:
                    RaycastHit hit;
                    focusPerformed = true;
                    if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, data.focusRange))
                    {
                        Interactable interactable = null;
                        if (hit.collider.TryGetComponent(out interactable) && interactable.Interact(this, eINTERACT_TYPE.Focus))
                        {
                            focusSuccess?.SetValue(gameObject);
                            break;
                        }
                    }
                    
                    focusFail.SetValue(gameObject);
                    focusState?.Post(gameObject);
                    break;
                case InputActionPhase.Canceled:
                    PostProcessManager.instance.ProcessEffect(0, 2f);
                    Vertigo(false);
                    focusEvent?.Stop(gameObject);
                    if (!focusPerformed)
                    {
                        //Debug.Log("Fail");
                        focusFail?.SetValue(gameObject);
                        focusState?.Post(gameObject);
                    }
                    //Debug.Log("canceled");
                    focusPerformed = false;
                    break;
            }
        }
    }

    #region Effects
    public void Vertigo(bool on)
    {
        float currentFOV = cam.GetComponent<CinemachineVirtualCamera>().m_Lens.FieldOfView;
        if (on)
        {
            cam.transform.DOLocalMove(new Vector3(cam.transform.localPosition.x, cam.transform.localPosition.y, 1f), 2f);
            DOVirtual.Float(currentFOV, 85f, 2f, value => cam.GetComponent<CinemachineVirtualCamera>().m_Lens.FieldOfView = value);
        }
        else
        {
            cam.transform.DOLocalMove(new Vector3(cam.transform.localPosition.x, cam.transform.localPosition.y, 0f), 2f);
            DOVirtual.Float(currentFOV, 60f, 2f, value => cam.GetComponent<CinemachineVirtualCamera>().m_Lens.FieldOfView = value);
        }
    }

    public void VertigoOut(bool on)
    {
        float currentFOV = cam.GetComponent<CinemachineVirtualCamera>().m_Lens.FieldOfView;
        if (on)
        {
            cam.transform.DOLocalMove(new Vector3(cam.transform.localPosition.x, cam.transform.localPosition.y, -1f), 2f);
            DOVirtual.Float(currentFOV, 35f, 2f, value => cam.GetComponent<CinemachineVirtualCamera>().m_Lens.FieldOfView = value);
        }
        else
        {
            cam.transform.DOLocalMove(new Vector3(cam.transform.localPosition.x, cam.transform.localPosition.y, 0f), 2f);
            DOVirtual.Float(currentFOV, 60f, 2f, value => cam.GetComponent<CinemachineVirtualCamera>().m_Lens.FieldOfView = value);
        }
    }

    #endregion
    
    public void TeletportTo(Transform trsf)
    {
        transform.position = trsf.position;
        transform.rotation = trsf.rotation;
        rotationPredict.localRotation = trsf.localRotation;
    }

    public void SetPos(Transform targ)
    {
        transform.position = targ.position;
    }

    public void KillPlayer()
    {
        alive = false;
    }

    public void StopMove()
    {
        canMove = false;
    }

    public void CanMove()
    {
        canMove = true;
    }
    
    public void BlockPawn()
    {
        blocked = true;
        lastSlow = slowMultiplier;
        //noCam = true;
        //Debug.Log("Block");
    }

    private float lastSlow;

    public void FreePawn()
    {
        blocked = false;
        noCam = false;
        attractor = null;
        slowMultiplier = lastSlow;
    }

    public void MoveForward(float duration, float dist, bool free = true)
    {
        StartCoroutine(ForwardCo(duration, dist, free));
    }

    private IEnumerator ForwardCo(float duration, float dist, bool free = true)
    {
        float time = Time.fixedTime;
        while (Time.fixedTime - time <= duration)
        {
            rigidBody.MovePosition(transform.position + (transform.forward * (dist/duration) * Time.fixedDeltaTime));
            cam.transform.localPosition = Vector3.Lerp(cam.transform.localPosition, new Vector3(0f, 0.5f, 0f), smoothstep);
            yield return new WaitForFixedUpdate();
        }
        if(free)
            FreePawn();
    }

    public void PushBack(float duration, Vector3 force, EffectProfile stopProfile)
    {
        StartCoroutine(BackCo(duration, force, stopProfile));
    }

    public VisualEffect darknessVFX;
    public void Darkness()
    {
        darknessVFX.SendEvent("Play");
    }

    private IEnumerator BackCo(float duration, Vector3 force, EffectProfile stopProfile)
    {
        BlockPawn();
        rigidBody.AddForce(force, ForceMode.Impulse);
        PostProcessManager.instance.Effect(stopProfile);
        yield return new WaitForSeconds(duration);
        darknessVFX.Stop();
        FreePawn();
    }

    private Transform attractor;
    private Quaternion lookAtRot;
    
    public void Attract(Transform attracter)
    {
        attractor = attracter;
    }
    
    public void ReverseGravity()
    {
        if (!floating)
        {
            floating = true;
            rigidBody.useGravity = false;
            rigidBody.AddForce(Vector3.up * 15f, ForceMode.Impulse);
        }
        else
        {
            floating = false;
            rigidBody.useGravity = true;
            FreePawn();
            CinemachineVirtualCamera virtualCamera = cam.GetComponent<CinemachineVirtualCamera>();
            CinemachineBasicMultiChannelPerlin perlinNoise = virtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

            virtualCamera.enabled = true;
            DOVirtual.Float(0.0f, 0.5f, 0.1f, x => perlinNoise.m_FrequencyGain = x);
            DOVirtual.Float(0.0f, 0.25f, 0.1f, x => perlinNoise.m_AmplitudeGain = x);
        }
    }

    private bool noCam;
    
    public void Perpendicular(float duration)
    {
        BlockPawn();
        Sequence perpSequence = DOTween.Sequence();
        
        perpSequence.Join(transform.DORotate(new Vector3(270f, 0f, 90f), duration).SetEase(Ease.InOutSine));
        perpSequence.Join(rotationPredict.DOLocalRotate(new Vector3(270f, 0f, 90f), duration).SetEase(Ease.InOutSine));
        perpSequence.Join(cam.transform.DOLocalRotate(new Vector3(0f, 0f, 0f), duration).SetEase(Ease.InOutSine)).onComplete += () => StartCoroutine(FreezeCam(duration));

        CinemachineVirtualCamera virtualCamera = cam.GetComponent<CinemachineVirtualCamera>();
        CinemachineBasicMultiChannelPerlin perlinNoise = virtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

        perpSequence.Join(DOVirtual.Float(0.5f, 0.0f, duration/2f, x => perlinNoise.m_FrequencyGain = x));
        perpSequence.Join(DOVirtual.Float(0.25f, 0.0f, duration/2f, x => perlinNoise.m_AmplitudeGain = x));

        perpSequence.AppendCallback(() => virtualCamera.enabled = false);
    }

    public void AskFreezeCam(float duration)
    {
        StartCoroutine(FreezeCam(duration));
    }

    private IEnumerator FreezeCam(float duration)
    {
        float freezeTime = Time.time;
        while (Time.time - freezeTime < duration)
        {
            //cam.transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
            cam.transform.localRotation = Quaternion.Slerp(cam.transform.localRotation, Quaternion.Euler(new Vector3(0f, 0f, 0f)), smoothstep);
            yield return new WaitForEndOfFrame();
        }
        
        yield return null;
    }

    public void Breath(bool active)
    {
        if (active)
            breath.Post(gameObject);
        else
            breath.Stop(gameObject);
    }

    private Transform lookAt;
    public void LookAt(Transform trsf)
    {
        lookAt = trsf;
    }

#if  UNITY_EDITOR
    
    private void OnDrawGizmos()
    {
        if (debugLines)
        {
            Vector3 startLine, endLine;
            //FOCUS ABILITY
            Handles.color = Color.cyan;
            startLine = (transform.position + transform.up * 0.1f);
            endLine = startLine + (transform.forward * data.focusRange + transform.up * 0.1f);
            Handles.Label(endLine,"Focus range");
            Handles.DrawLine(startLine, endLine);

            //INTERACT ABILITY
            Handles.color = Color.red;
            startLine = (transform.position);
            endLine = startLine + (transform.forward * data.interactRange);
            Handles.Label(endLine,"Interact range");
            Handles.DrawLine(startLine, endLine);
        
            //WALLWALK ABILITY
            Handles.color = Color.green;
            startLine = (transform.position - transform.up * 0.1f);
            endLine = startLine + (transform.forward * data.wallWalkRange - transform.up * 0.1f);
            Handles.Label(endLine,"WallWalk range");
            Handles.DrawLine(startLine, endLine);
        }
    }
#endif
}

#if UNITY_EDITOR
[CustomEditor(typeof(Pawn))]
public class PawnEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Pawn script = (Pawn) target;
        //Gizmos.DrawGUITexture(new Rect(script.cam.transform.position, Vector2.one * 4f), new Texture2D(4, 4, TextureFormat.R8, true));
    }
}
#endif