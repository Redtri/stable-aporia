﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.Rendering;
using DG.Tweening;
using Kino.PostProcessing;
using UnityEngine.PlayerLoop;

public class PPHandler
{
    public Vignette vignette;
    public Exposure exposure;
    public LensDistortion lensDistortion;
    public FilmGrain grain;
    public DepthOfField depthOfField;
    public ColorAdjustments colAdjust;
    public Glitch glitch;
    public Bloom bloom;
}

public class PostProcessManager : MonoSingleton<PostProcessManager>
{
    public Volume volume;
    public List<VolumeProfile> profiles;
    private VolumeProfile current;
    private VolumeProfile target;

    private PPHandler currHandler;
    private PPHandler targHandler;

    void Start()
    {
        Init();
    }

    private void Init()
    {
        current = volume.profile;
        target = null;
        currHandler = new PPHandler();
        targHandler = new PPHandler();
        LoadProfile(current, false);
    }

    private void Update()
    {
        
    }

    private void LoadProfile(VolumeProfile profile, bool isTarget)
    {
        if (isTarget)
        {
            profile.TryGet(out targHandler.exposure);
            profile.TryGet(out targHandler.vignette);
            profile.TryGet(out targHandler.lensDistortion);
            profile.TryGet(out targHandler.grain);
            profile.TryGet(out targHandler.glitch);
            //profile.TryGet(out targHandler.depthOfField);
            profile.TryGet(out targHandler.colAdjust);
            profile.TryGet(out targHandler.bloom);
        } else {
            profile.TryGet(out currHandler.exposure);
            profile.TryGet(out currHandler.vignette);
            profile.TryGet(out currHandler.lensDistortion);
            profile.TryGet(out currHandler.grain);
            profile.TryGet(out currHandler.glitch);
            //profile.TryGet(out currHandler.depthOfField);
            profile.TryGet(out currHandler.colAdjust);
            profile.TryGet(out currHandler.bloom);
        }
    }

    public void ProcessEffect(int index, float duration)
    {
        if (index < profiles.Count)
        {
            Sequence ppSequence = DOTween.Sequence();
            
            target = profiles[index];
            LoadProfile(target, true);

            DOVirtual.Float(currHandler.vignette.intensity.value, targHandler.vignette.intensity.value, duration, (float x) => currHandler.vignette.intensity.value = x); 
            DOVirtual.Float(currHandler.grain.intensity.value, targHandler.grain.intensity.value, duration, (float x) => currHandler.grain.intensity.value = x); 
            //DOVirtual.Float(currHandler.lensDistortion.intensity.value, targHandler.lensDistortion.intensity.value, duration, (float x) => currHandler.lensDistortion.intensity.value = x);
            //DOVirtual.Float(currHandler.grain.response.value, currHandler.grain.response.value, duration, (float x) => currHandler.grain.response.value = x); 

        }
    }

    private int desatCount = 0;

    public void Effect(EffectProfile profile)
    {
        float currJitter = currHandler.glitch.jitter.value;
        float currDrift = currHandler.glitch.drift.value;
        float currContrast = currHandler.colAdjust.contrast.value;
        float currSaturation = currHandler.colAdjust.saturation.value;
        float currLens = currHandler.lensDistortion.intensity.value;
        float currGrain = currHandler.grain.intensity.value;
        float currBloomInt = currHandler.bloom.intensity.value;
        float currBloomScatter = currHandler.bloom.scatter.value;
        float currGlitchShake = currHandler.glitch.shake.value;
        float currDestruction = currHandler.glitch.destruction.value;
        float currVignette = currHandler.vignette.intensity.value;

        switch (profile.profileType)
        {
            case eEFFECT_PROFILE_TYPE.Glitch:
                if (profile.additive)
                {
                    DOVirtual.Float(currJitter, profile.intensity, profile.duration, arg0 => currHandler.glitch.jitter.value = arg0).SetEase(profile.curve);
                    DOVirtual.Float(currDrift, profile.extraParam1, profile.duration, arg0 => currHandler.glitch.drift.value = arg0).SetEase(profile.curve);
                }
                else
                {
                    DOVirtual.Float(currJitter, currJitter-profile.intensity, profile.duration, arg0 => currHandler.glitch.jitter.value = arg0).SetEase(profile.curve);
                    DOVirtual.Float(currDrift, currDrift-profile.intensity, profile.duration, arg0 => currHandler.glitch.drift.value = arg0).SetEase(profile.curve);
                }
                break;
            case eEFFECT_PROFILE_TYPE.Desat:
                if (profile.additive)
                {
                    if(profile.steps > 0)
                    {
                        if(desatCount < profile.steps)
                        {
                            ++desatCount;
                            DOVirtual.Float(currContrast, currContrast + (profile.extraParam1 / profile.steps), profile.duration, (float x) => currHandler.colAdjust.contrast.value = x).SetEase(profile.curve);
                            DOVirtual.Float(currSaturation, currSaturation + (profile.extraParam2 / profile.steps), profile.duration, (float x) => currHandler.colAdjust.saturation.value = x).SetEase(profile.curve);
                        }
                    }
                    else
                    {
                        DOVirtual.Float(currContrast, profile.extraParam1, profile.duration, (float x) => currHandler.colAdjust.contrast.value = x).SetEase(profile.curve);
                        DOVirtual.Float(currSaturation, profile.extraParam2, profile.duration, (float x) => currHandler.colAdjust.saturation.value = x).SetEase(profile.curve);
                    }
                }
                else
                {
                    Debug.Log("Desat");
                    DOVirtual.Float(currContrast, currContrast - (profile.extraParam1), profile.duration, (float x) => currHandler.colAdjust.contrast.value = x).SetEase(profile.curve);
                    DOVirtual.Float(currSaturation, currSaturation - (profile.extraParam2), profile.duration, (float x) => currHandler.colAdjust.saturation.value = x).SetEase(profile.curve);
                }
                break;
            case eEFFECT_PROFILE_TYPE.Blackhole:
                
                if (profile.additive)
                {
                    DOVirtual.Float(currGrain, profile.intensity, profile.duration, (float x) => currHandler.grain.intensity.value = x).SetEase(profile.curve); 
                    DOVirtual.Float(currLens, profile.extraParam1, profile.duration, (float x) => currHandler.lensDistortion.intensity.value = x).SetEase(profile.curve);
                    DOVirtual.Float(currContrast, profile.extraParam2, profile.duration, (float x) => currHandler.colAdjust.contrast.value = x).SetEase(profile.curve);
                    DOVirtual.Float(currSaturation, profile.extraParam3, profile.duration, (float x) => currHandler.colAdjust.saturation.value = x).SetEase(profile.curve);
                }
                else
                {
                    DOVirtual.Float(currGrain, 0f, profile.duration, (float x) => currHandler.grain.intensity.value = x).SetEase(profile.curve); 
                    DOVirtual.Float(currLens, 0f, profile.duration, (float x) => currHandler.lensDistortion.intensity.value = x).SetEase(profile.curve);
                    DOVirtual.Float(currContrast, 0f, profile.duration, (float x) => currHandler.colAdjust.contrast.value = x).SetEase(profile.curve);
                    DOVirtual.Float(currSaturation, 0f, profile.duration, (float x) => currHandler.colAdjust.saturation.value = x).SetEase(profile.curve);
                }
                break;
            case eEFFECT_PROFILE_TYPE.Destruct:
                if (profile.additive)
                {
                    DOVirtual.Float(currDestruction, currDestruction + profile.intensity / profile.steps, profile.duration, arg0 => currHandler.glitch.destruction.value = arg0).SetEase(profile.curve);
                }
                else
                {
                    DOVirtual.Float(currDestruction, currDestruction - profile.intensity, profile.duration, arg0 => currHandler.glitch.destruction.value = arg0).SetEase(profile.curve);
                }
                break;
            case eEFFECT_PROFILE_TYPE.Bloom:
                if (profile.additive)
                {
                    DOVirtual.Float(currBloomInt, profile.intensity, profile.duration, arg0 => currHandler.bloom.intensity.value = arg0).SetEase(profile.curve);
                    DOVirtual.Float(currBloomScatter, profile.extraParam1, profile.duration, arg0 => currHandler.bloom.scatter.value = arg0).SetEase(profile.curve);
                }
                else
                {
                    DOVirtual.Float(currBloomInt, currBloomInt-profile.intensity, profile.duration, arg0 => currHandler.bloom.intensity.value = arg0).SetEase(profile.curve);
                    DOVirtual.Float(currBloomScatter, currBloomScatter-profile.extraParam1, profile.duration, arg0 => currHandler.bloom.scatter.value = arg0).SetEase(profile.curve);
                }
                break;
            case eEFFECT_PROFILE_TYPE.Shake:
                if (profile.additive)
                {
                    DOVirtual.Float(currGlitchShake, profile.intensity, profile.duration, arg0 => currHandler.glitch.shake.value = arg0).SetEase(profile.curve);
                }
                else
                {
                    DOVirtual.Float(currGlitchShake, currGlitchShake-profile.intensity, profile.duration, arg0 => currHandler.glitch.shake.value = arg0).SetEase(profile.curve);
                }
                break;
            case eEFFECT_PROFILE_TYPE.Trap:
                if(profile.additive)
                {
                    DOVirtual.Float(currVignette, profile.intensity, profile.duration, arg0 => currHandler.vignette.intensity.value = arg0).SetEase(profile.curve);
                }
                else
                {
                    DOVirtual.Float(currVignette, currVignette - profile.intensity, profile.duration, arg0 => currHandler.vignette.intensity.value = arg0).SetEase(profile.curve);
                }
                break;
        }
    }

    public void StopEffect(EffectProfile profile)
    {
    }

    public void UpdateBlackHole(float lensIntensity, float grainIntensity, float percent)
    {
        currHandler.lensDistortion.intensity.value = lensIntensity * percent;
        currHandler.grain.intensity.value = grainIntensity * percent;
        currHandler.colAdjust.colorFilter.value = Color.Lerp(Color.white, Color.grey, percent);
    }

    public void StopBlackHole()
    {
        currHandler.lensDistortion.intensity.value = 0f;
        currHandler.grain.intensity.value = 0f;
        currHandler.colAdjust.colorFilter.value = Color.white;
    }
}
