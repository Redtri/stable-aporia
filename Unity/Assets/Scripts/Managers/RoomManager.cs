﻿using System;
using System.Collections;
using System.Collections.Generic;
using Coffee.UIExtensions;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;
using UnityEngine.Rendering.HighDefinition;
using Random = System.Random;

[System.Serializable]
public class IntEvent : UnityEvent<int> { }

[System.Serializable]
public class TransfList
{
    public List<Transform> transforms;
}

public class RoomManager : MonoBehaviour
{
    public int roomIndex;
    public int unloadBuffer;
    public int preloadBuffer;
    public bool isSAS;
    public bool isEntrepot;
    public bool lockWhenLeft;
    public int nbKnockingDoors;
    public eFOOTSTEP footstepType;
    public AudioEvent[] wwEvents;
    public AK.Wwise.State phonoStateOverride;
    public AK.Wwise.State anguishStateOverride;
    [HideInInspector] public RoomGenerator generator;
    [HideInInspector] public DoorManager doorManager;
    [HideInInspector] public Phone phone;
    public PhoneHelper phoneHelper;
    public AudioActor phonographe;
    public Transform phonographeOverrideSpot;
    public Transform lightParent;
    public int maxSequentialLightsShut;
    private int sequentialShutLights;
    public Transform furnitureParent;
    public bool startLightsShut;
    public bool leaveLightsShut;
    public float slowMultiplier;
    [HideInInspector] public List<LightActor> lights;
    public List<TransfList> destroyables;
    public UnityEvent onCheckout;
    public UnityEvent onFirstCheckout;
    private int checkoutCount;
    public UnityEvent onFirstPreCheckout;
    private int preCheckoutCount;
    public UnityEvent onExit;
    public IntEvent onTeleport;
    public SpawnPoint spawnPoint;
    public UIDissolve textDissolver;

    public void Init()
    {
        TryGetComponent(out generator);
        TryGetComponent(out doorManager);

        
        phone = GetComponentInChildren<Phone>();

        if (phone)
        {
            phone.Init(phoneHelper, roomIndex);
            phone.enabled = false;
        }
        

        if (!phonographe)
        {
            AudioActor[] audioActors = GetComponentsInChildren<AudioActor>();

            foreach (AudioActor audioActor in audioActors) {
                if (audioActor.name.Contains("Phono")) {
                    phonographe = audioActor;
                    break;
                }
            }
        }
        
        lights = new List<LightActor>();
        
        if (lightParent && lightParent.childCount > 0) {
            foreach (Light light in lightParent.GetComponentsInChildren<Light>())
            {
                LightActor attempt = null;
                light.TryGetComponent(out attempt);
                if (!attempt){
                    attempt = light.gameObject.AddComponent<LightActor>();
                }
                //Shutting room lights if needed
                if (startLightsShut && !attempt.shut) {
                    attempt.Trigger(0f);
                }
                
                attempt.roomIndex = roomIndex;
                lights.Add(attempt);
            }
        }
        if(!lightParent)
            Debug.Log(name + " has no light parent");

        //Spawn
        SpawnPoint spawn = GetComponentInChildren<SpawnPoint>();
        if (spawn) {
            spawnPoint = spawn;
        }

        //Picking random doors that will post Knocking door Wwise event
        if (nbKnockingDoors > 0)
        {
            doorManager.PickKnockingDoors(nbKnockingDoors);
        }
    }

    public void PreloadLights(bool streaming)
    {
        Debug.Log("preload lights " + name);
        foreach (LightActor lightActor in lights)
        {
            if (streaming)
            {
                if(lightActor.optimize)
                {
                    lightActor.Load();
                }
            }
            else
            {
                if (startLightsShut)
                {
                    lightActor.Optimize();
                }
            }
        }
    }

    public void OptimizeLights()
    {
        Debug.Log("Optimize " + name);
        if (startLightsShut)
        {
            foreach (LightActor lightActor in lights)
            {
                lightActor.shut = false;
                lightActor.Optimize();
            }
        }
    }

    //Player truly exited this room
    public void ExitRoom()
    {
        Debug.Log("Exit room " + name);
        foreach (AudioEvent audioEvent in wwEvents)
        {
            if (SoundManager.instance.EventPlaying(audioEvent.wwEvent.Id, gameObject))
            {
                audioEvent.wwEvent.Stop(gameObject);
            }
        }
        //ShutLights(0f);
        FurnituresStopLooking();
        if (lockWhenLeft) {
            doorManager.LockDoors();
        }
        if (nbKnockingDoors > 0)
        {
            doorManager.KnockDoors(false);
        }

        phone?.LeavePhone();

        ShutLights(0.25f);
    }

    private void OnEnable()
    {
        TryGetComponent(out generator);
        if (TryGetComponent(out doorManager))
        {
            //Listeners for door events
        }
    }

    private void OnDisable()
    {
        if (TryGetComponent(out doorManager))
        {
            //Listeners for door events
        }
    }

    public void PreCheckout()
    {
        if(preCheckoutCount == 0)
            onFirstPreCheckout?.Invoke();
        ++preCheckoutCount;
        if (isSAS) {
            SoundManager.instance.EnterSAS();
        } else if(!isEntrepot){
            SoundManager.instance.TryAmbiances();
        }else{
            SoundManager.instance.StopAmbiances();
        }
        if (anguishStateOverride.IsValid()) {
            //Debug.Log("Anguish state valid");
            //Debug.Log(anguishStateOverride);
            SoundManager.instance.SetAnguishStateID(anguishStateOverride.Id);
        }
        if (phonoStateOverride.IsValid() ) {
            //Debug.Log("Phono state valid");
            SoundManager.instance.SetPhonoStateID(phonoStateOverride.Id);
        }

            foreach (LightActor lightActor in lights) {
                if (startLightsShut) {
                    if (lightActor.shut)
                        lightActor.Trigger(0.1f);
                }
                if (lightActor.blitz)
                {
                    lightActor.Blitz();
                }
            }
        

        if(gameObject.activeInHierarchy)
            StartCoroutine(StartPhonoCo());
        
        foreach (AudioEvent audioEvent in wwEvents)
        {
            audioEvent.Play(gameObject);
        }

        if(slowMultiplier > 0.0f)
        {
            Pawn.controlledPawn.slowMultiplier = slowMultiplier;
        }
        else
        {
            Pawn.controlledPawn.slowMultiplier = 1f;
        }

        phone?.CheckoutPhone();
    }

    public void StopWwiseEvents(int ms)
    {
        foreach(AudioEvent audioEvent in wwEvents)
        {
            audioEvent.wwEvent.Stop(gameObject, ms);
        }
    }

    private IEnumerator StartPhonoCo()
    {
        yield return new WaitForSeconds(.1f);
        if (phonographe) {
            if (phonographeOverrideSpot) {
                phonographe.transform.position = phonographeOverrideSpot.transform.position;
                phonographe.transform.parent = phonographeOverrideSpot;
            }
            phonographe.PlayStd();
        }

        yield return null;
    }

    public void Checkout()
    {
        if (checkoutCount == 0)
            onFirstCheckout?.Invoke();
        ++checkoutCount;
        onCheckout?.Invoke();
        //Debug.Log("Checking out room " + gameObject.name);
        if (phone && !phone.isCheckedOut)
        {
            phone.CheckoutPhone();
            //phoneHelper.validated = true;
        }

        Pawn.controlledPawn.SetFootstep((int)footstepType);
        if (nbKnockingDoors > 0)
        {
            doorManager.KnockDoors(true);
        }

        foreach (LightActor lightActor in lights)
        {
            lightActor.currRoomIndex = GameManager.instance.currRoomIndex;
        }
    }

    public void TriggerPhone()
    {
        if(phoneHelper.trigger == eTRIGGER.Event)
            phone.CheckoutPhone();
    }

    public void DisablePhone()
    {
        phone?.LeavePhone();
    }

    public void TriggerLights(float duration)
    {
        for (int i = 0; i < lights.Count; ++i)
        {
            lights[i].Trigger(duration);
        }
    }
    
    public void ShutLights(float duration)
    {
        for (int i = 0; i < lights.Count; ++i)
        {
            lights[i].shut = false;
            lights[i].Trigger(duration);
        }
    }

    public void ShutLights(Transform lightContainer)
    {
        foreach (HDAdditionalLightData light in lightContainer.GetComponentsInChildren<HDAdditionalLightData>())
        {
            Light lightComp = light.GetComponent<Light>();
            float intensity = light.intensity;
            DOVirtual.Float(intensity, 0f, 2f, value => light.intensity = value);
            intensity = lightComp.intensity;
            DOVirtual.Float(intensity, 0f, 2f, value => lightComp.intensity = value);
        }
    }
    
    public void SequentialShutDown(float intervalDelay)
    {
        sequentialShutting = true;
        StartCoroutine(SequentialCo(intervalDelay));
    }

    private bool sequentialShutting;
    
    private IEnumerator SequentialCo(float intervalDelay)
    {
        int currentLight = 0;
        
        while (sequentialShutting)
        {
            if (lights[currentLight].seqShutdown)
            {
                if (sequentialShutLights == maxSequentialLightsShut)
                {
                    LightActor repowerLight = lights[ (currentLight-maxSequentialLightsShut < 0) ? (lights.Count-1) + currentLight-maxSequentialLightsShut : currentLight - maxSequentialLightsShut];
                    repowerLight.Trigger(1f);
                } else {
                    ++sequentialShutLights;
                }
                LightActor lightActor = lights[currentLight];
                lightActor.Trigger(0);
                DOVirtual.Float(0f, 100f, .5f, value => SoundManager.instance.SetLightSparkRTPC(value, lightActor.gameObject));
                yield return new WaitForSeconds(intervalDelay);
            }

            currentLight = (currentLight + 1) % lights.Count;
        }
        yield return null;
    }

    public void FurnituresLooking()
    {
        foreach(FacingTarget facingTarget in furnitureParent.GetComponentsInChildren<FacingTarget>())
        {
            facingTarget.Activate();
        }
    }

    public void FurnituresStopLooking()
    {
        if (furnitureParent)
        {
            FacingTarget[] facingTargets = furnitureParent.GetComponentsInChildren<FacingTarget>();
            if (facingTargets.Length > 0)
            {
                foreach(FacingTarget facingTarget in facingTargets)
                {
                    facingTarget.Deactivate();
                }
            }
        }
    }
    
    public void ReverseGravity()
    {
        for (int i = 0; i < furnitureParent.childCount; ++i)
        {
            Transform child = furnitureParent.GetChild(i);
            if (child.name.ToLower().Contains("gravity"))
            {
                for (int j = 0; j < child.childCount; ++j)
                {
                    Rigidbody rigid = child.GetChild(j).gameObject.AddComponent<Rigidbody>();

                    rigid.mass = 1f;
                    rigid.useGravity = false;
                }
            }
            else
            {
                Rigidbody rb = furnitureParent.GetChild(i).gameObject.AddComponent<Rigidbody>();
                rb.mass = 1f;
                rb.useGravity = false;
                rb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
                if (rb.name.ToLower().Contains("lamp") || rb.name.ToLower().Contains("phone"))
                {
                    rb.AddForce(Vector3.up * 15f);
                    rb.AddTorque(UnityEngine.Random.insideUnitSphere * 10f);
                }
                else
                {
                    rb.AddForce(Vector3.up);
                    rb.AddTorque(UnityEngine.Random.insideUnitSphere);
                }
            }
        }
    }

    public void GrowShaking(float duration)
    {
        for (int i = 0; i < furnitureParent.childCount; ++i)
        {
            //Rigidbody rb = furnitureParent.GetChild(i).gameObject.AddComponent<Rigidbody>();
            //rb.useGravity = false;

            TestShake testShake = furnitureParent.GetChild(i).gameObject.GetComponent<TestShake>();
            if (testShake)
            {
                testShake.duration = duration;
                testShake.Shake();
            }
            /*
            for (int j = 0; j < 6; ++j)
            {
                sequence.Append(transform.DOShakeRotation(quarDuration, quarAmount * (j+1), quarVib * (j+1), 90f, false));
            }*/
        }
    }
    
    public void DestroyAtIndex(int index)
    {
        if (index < destroyables.Count)
        {
            TransfList list = destroyables[index];
            foreach (Transform trsf in list.transforms)
            {
                trsf.gameObject.SetActive(false);
            }
        }
    }

    public void ActivateAtIndex(int index)
    {
        if (index < destroyables.Count)
        {
            TransfList list = destroyables[index];
            foreach (Transform trsf in list.transforms)
            {
                trsf.gameObject.SetActive(true);
            }
        }
    }

    public void FadeNormalMaps(float duration)
    {
        WallHandler[] wallHandlers = new[] {generator.nort, generator.east, generator.south, generator.west};
        for (int i = 0; i < wallHandlers.Length; ++i)
        {
            foreach (List<WallHandler.WallInfos> wallInfoses in wallHandlers[i].walls)
            {
                foreach (WallHandler.WallInfos wallInfos in wallInfoses)
                {
                    wallInfos.gameObject.GetComponent<Wall>().renderers[0].material
                        .DOFloat(1f, "Alpha", duration);
                }
            }
        }

        foreach (SurfaceHandler.SurfaceInfos surfaceInfos in generator.floorHandler.tiles)
        {
            Debug.Log("Hey");
            surfaceInfos.gameObject.GetComponent<Surface>().renderers[0].material.DOFloat(1f, "_Alpha", duration);
        }
    }

    public void MoveToCenter(Transform trsf)
    {
        Vector3 pos = generator.roomCenter.position;
        Vector3 target = trsf.transform.position;
        DOVirtual.Float(target.x, pos.x, 8f, (arg0) => trsf.position = new Vector3(arg0, trsf.position.y, trsf.position.z));
        DOVirtual.Float(target.z, pos.z, 8f, (arg0) => trsf.position = new Vector3(trsf.position.x, trsf.position.y, arg0));
        //trsf.DOMove(new Vector3(pos.x, trsf.position.y, pos.z), 8f);
    }

    public void TeleportInto()
    {
        onTeleport?.Invoke(roomIndex);
    }
}
