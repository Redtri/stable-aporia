/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID PLAY_AMB_ENTREPOT = 331413848U;
        static const AkUniqueID PLAY_AMB_ENTREPOT_FIN = 2609580526U;
        static const AkUniqueID PLAY_AMB_MENU = 1989837964U;
        static const AkUniqueID PLAY_AMB_SAS = 928165024U;
        static const AkUniqueID PLAY_AMB_VEST_EMITTER_LUMIERE = 1733548302U;
        static const AkUniqueID PLAY_AMB_VEST_SSHOTS_BOIS_STATES = 4168895155U;
        static const AkUniqueID PLAY_AMB_VEST_SSHOTS_CHUCHOTIS_STATES = 4092207124U;
        static const AkUniqueID PLAY_AMB_VESTIBULE_BED_STATES = 1462214091U;
        static const AkUniqueID PLAY_AMB_VESTIBULE_RETRECISSEMENTSALLE7 = 2294713884U;
        static const AkUniqueID PLAY_FOL_CHAISE1_CHUCHOTIS1 = 2782296144U;
        static const AkUniqueID PLAY_FOL_CHAISE2_CHUCHOTIS1 = 4081192429U;
        static const AkUniqueID PLAY_FOL_CHAISE3_CHUCHOTIS1 = 15505242U;
        static const AkUniqueID PLAY_FOL_CHAISE4_CHUCHOTIS1 = 697440871U;
        static const AkUniqueID PLAY_FOL_CHAISES_DEPLACENT = 1789045155U;
        static const AkUniqueID PLAY_FOL_CHAISES_DEPLACENTLOOP = 1151522189U;
        static const AkUniqueID PLAY_FOL_CHANGEMENTGRAVITE = 161984286U;
        static const AkUniqueID PLAY_FOL_LUMIERE_CLAQUENT = 3809569913U;
        static const AkUniqueID PLAY_FOL_LUMIERE_ETEINDRE = 1818177262U;
        static const AkUniqueID PLAY_FOL_LUMIERE_GRESILLEMENT = 3054449871U;
        static const AkUniqueID PLAY_FOL_LUMIERE_SASSALLE2 = 1091085932U;
        static const AkUniqueID PLAY_FOL_PHONO_ENTREPOT_PHILO = 1221875783U;
        static const AkUniqueID PLAY_FOL_PHONO_STATENIVEAUXANGOISSE = 1661041505U;
        static const AkUniqueID PLAY_FOL_PORTE_APPARITION = 2958157634U;
        static const AkUniqueID PLAY_FOL_PORTE_CLOSE = 205611475U;
        static const AkUniqueID PLAY_FOL_PORTE_COINCE = 3966394522U;
        static const AkUniqueID PLAY_FOL_PORTE_CRI = 71654703U;
        static const AkUniqueID PLAY_FOL_PORTE_KNOCK = 2808225447U;
        static const AkUniqueID PLAY_FOL_PORTE_OPEN = 18956901U;
        static const AkUniqueID PLAY_FOL_PORTE_RESPIRATIONS = 775440436U;
        static const AkUniqueID PLAY_FOL_TABLEAUBOUGE = 921806336U;
        static const AkUniqueID PLAY_FOL_TELEPHONE_BUZZ_ELEMENTAUTRES = 574329133U;
        static const AkUniqueID PLAY_FOL_TELEPHONE_BUZZ_SIMPLE = 911890705U;
        static const AkUniqueID PLAY_FOL_TELEPHONE_DECROCHE = 3858810250U;
        static const AkUniqueID PLAY_FOL_TELEPHONE_DECROCHE_SALLE6 = 1079295508U;
        static const AkUniqueID PLAY_FOL_TELEPHONE_MESSAGEAUTOMATIQUE = 242183033U;
        static const AkUniqueID PLAY_FOL_TELEPHONE_RACCROCHE = 1033749283U;
        static const AkUniqueID PLAY_FOL_TELEPHONE_RACCROCHE_SALLE6 = 2571484175U;
        static const AkUniqueID PLAY_FOL_TELEPHONE_SONNERIE = 199966330U;
        static const AkUniqueID PLAY_FOL_TELEPHONESALON2_FILTRE = 424518206U;
        static const AkUniqueID PLAY_FX_AMB_CREDITS = 123323362U;
        static const AkUniqueID PLAY_FX_BRICKENTREPOTFIN = 4268696266U;
        static const AkUniqueID PLAY_FX_CUTAUNOIRENTREPOTFIN = 1678369229U;
        static const AkUniqueID PLAY_FX_OMBRE = 971992092U;
        static const AkUniqueID PLAY_FX_PASSAGECANEVA = 3312299345U;
        static const AkUniqueID PLAY_FX_PASSAGECLAQUEMENTLUMI_RE = 909420793U;
        static const AkUniqueID PLAY_FX_PASSAGEGLITCHAMB = 4004184858U;
        static const AkUniqueID PLAY_FX_PASSAGEGLITCHSTEP = 2565472060U;
        static const AkUniqueID PLAY_FX_PASSAGEPORTE = 2281366457U;
        static const AkUniqueID PLAY_FX_TREMBLEMENT_BASE = 3457963342U;
        static const AkUniqueID PLAY_FX_TREMBLEMENTFIN = 706175121U;
        static const AkUniqueID PLAY_FX_TROU_NOIR = 1829260220U;
        static const AkUniqueID PLAY_PERSO_FOOTSETPS_BITUME = 3867774950U;
        static const AkUniqueID PLAY_PERSO_FOOTSETPS_LIQUID = 3915998562U;
        static const AkUniqueID PLAY_PERSO_FOOTSETPS_PARQUET = 3999358298U;
        static const AkUniqueID PLAY_PERSO_FOOTSETPS_REVERSE = 943023448U;
        static const AkUniqueID PLAY_PERSO_FOOTSETPS_TAPIS = 1954234369U;
        static const AkUniqueID PLAY_PERSO_RESPIRATION_PLEURS = 871797694U;
        static const AkUniqueID PLAY_PERSO_RESPIRATION_REVEIL = 1256498166U;
        static const AkUniqueID PLAY_PERSO_RESPIRATIONS_NA = 654872799U;
        static const AkUniqueID PLAY_TABLEAU_SHADERSALLE7 = 1651280236U;
        static const AkUniqueID PLAY_TABLEAUFOCAL = 3235849247U;
        static const AkUniqueID PLAY_TABLEAUFOCAL_ETAT = 1515972966U;
        static const AkUniqueID PLAY_THEMENA3LOOP = 2300697303U;
        static const AkUniqueID PLAY_THEMENA6LOOP = 2425766314U;
        static const AkUniqueID PLAY_THEMENA7LOOP = 724805371U;
        static const AkUniqueID PLAY_THEMENA8LOOP = 3500091624U;
        static const AkUniqueID PLAY_UI_CLIC = 3897570994U;
        static const AkUniqueID PLAY_UI_SLIDER = 2007211132U;
        static const AkUniqueID PLAY_VOIXAP_ENTREPOT_1_UM = 1520358452U;
        static const AkUniqueID PLAY_VOIXAP_ENTREPOT_2_V = 3787843239U;
        static const AkUniqueID PLAY_VOIXAP_ENTREPOT_3_UC_EMITTER = 146049963U;
        static const AkUniqueID PLAY_VOIXAP_ENTREPOT_4_S_C = 1876002782U;
        static const AkUniqueID PLAY_VOIXAP_ENTREPOTFINAL_1_V = 1890770154U;
        static const AkUniqueID PLAY_VOIXAP_ENTREPOTFINAL_2_S_C_G = 3518057480U;
        static const AkUniqueID PLAY_VOIXAP_SALON1_1_CHUCHOTICS_C_CLARA = 1664424546U;
        static const AkUniqueID PLAY_VOIXAP_SALON1_2_G_EMITTER = 682149064U;
        static const AkUniqueID PLAY_VOIXAP_SALON1_3A_G_EMITTER = 3362187992U;
        static const AkUniqueID PLAY_VOIXAP_SALON1_3B_G_UC_EMITTER = 3928553732U;
        static const AkUniqueID PLAY_VOIXAP_SALON1_4_UC_EMITTER = 4110783489U;
        static const AkUniqueID PLAY_VOIXAP_SALON1_5_S_CTABLEAU = 1900397956U;
        static const AkUniqueID PLAY_VOIXAP_SALON1_S_C_DECROCHE = 3292974824U;
        static const AkUniqueID PLAY_VOIXAP_SALON1SAS_6_S_CCOEUR = 2611189880U;
        static const AkUniqueID PLAY_VOIXAP_SALON2_1_UC_EMITTER = 1787207987U;
        static const AkUniqueID PLAY_VOIXAP_SALON2_2_CHUCHOTISS_CCLARA = 122389935U;
        static const AkUniqueID PLAY_VOIXAP_SALON2_2_CHUCHOTISS_CSALOME = 170613259U;
        static const AkUniqueID PLAY_VOIXAP_SALON3_1_V_G_EMITTER = 3290250382U;
        static const AkUniqueID PLAY_VOIXAP_SALON3_2_CHUCHOTISS_CCLARA = 2126348862U;
        static const AkUniqueID PLAY_VOIXAP_SALON3_2_V_G_EMITTER = 614690849U;
        static const AkUniqueID PLAY_VOIXAP_SALON3_3_V_G_EMITTER = 1729061416U;
        static const AkUniqueID PLAY_VOIXAP_SALON3_SALLE1_S_CREGARDE = 3384165305U;
        static const AkUniqueID PLAY_VOIXAP_SALON3_SALLE3_CHUCHOTISS_CCLARA = 176459102U;
        static const AkUniqueID PLAY_VOIXAP_SALON3_SALLE3_S_CPOURPRE = 3784909964U;
        static const AkUniqueID PLAY_VOIXAP_SALON4_1_CHUCHOTISS_CCLARARANDOM = 1743877271U;
        static const AkUniqueID PLAY_VOIXAP_SALON4_2_G_EMITTER = 86128333U;
        static const AkUniqueID PLAY_VOIXAP_SALON4_3_UC_EMITTER = 2808160395U;
        static const AkUniqueID PLAY_VOIXAP_SALON4_4_UC_EMITTER = 3136433878U;
        static const AkUniqueID PLAY_VOIXAP_SALON4_5_CHUCHOTISS_CCLARA = 512004586U;
        static const AkUniqueID PLAY_VOIXAP_SALON4_S_C_MAUVAISEPORTE = 267618255U;
        static const AkUniqueID PLAY_VOIXAP_SALON5_1_G_UC_EMITTER = 610127048U;
        static const AkUniqueID PLAY_VOIXAP_SALON6_CHUHCOTISSLESPAS = 2307160043U;
        static const AkUniqueID PLAY_VOIXAP_SALON6_G_UC = 3082625698U;
        static const AkUniqueID PLAY_VOIXAP_SALON6_SCHUCHOTISCOEUR = 199773179U;
        static const AkUniqueID PLAY_VOIXAP_SALON7_2GUC = 1181972390U;
        static const AkUniqueID PLAY_VOIXAP_SALON7_CHUHCOTISS_C = 1313764054U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace NIVEAUXANGOISSES
        {
            static const AkUniqueID GROUP = 2708581533U;

            namespace STATE
            {
                static const AkUniqueID NIVEAUANGOISSE1 = 2520750713U;
                static const AkUniqueID NIVEAUANGOISSE2 = 2520750714U;
                static const AkUniqueID NIVEAUANGOISSE3 = 2520750715U;
                static const AkUniqueID NIVEAUANGOISSE4 = 2520750716U;
                static const AkUniqueID NIVEAUANGOISSE5 = 2520750717U;
                static const AkUniqueID NIVEAUANGOISSE6 = 2520750718U;
                static const AkUniqueID NIVEAUANGOISSE7 = 2520750719U;
                static const AkUniqueID NIVEAUANGOISSE8 = 2520750704U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID SALLE5 = 2571129851U;
            } // namespace STATE
        } // namespace NIVEAUXANGOISSES

        namespace PHONO_NIVEAUXANGOISSES
        {
            static const AkUniqueID GROUP = 3912100548U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID PHONO_NIVEAUANGOISSE1 = 786463466U;
                static const AkUniqueID PHONO_NIVEAUANGOISSE2_1 = 2306284749U;
                static const AkUniqueID PHONO_NIVEAUANGOISSE2_2 = 2306284750U;
                static const AkUniqueID PHONO_NIVEAUANGOISSE3_1 = 3379905300U;
                static const AkUniqueID PHONO_NIVEAUANGOISSE3_2 = 3379905303U;
                static const AkUniqueID PHONO_NIVEAUANGOISSE4 = 786463471U;
                static const AkUniqueID PHONO_NIVEAUANGOISSE5 = 786463470U;
                static const AkUniqueID PHONO_NIVEAUANGOISSE6_1 = 2306976361U;
                static const AkUniqueID PHONO_NIVEAUANGOISSE6_2 = 2306976362U;
                static const AkUniqueID PHONO_NIVEAUANGOISSE7 = 786463468U;
                static const AkUniqueID PHONO_NIVEAUANGOISSE8 = 786463459U;
                static const AkUniqueID PHONO_SALLE5 = 2395659098U;
            } // namespace STATE
        } // namespace PHONO_NIVEAUXANGOISSES

    } // namespace STATES

    namespace SWITCHES
    {
        namespace TABLEAUFOCAL
        {
            static const AkUniqueID GROUP = 3691366952U;

            namespace SWITCH
            {
                static const AkUniqueID FAIL = 2596272617U;
                static const AkUniqueID GOOD = 668632890U;
            } // namespace SWITCH
        } // namespace TABLEAUFOCAL

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID CLAQUEMENTLUMIERE = 3702044103U;
        static const AkUniqueID PLAYBACK_RATE = 1524500807U;
        static const AkUniqueID RETRECISSEMENT = 3314924824U;
        static const AkUniqueID REVERBPIECEHP = 2046134637U;
        static const AkUniqueID RPM = 796049864U;
        static const AkUniqueID SS_AIR_FEAR = 1351367891U;
        static const AkUniqueID SS_AIR_FREEFALL = 3002758120U;
        static const AkUniqueID SS_AIR_FURY = 1029930033U;
        static const AkUniqueID SS_AIR_MONTH = 2648548617U;
        static const AkUniqueID SS_AIR_PRESENCE = 3847924954U;
        static const AkUniqueID SS_AIR_RPM = 822163944U;
        static const AkUniqueID SS_AIR_SIZE = 3074696722U;
        static const AkUniqueID SS_AIR_STORM = 3715662592U;
        static const AkUniqueID SS_AIR_TIMEOFDAY = 3203397129U;
        static const AkUniqueID SS_AIR_TURBULENCE = 4160247818U;
        static const AkUniqueID TELEPHONESALON2 = 2168182400U;
        static const AkUniqueID THEMEVOLUME = 4136678742U;
        static const AkUniqueID VOLUME_MASTER = 3695994288U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MANOIR = 3294054859U;
        static const AkUniqueID SOUNDBANKEND = 3844605687U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMBIENCES = 1017660616U;
        static const AkUniqueID AMBIENCES_EMITTER = 1434235487U;
        static const AkUniqueID FOLEYS = 4035004657U;
        static const AkUniqueID FX_THEME = 1600651075U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MOTION_FACTORY_BUS = 985987111U;
        static const AkUniqueID PERSONNAGE = 2307968525U;
        static const AkUniqueID VOIX = 3370469991U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID REVERBENTREPOT = 3564767884U;
        static const AkUniqueID REVERBIRREEL = 682014184U;
        static const AkUniqueID REVERBSALON = 3052268332U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID DEFAULT_MOTION_DEVICE = 4230635974U;
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
