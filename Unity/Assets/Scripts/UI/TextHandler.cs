﻿using System.Collections;
using System.Collections.Generic;
using Coffee.UIExtensions;
using DG.Tweening;
using UnityEngine;

public class TextHandler : MonoBehaviour
{
    private UIDissolve dissolver;
    // Start is called before the first frame update
    void OnEnable()
    {
        dissolver = GetComponent<UIDissolve>();
    }

    public void Dissolve(float duration)
    {
        DOVirtual.Float(1f, 0.5f, duration, value => dissolver.effectFactor = value);
    }
    public void Undissolve(float duration)
    {
        DOVirtual.Float(0.5f, 1f, duration, value => dissolver.effectFactor = value);
    }
}
