﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;
using DG.Tweening;

public class Floating : MonoBehaviour
{
    public bool floatStart = true;
    public float speed;
    public AnimationCurve curve;

    public float scale;
    public float waitRange;
    private bool floating;
    private float startTime;
    
    private void Start()
    {
        if(floatStart)
            StartCoroutine(FloatCo());
    }
    
    void Update()
    {
        if (floating)
        {
            transform.position += Vector3.up * Mathf.Sin(Time.time - startTime) * speed * Time.deltaTime;
        }
    }

    private IEnumerator FloatCo()
    {
        yield return new WaitForSeconds(UnityEngine.Random.Range(0f, waitRange));
        floating = true;
        startTime = Time.time;
        yield return null;
    }

    public void SlowFloat(float duration)
    {
        Rigidbody rigid = gameObject.AddComponent<Rigidbody>();

        rigid.mass = 1f;
        rigid.useGravity = false;
        rigid.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;

        DOVirtual.Float(0f, 1f, duration, arg0 => rigid.velocity = new Vector3(0f, arg0, 0f)).SetEase(curve);
        DOVirtual.Float(0f, 1f, duration/2f, arg0 => rigid.angularVelocity = new Vector3(0f, arg0, 0f)).SetEase(curve);
    }
}
