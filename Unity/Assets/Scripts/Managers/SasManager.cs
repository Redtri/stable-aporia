﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class DoorEvent : UnityEvent<Door>{}
[System.Serializable]
public class Door2Event : UnityEvent<Door, Door>{}

public class SasManager : MonoBehaviour
{
}
