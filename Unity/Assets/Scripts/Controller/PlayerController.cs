﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;
using UnityEngine.Rendering.UI;
using UnityEngine.SceneManagement;

public class HoldInputEvent : UnityEvent<InputActionPhase> { }

[System.Serializable]
public class InAction
{
    
    public float invokeDuration;
    public float duration;
    [HideInInspector] public bool enabled;
    
    [HideInInspector] public bool holding;
    [HideInInspector] public InputActionPhase phase;
    [HideInInspector] public InputActionPhase prevPhase;
    [HideInInspector] public float oldInvokePercent;
    [HideInInspector] public float oldReleasePercent;

    [HideInInspector]public float invokeStart;
    public HoldInputEvent onInvoke;
    [HideInInspector]public float actionStart;
    public HoldInputEvent onStart;
    [HideInInspector]public float releaseStart;
    public HoldInputEvent onRelease;

    public InAction()
    {
        onInvoke = new HoldInputEvent();
        onStart = new HoldInputEvent();
        onRelease = new HoldInputEvent();
    }
    
    public float invokePercent()
    {
        float percent = 0f;
        
        if (oldReleasePercent > 0f)
            percent = 1f-oldReleasePercent;
        
        percent += (Time.time - invokeStart) / invokeDuration;
        //Debug.Log("Invokepercent solo " + ((Time.time - invokeStart) / invokeDuration));
        
        return Mathf.Clamp(percent, 0f, 1f);
    }

    public float actionPercent()
    {
        return Mathf.Clamp((Time.time - actionStart) / duration, 0f, 1f);
    }
    
    public float releasePercent()
    {
        float percent = 0f;
        
        if (oldInvokePercent > 0f)
            percent = 1f-oldInvokePercent;
        
        percent += (Time.time - releaseStart) / invokeDuration;
        //Debug.Log("Releasepercent solo " + ((Time.time - actionEnd) / invokeDuration));
        
        return Mathf.Clamp(percent,0f, 1f);
    }

    public float getPercent(InputActionPhase phaseWanted)
    {
        switch (phaseWanted)
        {
            case InputActionPhase.Waiting:
            case InputActionPhase.Canceled:
                return releasePercent();
            case InputActionPhase.Performed:
                return actionPercent();
            case InputActionPhase.Started:
                return invokePercent();
        }

        return -1f;
    }

    public void setPrevPercent(InputActionPhase newPhase)
    {
        switch (newPhase)
        {
            case InputActionPhase.Started:
                oldReleasePercent = releasePercent();
                break;
            case InputActionPhase.Performed:
                oldReleasePercent = releasePercent();
                oldInvokePercent = invokePercent();
                break;
            case InputActionPhase.Canceled:
                oldInvokePercent = invokePercent();
                //Debug.Log("Setting old invoke percent to " + oldInvokePercent);
                break;
        }
    }
}

public class PlayerController : MonoSingleton<PlayerController>
{
    public PlayerData.InputInfos currInputInfos;
    public PlayerInput playerInput;
    [HideInInspector] public Vector2 movementInput;
    [HideInInspector] public Vector2 scrollInput;
    [HideInInspector] public Vector2 lookInput;
    [HideInInspector] public bool interactInput;
    [HideInInspector] public bool escapeInput;
    [HideInInspector] public UnityEvent backInputEvent;
    public InAction focusInput;
    public bool usingKeyboard;
    
    [HideInInspector] public bool jumpInput;

    private void OnEnable()
    {
        focusInput = new InAction();
        //Movement input listener
        playerInput.currentActionMap["Movement"].performed += OnMovement;
        playerInput.currentActionMap["Movement"].canceled += OnMovement;

        playerInput.currentActionMap["Scroll"].performed += OnScroll;
        playerInput.currentActionMap["Scroll"].canceled += OnScroll;
        //Look input listener
        playerInput.currentActionMap["Look"].performed += OnLook;
        playerInput.currentActionMap["Look"].canceled += OnLook;
        //Interact input listener
        playerInput.currentActionMap["Interact"].performed += OnInteract;
        playerInput.currentActionMap["Interact"].canceled += OnInteract;

        playerInput.currentActionMap["Back"].performed += OnBack;
        
        playerInput.currentActionMap["Focus"].started += OnFocus;
        playerInput.currentActionMap["Focus"].performed += OnFocus;
        playerInput.currentActionMap["Focus"].canceled += OnFocus;
        
        playerInput.currentActionMap["Jump"].performed += OnJump;
        playerInput.currentActionMap["Jump"].canceled += OnJump;
        
        playerInput.currentActionMap["Esc"].performed += OnEsc;
        playerInput.currentActionMap["Esc"].canceled += OnEsc;

    }

    private void OnEsc(InputAction.CallbackContext value)
    {
        //SoundManager.instance.Pause();
        escapeInput = !escapeInput;
    }

    private void OnMovement(InputAction.CallbackContext value)
    {
        CheckDevice(value.control.ToString());

        movementInput = value.ReadValue<Vector2>();
    }

    private void OnScroll(InputAction.CallbackContext value)
    {
        CheckDevice(value.control.ToString());

        scrollInput = value.ReadValue<Vector2>();
    }

    private void OnLook(InputAction.CallbackContext value)
    {
        CheckDevice(value.control.ToString());
        lookInput = value.ReadValue<Vector2>();
    }

    private void OnInteract(InputAction.CallbackContext value)
    {
        CheckDevice(value.control.ToString());

        interactInput = !interactInput;
    }
    private void OnBack(InputAction.CallbackContext value)
    {
        CheckDevice(value.control.ToString());

        backInputEvent?.Invoke();
    }

    public void Interrupt()
    {
        focusInput.prevPhase = focusInput.phase;
        focusInput.phase = InputActionPhase.Canceled;

        focusInput.releaseStart = Time.time;

        focusInput.setPrevPercent(focusInput.phase);
        focusInput.onRelease?.Invoke(focusInput.phase);
    }
    
    private void OnFocus(InputAction.CallbackContext value)
    {
        CheckDevice(value.control.ToString());

        //Debug.Log("Previous : " + focusInput.prevPhase + ". Current : " + focusInput.phase + ". Next : " + value.phase + ". Device : " + value.control.ToString());
        if (value.phase != focusInput.phase)
        {
            focusInput.prevPhase = focusInput.phase;
            focusInput.phase = value.phase;
            switch (value.phase)
            {
                case InputActionPhase.Started:
                    focusInput.invokeStart = Time.time;
                    if (!focusInput.enabled)
                    {
                        focusInput.enabled = true;
                    }
                    else
                    {
                        focusInput.setPrevPercent(focusInput.phase);
                    }
                    focusInput.onInvoke?.Invoke(focusInput.phase);
                    break;
                case InputActionPhase.Performed:
                    if(focusInput.prevPhase == InputActionPhase.Started) {
                        focusInput.actionStart = Time.time;
                        focusInput.onStart?.Invoke(focusInput.phase);

                        focusInput.phase = InputActionPhase.Canceled;
                        focusInput.setPrevPercent(focusInput.phase);
                        focusInput.onRelease?.Invoke(focusInput.phase);
                    }
                    break;
                case InputActionPhase.Canceled:
                    focusInput.releaseStart = Time.time;
                    focusInput.setPrevPercent(focusInput.phase);
                    focusInput.onRelease?.Invoke(focusInput.phase);
                    break;
            }
        }
    }

    private void OnJump(InputAction.CallbackContext value)
    {
        CheckDevice(value.control.ToString());
        
        jumpInput = !jumpInput;
    }

    private void CheckDevice(string message)
    {
        bool isUsingKeyboard = message.Contains("Keyboard") || message.Contains("Mouse");
        if (isUsingKeyboard != usingKeyboard) {
            InputDevice.UpdateDevice(isUsingKeyboard);
            usingKeyboard = isUsingKeyboard;
        }
    }
}
