﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Random = UnityEngine.Random;

public class ExitDoorDealingTeleporter : MonoBehaviour
{
    public Vector2 delayRange;
    public Vector2Int neighborRange;
    public Door currentExitDoor;
    public List<Transform> followers;
    private List<Furniture> furnitures;
    private DoorManager doorManager;

    private bool dealing;

    private void Awake()
    {
    }

    public void Deal()
    {
        if (!dealing)
        {
            dealing = true;
            Teleport(true);
        }
    }

    public void StopDeal()
    {
        dealing = false;
    }

    private IEnumerator DealingCo(float duration)
    {
        yield return new WaitForSeconds(duration);
        Teleport();
        yield return null;
    }

    private void Teleport(bool first = false)
    {
        if (currentExitDoor && !doorManager)
            doorManager = currentExitDoor.doorManager;
        //Picking neighboring new door in the room
        int currentDoorIndex = doorManager.roomDoors.IndexOf(currentExitDoor);

        int newIndex = currentDoorIndex + 1 + Random.Range(Mathf.Abs(neighborRange.x),Mathf.Abs(neighborRange.y));
        newIndex %= doorManager.roomDoors.Count;

        while (doorManager.roomDoors[newIndex].portal.HasTraveller(Pawn.controlledPawn.transform, 3f)) {

            newIndex = newIndex + 1 + Random.Range(Mathf.Abs(neighborRange.x), Mathf.Abs(neighborRange.y));
            newIndex %= doorManager.roomDoors.Count;
        }

        Door newDoor = doorManager.roomDoors[newIndex];
        /*while(doorManager.roomDoors.Count > 1 && newDoor == currentExitDoor)
        {
            newDoor = doorManager.roomDoors[Random.Range(0, doorManager.roomDoors.Count)];
        }*/
        Debug.Log("New exit door index : " + newIndex);
        if(first)
            UpdateDoor(newDoor);
        else
            StartCoroutine(WaitForCurrentDoor(newDoor));
    }

    //Waiting for the current door to be teleportable (No traveller in range and closed)
    private IEnumerator WaitForCurrentDoor(Door newDoor)
    {
        while (currentExitDoor && (currentExitDoor.doorState != eDOOR_STATE.CLOSED || currentExitDoor.portal.HasTraveller(Pawn.controlledPawn.transform, 5f)))
        {
            Debug.Log("Waiting : " + currentExitDoor + " " + currentExitDoor.doorState);
            yield return new WaitForSeconds(1f);
        }

        UpdateDoor(newDoor);
        yield return null;
    }

    private void UpdateDoor(Door newDoor)
    {
        newDoor.Standard(true);
        newDoor.travelMode = eDOOR_TRAVELMODE.PortalToNextRoom;

        currentExitDoor.Void(false);
        
        BringFollowers(newDoor);
        CancelParty();
        InviteFurnituresToParty(newDoor);

        currentExitDoor = newDoor;
        doorManager.doorToAvoid = newDoor;

        StartCoroutine(DealingCo(UnityEngine.Random.Range(delayRange.x, delayRange.y)));
    }

    private void BringFollowers(Door newDoor)
    {
        foreach (Transform follower in followers)
        {
            Matrix4x4 tpMatrix = newDoor.transform.localToWorldMatrix * currentExitDoor.transform.worldToLocalMatrix * follower.localToWorldMatrix;

            follower.position = tpMatrix.GetColumn(3);
            follower.rotation = tpMatrix.rotation;
        }
    }

    private void InviteFurnituresToParty(Door newDoor)
    {
        furnitures = new List<Furniture>();
        Collider[] colliders = Physics.OverlapSphere(newDoor.transform.position, 6f);

        foreach (Collider col in colliders)
        {
            Furniture furAttempt = col.GetComponent<Furniture>();
            if (furAttempt)
            {
                //Debug.Log(col.name);
                furnitures.Add(furAttempt);
                furAttempt.Dance();
            }
        }
    }

    private void CancelParty()
    {
        if (furnitures != null)
        {
            foreach (Furniture furniture in furnitures)
            {
                furniture.StopDancing();
            }
        }
    }
}
