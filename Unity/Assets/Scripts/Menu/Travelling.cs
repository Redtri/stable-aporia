﻿using DG.Tweening;
using DG.Tweening.Plugins.Options;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Travelling : MonoBehaviour
{
    public Transform[] corridors;
    public float corridorSize;
    public Transform corridorSpawn;

    public float mps;
    public Vector3 absoluteDirection;

    private Transform target;

    private bool scrolling;

    public void BeginTravelling(Transform tTarget)
    {
        scrolling = true;
        target = tTarget;
        StartCoroutine(Scroll());
    }

    public void Stop()
    {
        scrolling = false;
    }

    private IEnumerator Scroll()
    {
        while (scrolling) {
            foreach(Transform corridor in corridors) {
                corridor.Translate(absoluteDirection * mps * Time.deltaTime, Space.World);
                if (Mathf.Sign(Vector3.Dot(target.forward, (target.position - corridor.position).normalized)) < 0f && Vector3.Distance(target.position, corridor.position) > corridorSize)
                    corridor.position = corridorSpawn.position;
            }
            yield return new WaitForEndOfFrame();
        }
        yield return null;
    }
}
