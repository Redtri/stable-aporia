﻿Shader "Unlit Master"
{
    Properties
    {
        [NoScaleOffset] _MainTex("MainTex", 2D) = "white" {}
        _Alpha("Alpha", Range(0, 1)) = 0
        [NoScaleOffset]Mask("Mask", 2D) = "white" {}
    }
        SubShader
    {
        Tags
        {
            "RenderPipeline" = "HDRenderPipeline"
            "RenderType" = "HDUnlitShader"
            "Queue" = "Transparent+0"
        }

        Pass
        {
            // based on UnlitPass.template
            Name "ShadowCaster"
            Tags { "LightMode" = "ShadowCaster" }

        //-------------------------------------------------------------------------------------
        // Render Modes (Blend, Cull, ZTest, Stencil, etc)
        //-------------------------------------------------------------------------------------

        Cull Off


        ZWrite On
        ZTest Always

        ColorMask 0

        //-------------------------------------------------------------------------------------
        // End Render Modes
        //-------------------------------------------------------------------------------------

        HLSLPROGRAM

        #pragma target 4.5
        #pragma only_renderers d3d11 ps4 xboxone vulkan metal switch
        //#pragma enable_d3d11_debug_symbols

        //enable GPU instancing support
        #pragma multi_compile_instancing

        //-------------------------------------------------------------------------------------
        // Variant Definitions (active field translations to HDRP defines)
        //-------------------------------------------------------------------------------------
        #define _SURFACE_TYPE_TRANSPARENT 1
        #define _BLENDMODE_ALPHA 1
        // #define _BLENDMODE_ADD 1
        // #define _BLENDMODE_PRE_MULTIPLY 1
        // #define _ADD_PRECOMPUTED_VELOCITY

        //-------------------------------------------------------------------------------------
        // End Variant Definitions
        //-------------------------------------------------------------------------------------

        #pragma vertex Vert
        #pragma fragment Frag

        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderVariables.hlsl"
        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/FragInputs.hlsl"
        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/ShaderPass.cs.hlsl"

        //-------------------------------------------------------------------------------------
        // Defines
        //-------------------------------------------------------------------------------------
                #define SHADERPASS SHADERPASS_SHADOWS
            // ACTIVE FIELDS:
            //   AlphaTest
            //   SurfaceType.Transparent
            //   BlendMode.Alpha
            //   SurfaceDescriptionInputs.uv0
            //   VertexDescriptionInputs.ObjectSpaceNormal
            //   VertexDescriptionInputs.ObjectSpaceTangent
            //   VertexDescriptionInputs.ObjectSpacePosition
            //   SurfaceDescription.Alpha
            //   SurfaceDescription.AlphaClipThreshold
            //   FragInputs.texCoord0
            //   AttributesMesh.normalOS
            //   AttributesMesh.tangentOS
            //   AttributesMesh.positionOS
            //   VaryingsMeshToPS.texCoord0
            //   AttributesMesh.uv0
            // Shared Graph Keywords

        // this translates the new dependency tracker into the old preprocessor definitions for the existing HDRP shader code
        #define ATTRIBUTES_NEED_NORMAL
        #define ATTRIBUTES_NEED_TANGENT
        #define ATTRIBUTES_NEED_TEXCOORD0
        // #define ATTRIBUTES_NEED_TEXCOORD1
        // #define ATTRIBUTES_NEED_TEXCOORD2
        // #define ATTRIBUTES_NEED_TEXCOORD3
        // #define ATTRIBUTES_NEED_COLOR
        // #define VARYINGS_NEED_POSITION_WS
        // #define VARYINGS_NEED_TANGENT_TO_WORLD
        #define VARYINGS_NEED_TEXCOORD0
        // #define VARYINGS_NEED_TEXCOORD1
        // #define VARYINGS_NEED_TEXCOORD2
        // #define VARYINGS_NEED_TEXCOORD3
        // #define VARYINGS_NEED_COLOR
        // #define VARYINGS_NEED_CULLFACE
        // #define HAVE_MESH_MODIFICATION

        //-------------------------------------------------------------------------------------
        // End Defines
        //-------------------------------------------------------------------------------------


        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/Material.hlsl"
        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/Unlit/Unlit.hlsl"

        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/BuiltinUtilities.hlsl"
        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/MaterialUtilities.hlsl"
        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderGraphFunctions.hlsl"

        // Used by SceneSelectionPass
        int _ObjectId;
        int _PassValue;

        //-------------------------------------------------------------------------------------
        // Interpolator Packing And Struct Declarations
        //-------------------------------------------------------------------------------------
        // Generated Type: AttributesMesh
        struct AttributesMesh
        {
            float3 positionOS : POSITION;
            float3 normalOS : NORMAL; // optional
            float4 tangentOS : TANGENT; // optional
            float4 uv0 : TEXCOORD0; // optional
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif // UNITY_ANY_INSTANCING_ENABLED
        };
        // Generated Type: VaryingsMeshToPS
        struct VaryingsMeshToPS
        {
            float4 positionCS : SV_Position;
            float4 texCoord0; // optional
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif // UNITY_ANY_INSTANCING_ENABLED
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif // defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        };

        // Generated Type: PackedVaryingsMeshToPS
        struct PackedVaryingsMeshToPS
        {
            float4 positionCS : SV_Position; // unpacked
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID; // unpacked
            #endif // conditional
            float4 interp00 : TEXCOORD0; // auto-packed
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC; // unpacked
            #endif // conditional
        };

        // Packed Type: VaryingsMeshToPS
        PackedVaryingsMeshToPS PackVaryingsMeshToPS(VaryingsMeshToPS input)
        {
            PackedVaryingsMeshToPS output;
            output.positionCS = input.positionCS;
            output.interp00.xyzw = input.texCoord0;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif // conditional
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif // conditional
            return output;
        }

        // Unpacked Type: VaryingsMeshToPS
        VaryingsMeshToPS UnpackVaryingsMeshToPS(PackedVaryingsMeshToPS input)
        {
            VaryingsMeshToPS output;
            output.positionCS = input.positionCS;
            output.texCoord0 = input.interp00.xyzw;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif // conditional
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif // conditional
            return output;
        }
        // Generated Type: VaryingsMeshToDS
        struct VaryingsMeshToDS
        {
            float3 positionRWS;
            float3 normalWS;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif // UNITY_ANY_INSTANCING_ENABLED
        };

        // Generated Type: PackedVaryingsMeshToDS
        struct PackedVaryingsMeshToDS
        {
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID; // unpacked
            #endif // conditional
            float3 interp00 : TEXCOORD0; // auto-packed
            float3 interp01 : TEXCOORD1; // auto-packed
        };

        // Packed Type: VaryingsMeshToDS
        PackedVaryingsMeshToDS PackVaryingsMeshToDS(VaryingsMeshToDS input)
        {
            PackedVaryingsMeshToDS output;
            output.interp00.xyz = input.positionRWS;
            output.interp01.xyz = input.normalWS;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif // conditional
            return output;
        }

        // Unpacked Type: VaryingsMeshToDS
        VaryingsMeshToDS UnpackVaryingsMeshToDS(PackedVaryingsMeshToDS input)
        {
            VaryingsMeshToDS output;
            output.positionRWS = input.interp00.xyz;
            output.normalWS = input.interp01.xyz;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif // conditional
            return output;
        }
        //-------------------------------------------------------------------------------------
        // End Interpolator Packing And Struct Declarations
        //-------------------------------------------------------------------------------------

        //-------------------------------------------------------------------------------------
        // Graph generated code
        //-------------------------------------------------------------------------------------
                // Shared Graph Properties (uniform inputs)
                CBUFFER_START(UnityPerMaterial)
                float _Alpha;
                CBUFFER_END
                TEXTURE2D(_MainTex); SAMPLER(sampler_MainTex); float4 _MainTex_TexelSize;
                TEXTURE2D(Mask); SAMPLER(samplerMask); float4 Mask_TexelSize;
                SAMPLER(_SampleTexture2D_2D75C97F_Sampler_3_Linear_Repeat);

                // Pixel Graph Inputs
                    struct SurfaceDescriptionInputs
                    {
                        float4 uv0; // optional
                    };
                    // Pixel Graph Outputs
                        struct SurfaceDescription
                        {
                            float Alpha;
                            float AlphaClipThreshold;
                        };

                        // Shared Graph Node Functions

                            void Unity_Power_float4(float4 A, float4 B, out float4 Out)
                            {
                                Out = pow(A, B);
                            }

                            void Unity_Subtract_float4(float4 A, float4 B, out float4 Out)
                            {
                                Out = A - B;
                            }

                            void Unity_Clamp_float4(float4 In, float4 Min, float4 Max, out float4 Out)
                            {
                                Out = clamp(In, Min, Max);
                            }

                            // Pixel Graph Evaluation
                                SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
                                {
                                    SurfaceDescription surface = (SurfaceDescription)0;
                                    float4 _SampleTexture2D_2D75C97F_RGBA_0 = SAMPLE_TEXTURE2D(Mask, samplerMask, IN.uv0.xy);
                                    float _SampleTexture2D_2D75C97F_R_4 = _SampleTexture2D_2D75C97F_RGBA_0.r;
                                    float _SampleTexture2D_2D75C97F_G_5 = _SampleTexture2D_2D75C97F_RGBA_0.g;
                                    float _SampleTexture2D_2D75C97F_B_6 = _SampleTexture2D_2D75C97F_RGBA_0.b;
                                    float _SampleTexture2D_2D75C97F_A_7 = _SampleTexture2D_2D75C97F_RGBA_0.a;
                                    float _Vector1_3D656997_Out_0 = 0.8;
                                    float4 _Power_4DAD70D9_Out_2;
                                    Unity_Power_float4(_SampleTexture2D_2D75C97F_RGBA_0, (_Vector1_3D656997_Out_0.xxxx), _Power_4DAD70D9_Out_2);
                                    float _Property_6BBE9849_Out_0 = _Alpha;
                                    float4 _Subtract_57CD835A_Out_2;
                                    Unity_Subtract_float4(_Power_4DAD70D9_Out_2, (_Property_6BBE9849_Out_0.xxxx), _Subtract_57CD835A_Out_2);
                                    float4 _Clamp_CF02927_Out_3;
                                    Unity_Clamp_float4(_Subtract_57CD835A_Out_2, float4(0, 0, 0, 0), float4(1, 1, 1, 1), _Clamp_CF02927_Out_3);
                                    float _Vector1_9997196B_Out_0 = 0;
                                    surface.Alpha = (_Clamp_CF02927_Out_3).x;
                                    surface.AlphaClipThreshold = _Vector1_9997196B_Out_0;
                                    return surface;
                                }

                                //-------------------------------------------------------------------------------------
                                // End graph generated code
                                //-------------------------------------------------------------------------------------

                            // $include("VertexAnimation.template.hlsl")

                            //-------------------------------------------------------------------------------------
                                // TEMPLATE INCLUDE : SharedCode.template.hlsl
                                //-------------------------------------------------------------------------------------

                                    FragInputs BuildFragInputs(VaryingsMeshToPS input)
                                    {
                                        FragInputs output;
                                        ZERO_INITIALIZE(FragInputs, output);

                                        // Init to some default value to make the computer quiet (else it output 'divide by zero' warning even if value is not used).
                                        // TODO: this is a really poor workaround, but the variable is used in a bunch of places
                                        // to compute normals which are then passed on elsewhere to compute other values...
                                        output.tangentToWorld = k_identity3x3;
                                        output.positionSS = input.positionCS;       // input.positionCS is SV_Position

                                        // output.positionRWS = input.positionRWS;
                                        // output.tangentToWorld = BuildTangentToWorld(input.tangentWS, input.normalWS);
                                        output.texCoord0 = input.texCoord0;
                                        // output.texCoord1 = input.texCoord1;
                                        // output.texCoord2 = input.texCoord2;
                                        // output.texCoord3 = input.texCoord3;
                                        // output.color = input.color;
                                        #if _DOUBLESIDED_ON && SHADER_STAGE_FRAGMENT
                                        output.isFrontFace = IS_FRONT_VFACE(input.cullFace, true, false);
                                        #elif SHADER_STAGE_FRAGMENT
                                        // output.isFrontFace = IS_FRONT_VFACE(input.cullFace, true, false);
                                        #endif // SHADER_STAGE_FRAGMENT

                                        return output;
                                    }

                                    SurfaceDescriptionInputs FragInputsToSurfaceDescriptionInputs(FragInputs input, float3 viewWS)
                                    {
                                        SurfaceDescriptionInputs output;
                                        ZERO_INITIALIZE(SurfaceDescriptionInputs, output);

                                        // output.WorldSpaceNormal =            normalize(input.tangentToWorld[2].xyz);
                                        // output.ObjectSpaceNormal =           mul(output.WorldSpaceNormal, (float3x3) UNITY_MATRIX_M);           // transposed multiplication by inverse matrix to handle normal scale
                                        // output.ViewSpaceNormal =             mul(output.WorldSpaceNormal, (float3x3) UNITY_MATRIX_I_V);         // transposed multiplication by inverse matrix to handle normal scale
                                        // output.TangentSpaceNormal =          float3(0.0f, 0.0f, 1.0f);
                                        // output.WorldSpaceTangent =           input.tangentToWorld[0].xyz;
                                        // output.ObjectSpaceTangent =          TransformWorldToObjectDir(output.WorldSpaceTangent);
                                        // output.ViewSpaceTangent =            TransformWorldToViewDir(output.WorldSpaceTangent);
                                        // output.TangentSpaceTangent =         float3(1.0f, 0.0f, 0.0f);
                                        // output.WorldSpaceBiTangent =         input.tangentToWorld[1].xyz;
                                        // output.ObjectSpaceBiTangent =        TransformWorldToObjectDir(output.WorldSpaceBiTangent);
                                        // output.ViewSpaceBiTangent =          TransformWorldToViewDir(output.WorldSpaceBiTangent);
                                        // output.TangentSpaceBiTangent =       float3(0.0f, 1.0f, 0.0f);
                                        // output.WorldSpaceViewDirection =     normalize(viewWS);
                                        // output.ObjectSpaceViewDirection =    TransformWorldToObjectDir(output.WorldSpaceViewDirection);
                                        // output.ViewSpaceViewDirection =      TransformWorldToViewDir(output.WorldSpaceViewDirection);
                                        // float3x3 tangentSpaceTransform =     float3x3(output.WorldSpaceTangent,output.WorldSpaceBiTangent,output.WorldSpaceNormal);
                                        // output.TangentSpaceViewDirection =   mul(tangentSpaceTransform, output.WorldSpaceViewDirection);
                                        // output.WorldSpacePosition =          input.positionRWS;
                                        // output.ObjectSpacePosition =         TransformWorldToObject(input.positionRWS);
                                        // output.ViewSpacePosition =           TransformWorldToView(input.positionRWS);
                                        // output.TangentSpacePosition =        float3(0.0f, 0.0f, 0.0f);
                                        // output.AbsoluteWorldSpacePosition =  GetAbsolutePositionWS(input.positionRWS);
                                        // output.ScreenPosition =              ComputeScreenPos(TransformWorldToHClip(input.positionRWS), _ProjectionParams.x);
                                        output.uv0 = input.texCoord0;
                                        // output.uv1 =                         input.texCoord1;
                                        // output.uv2 =                         input.texCoord2;
                                        // output.uv3 =                         input.texCoord3;
                                        // output.VertexColor =                 input.color;
                                        // output.FaceSign =                    input.isFrontFace;
                                        // output.TimeParameters =              _TimeParameters.xyz; // This is mainly for LW as HD overwrite this value

                                        return output;
                                    }

                                    // existing HDRP code uses the combined function to go directly from packed to frag inputs
                                    FragInputs UnpackVaryingsMeshToFragInputs(PackedVaryingsMeshToPS input)
                                    {
                                        UNITY_SETUP_INSTANCE_ID(input);
                                        VaryingsMeshToPS unpacked = UnpackVaryingsMeshToPS(input);
                                        return BuildFragInputs(unpacked);
                                    }

                                    //-------------------------------------------------------------------------------------
                                    // END TEMPLATE INCLUDE : SharedCode.template.hlsl
                                    //-------------------------------------------------------------------------------------



                                    void BuildSurfaceData(FragInputs fragInputs, inout SurfaceDescription surfaceDescription, float3 V, PositionInputs posInput, out SurfaceData surfaceData)
                                    {
                                        // setup defaults -- these are used if the graph doesn't output a value
                                        ZERO_INITIALIZE(SurfaceData, surfaceData);

                                        // copy across graph values, if defined
                                        // surfaceData.color = surfaceDescription.Color;

                                #if defined(DEBUG_DISPLAY)
                                        if (_DebugMipMapMode != DEBUGMIPMAPMODE_NONE)
                                        {
                                            // TODO
                                        }
                                #endif
                                    }

                                    void GetSurfaceAndBuiltinData(FragInputs fragInputs, float3 V, inout PositionInputs posInput, out SurfaceData surfaceData, out BuiltinData builtinData)
                                    {
                                        SurfaceDescriptionInputs surfaceDescriptionInputs = FragInputsToSurfaceDescriptionInputs(fragInputs, V);
                                        SurfaceDescription surfaceDescription = SurfaceDescriptionFunction(surfaceDescriptionInputs);

                                        // Perform alpha test very early to save performance (a killed pixel will not sample textures)
                                        // TODO: split graph evaluation to grab just alpha dependencies first? tricky..
                                        DoAlphaTest(surfaceDescription.Alpha, surfaceDescription.AlphaClipThreshold);

                                        BuildSurfaceData(fragInputs, surfaceDescription, V, posInput, surfaceData);

                                        // Builtin Data
                                        ZERO_INITIALIZE(BuiltinData, builtinData); // No call to InitBuiltinData as we don't have any lighting
                                        builtinData.opacity = surfaceDescription.Alpha;
                                    }

                                    //-------------------------------------------------------------------------------------
                                    // Pass Includes
                                    //-------------------------------------------------------------------------------------
                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/ShaderPassDepthOnly.hlsl"
                                    //-------------------------------------------------------------------------------------
                                    // End Pass Includes
                                    //-------------------------------------------------------------------------------------

                                    ENDHLSL
                                }

                                Pass
                                {
                                        // based on UnlitPass.template
                                        Name "META"
                                        Tags { "LightMode" = "META" }

                                        //-------------------------------------------------------------------------------------
                                        // Render Modes (Blend, Cull, ZTest, Stencil, etc)
                                        //-------------------------------------------------------------------------------------

                                        Cull Off





                                        //-------------------------------------------------------------------------------------
                                        // End Render Modes
                                        //-------------------------------------------------------------------------------------

                                        HLSLPROGRAM

                                        #pragma target 4.5
                                        #pragma only_renderers d3d11 ps4 xboxone vulkan metal switch
                                        //#pragma enable_d3d11_debug_symbols

                                        //enable GPU instancing support
                                        #pragma multi_compile_instancing

                                        //-------------------------------------------------------------------------------------
                                        // Variant Definitions (active field translations to HDRP defines)
                                        //-------------------------------------------------------------------------------------
                                        #define _SURFACE_TYPE_TRANSPARENT 1
                                        #define _BLENDMODE_ALPHA 1
                                        // #define _BLENDMODE_ADD 1
                                        // #define _BLENDMODE_PRE_MULTIPLY 1
                                        // #define _ADD_PRECOMPUTED_VELOCITY

                                        //-------------------------------------------------------------------------------------
                                        // End Variant Definitions
                                        //-------------------------------------------------------------------------------------

                                        #pragma vertex Vert
                                        #pragma fragment Frag

                                        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderVariables.hlsl"
                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/FragInputs.hlsl"
                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/ShaderPass.cs.hlsl"

                                        //-------------------------------------------------------------------------------------
                                        // Defines
                                        //-------------------------------------------------------------------------------------
                                                #define SHADERPASS SHADERPASS_LIGHT_TRANSPORT
                                            // ACTIVE FIELDS:
                                            //   AlphaTest
                                            //   SurfaceType.Transparent
                                            //   BlendMode.Alpha
                                            //   SurfaceDescriptionInputs.uv0
                                            //   VertexDescriptionInputs.ObjectSpaceNormal
                                            //   VertexDescriptionInputs.ObjectSpaceTangent
                                            //   VertexDescriptionInputs.ObjectSpacePosition
                                            //   SurfaceDescription.Color
                                            //   SurfaceDescription.Alpha
                                            //   SurfaceDescription.AlphaClipThreshold
                                            //   AttributesMesh.normalOS
                                            //   AttributesMesh.tangentOS
                                            //   AttributesMesh.uv0
                                            //   AttributesMesh.uv1
                                            //   AttributesMesh.color
                                            //   AttributesMesh.uv2
                                            //   FragInputs.texCoord0
                                            //   AttributesMesh.positionOS
                                            //   VaryingsMeshToPS.texCoord0
                                            // Shared Graph Keywords

                                        // this translates the new dependency tracker into the old preprocessor definitions for the existing HDRP shader code
                                        #define ATTRIBUTES_NEED_NORMAL
                                        #define ATTRIBUTES_NEED_TANGENT
                                        #define ATTRIBUTES_NEED_TEXCOORD0
                                        #define ATTRIBUTES_NEED_TEXCOORD1
                                        #define ATTRIBUTES_NEED_TEXCOORD2
                                        // #define ATTRIBUTES_NEED_TEXCOORD3
                                        #define ATTRIBUTES_NEED_COLOR
                                        // #define VARYINGS_NEED_POSITION_WS
                                        // #define VARYINGS_NEED_TANGENT_TO_WORLD
                                        #define VARYINGS_NEED_TEXCOORD0
                                        // #define VARYINGS_NEED_TEXCOORD1
                                        // #define VARYINGS_NEED_TEXCOORD2
                                        // #define VARYINGS_NEED_TEXCOORD3
                                        // #define VARYINGS_NEED_COLOR
                                        // #define VARYINGS_NEED_CULLFACE
                                        // #define HAVE_MESH_MODIFICATION

                                        //-------------------------------------------------------------------------------------
                                        // End Defines
                                        //-------------------------------------------------------------------------------------


                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/Material.hlsl"
                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/Unlit/Unlit.hlsl"

                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/BuiltinUtilities.hlsl"
                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/MaterialUtilities.hlsl"
                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderGraphFunctions.hlsl"

                                        // Used by SceneSelectionPass
                                        int _ObjectId;
                                        int _PassValue;

                                        //-------------------------------------------------------------------------------------
                                        // Interpolator Packing And Struct Declarations
                                        //-------------------------------------------------------------------------------------
                                        // Generated Type: AttributesMesh
                                        struct AttributesMesh
                                        {
                                            float3 positionOS : POSITION;
                                            float3 normalOS : NORMAL; // optional
                                            float4 tangentOS : TANGENT; // optional
                                            float4 uv0 : TEXCOORD0; // optional
                                            float4 uv1 : TEXCOORD1; // optional
                                            float4 uv2 : TEXCOORD2; // optional
                                            float4 color : COLOR; // optional
                                            #if UNITY_ANY_INSTANCING_ENABLED
                                            uint instanceID : INSTANCEID_SEMANTIC;
                                            #endif // UNITY_ANY_INSTANCING_ENABLED
                                        };
                                        // Generated Type: VaryingsMeshToPS
                                        struct VaryingsMeshToPS
                                        {
                                            float4 positionCS : SV_Position;
                                            float4 texCoord0; // optional
                                            #if UNITY_ANY_INSTANCING_ENABLED
                                            uint instanceID : CUSTOM_INSTANCE_ID;
                                            #endif // UNITY_ANY_INSTANCING_ENABLED
                                            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                                            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
                                            #endif // defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                                        };

                                        // Generated Type: PackedVaryingsMeshToPS
                                        struct PackedVaryingsMeshToPS
                                        {
                                            float4 positionCS : SV_Position; // unpacked
                                            #if UNITY_ANY_INSTANCING_ENABLED
                                            uint instanceID : CUSTOM_INSTANCE_ID; // unpacked
                                            #endif // conditional
                                            float4 interp00 : TEXCOORD0; // auto-packed
                                            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                                            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC; // unpacked
                                            #endif // conditional
                                        };

                                        // Packed Type: VaryingsMeshToPS
                                        PackedVaryingsMeshToPS PackVaryingsMeshToPS(VaryingsMeshToPS input)
                                        {
                                            PackedVaryingsMeshToPS output;
                                            output.positionCS = input.positionCS;
                                            output.interp00.xyzw = input.texCoord0;
                                            #if UNITY_ANY_INSTANCING_ENABLED
                                            output.instanceID = input.instanceID;
                                            #endif // conditional
                                            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                                            output.cullFace = input.cullFace;
                                            #endif // conditional
                                            return output;
                                        }

                                        // Unpacked Type: VaryingsMeshToPS
                                        VaryingsMeshToPS UnpackVaryingsMeshToPS(PackedVaryingsMeshToPS input)
                                        {
                                            VaryingsMeshToPS output;
                                            output.positionCS = input.positionCS;
                                            output.texCoord0 = input.interp00.xyzw;
                                            #if UNITY_ANY_INSTANCING_ENABLED
                                            output.instanceID = input.instanceID;
                                            #endif // conditional
                                            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                                            output.cullFace = input.cullFace;
                                            #endif // conditional
                                            return output;
                                        }
                                        // Generated Type: VaryingsMeshToDS
                                        struct VaryingsMeshToDS
                                        {
                                            float3 positionRWS;
                                            float3 normalWS;
                                            #if UNITY_ANY_INSTANCING_ENABLED
                                            uint instanceID : CUSTOM_INSTANCE_ID;
                                            #endif // UNITY_ANY_INSTANCING_ENABLED
                                        };

                                        // Generated Type: PackedVaryingsMeshToDS
                                        struct PackedVaryingsMeshToDS
                                        {
                                            #if UNITY_ANY_INSTANCING_ENABLED
                                            uint instanceID : CUSTOM_INSTANCE_ID; // unpacked
                                            #endif // conditional
                                            float3 interp00 : TEXCOORD0; // auto-packed
                                            float3 interp01 : TEXCOORD1; // auto-packed
                                        };

                                        // Packed Type: VaryingsMeshToDS
                                        PackedVaryingsMeshToDS PackVaryingsMeshToDS(VaryingsMeshToDS input)
                                        {
                                            PackedVaryingsMeshToDS output;
                                            output.interp00.xyz = input.positionRWS;
                                            output.interp01.xyz = input.normalWS;
                                            #if UNITY_ANY_INSTANCING_ENABLED
                                            output.instanceID = input.instanceID;
                                            #endif // conditional
                                            return output;
                                        }

                                        // Unpacked Type: VaryingsMeshToDS
                                        VaryingsMeshToDS UnpackVaryingsMeshToDS(PackedVaryingsMeshToDS input)
                                        {
                                            VaryingsMeshToDS output;
                                            output.positionRWS = input.interp00.xyz;
                                            output.normalWS = input.interp01.xyz;
                                            #if UNITY_ANY_INSTANCING_ENABLED
                                            output.instanceID = input.instanceID;
                                            #endif // conditional
                                            return output;
                                        }
                                        //-------------------------------------------------------------------------------------
                                        // End Interpolator Packing And Struct Declarations
                                        //-------------------------------------------------------------------------------------

                                        //-------------------------------------------------------------------------------------
                                        // Graph generated code
                                        //-------------------------------------------------------------------------------------
                                                // Shared Graph Properties (uniform inputs)
                                                CBUFFER_START(UnityPerMaterial)
                                                float _Alpha;
                                                CBUFFER_END
                                                TEXTURE2D(_MainTex); SAMPLER(sampler_MainTex); float4 _MainTex_TexelSize;
                                                TEXTURE2D(Mask); SAMPLER(samplerMask); float4 Mask_TexelSize;
                                                SAMPLER(_SampleTexture2D_29C4BEA2_Sampler_3_Linear_Repeat);
                                                SAMPLER(_SampleTexture2D_2D75C97F_Sampler_3_Linear_Repeat);

                                                // Pixel Graph Inputs
                                                    struct SurfaceDescriptionInputs
                                                    {
                                                        float4 uv0; // optional
                                                    };
                                                    // Pixel Graph Outputs
                                                        struct SurfaceDescription
                                                        {
                                                            float3 Color;
                                                            float Alpha;
                                                            float AlphaClipThreshold;
                                                        };

                                                        // Shared Graph Node Functions

                                                            void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
                                                            {
                                                                Out = A * B;
                                                            }

                                                            void Unity_Power_float4(float4 A, float4 B, out float4 Out)
                                                            {
                                                                Out = pow(A, B);
                                                            }

                                                            void Unity_Subtract_float4(float4 A, float4 B, out float4 Out)
                                                            {
                                                                Out = A - B;
                                                            }

                                                            void Unity_Clamp_float4(float4 In, float4 Min, float4 Max, out float4 Out)
                                                            {
                                                                Out = clamp(In, Min, Max);
                                                            }

                                                            // Pixel Graph Evaluation
                                                                SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
                                                                {
                                                                    SurfaceDescription surface = (SurfaceDescription)0;
                                                                    float4 _SampleTexture2D_29C4BEA2_RGBA_0 = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, IN.uv0.xy);
                                                                    float _SampleTexture2D_29C4BEA2_R_4 = _SampleTexture2D_29C4BEA2_RGBA_0.r;
                                                                    float _SampleTexture2D_29C4BEA2_G_5 = _SampleTexture2D_29C4BEA2_RGBA_0.g;
                                                                    float _SampleTexture2D_29C4BEA2_B_6 = _SampleTexture2D_29C4BEA2_RGBA_0.b;
                                                                    float _SampleTexture2D_29C4BEA2_A_7 = _SampleTexture2D_29C4BEA2_RGBA_0.a;
                                                                    float4 _Multiply_3FD8CB6E_Out_2;
                                                                    Unity_Multiply_float(_SampleTexture2D_29C4BEA2_RGBA_0, float4(2, 2, 2, 2), _Multiply_3FD8CB6E_Out_2);
                                                                    float4 _SampleTexture2D_2D75C97F_RGBA_0 = SAMPLE_TEXTURE2D(Mask, samplerMask, IN.uv0.xy);
                                                                    float _SampleTexture2D_2D75C97F_R_4 = _SampleTexture2D_2D75C97F_RGBA_0.r;
                                                                    float _SampleTexture2D_2D75C97F_G_5 = _SampleTexture2D_2D75C97F_RGBA_0.g;
                                                                    float _SampleTexture2D_2D75C97F_B_6 = _SampleTexture2D_2D75C97F_RGBA_0.b;
                                                                    float _SampleTexture2D_2D75C97F_A_7 = _SampleTexture2D_2D75C97F_RGBA_0.a;
                                                                    float _Vector1_3D656997_Out_0 = 0.8;
                                                                    float4 _Power_4DAD70D9_Out_2;
                                                                    Unity_Power_float4(_SampleTexture2D_2D75C97F_RGBA_0, (_Vector1_3D656997_Out_0.xxxx), _Power_4DAD70D9_Out_2);
                                                                    float _Property_6BBE9849_Out_0 = _Alpha;
                                                                    float4 _Subtract_57CD835A_Out_2;
                                                                    Unity_Subtract_float4(_Power_4DAD70D9_Out_2, (_Property_6BBE9849_Out_0.xxxx), _Subtract_57CD835A_Out_2);
                                                                    float4 _Clamp_CF02927_Out_3;
                                                                    Unity_Clamp_float4(_Subtract_57CD835A_Out_2, float4(0, 0, 0, 0), float4(1, 1, 1, 1), _Clamp_CF02927_Out_3);
                                                                    float _Vector1_9997196B_Out_0 = 0;
                                                                    surface.Color = (_Multiply_3FD8CB6E_Out_2.xyz);
                                                                    surface.Alpha = (_Clamp_CF02927_Out_3).x;
                                                                    surface.AlphaClipThreshold = _Vector1_9997196B_Out_0;
                                                                    return surface;
                                                                }

                                                                //-------------------------------------------------------------------------------------
                                                                // End graph generated code
                                                                //-------------------------------------------------------------------------------------

                                                            // $include("VertexAnimation.template.hlsl")

                                                            //-------------------------------------------------------------------------------------
                                                                // TEMPLATE INCLUDE : SharedCode.template.hlsl
                                                                //-------------------------------------------------------------------------------------

                                                                    FragInputs BuildFragInputs(VaryingsMeshToPS input)
                                                                    {
                                                                        FragInputs output;
                                                                        ZERO_INITIALIZE(FragInputs, output);

                                                                        // Init to some default value to make the computer quiet (else it output 'divide by zero' warning even if value is not used).
                                                                        // TODO: this is a really poor workaround, but the variable is used in a bunch of places
                                                                        // to compute normals which are then passed on elsewhere to compute other values...
                                                                        output.tangentToWorld = k_identity3x3;
                                                                        output.positionSS = input.positionCS;       // input.positionCS is SV_Position

                                                                        // output.positionRWS = input.positionRWS;
                                                                        // output.tangentToWorld = BuildTangentToWorld(input.tangentWS, input.normalWS);
                                                                        output.texCoord0 = input.texCoord0;
                                                                        // output.texCoord1 = input.texCoord1;
                                                                        // output.texCoord2 = input.texCoord2;
                                                                        // output.texCoord3 = input.texCoord3;
                                                                        // output.color = input.color;
                                                                        #if _DOUBLESIDED_ON && SHADER_STAGE_FRAGMENT
                                                                        output.isFrontFace = IS_FRONT_VFACE(input.cullFace, true, false);
                                                                        #elif SHADER_STAGE_FRAGMENT
                                                                        // output.isFrontFace = IS_FRONT_VFACE(input.cullFace, true, false);
                                                                        #endif // SHADER_STAGE_FRAGMENT

                                                                        return output;
                                                                    }

                                                                    SurfaceDescriptionInputs FragInputsToSurfaceDescriptionInputs(FragInputs input, float3 viewWS)
                                                                    {
                                                                        SurfaceDescriptionInputs output;
                                                                        ZERO_INITIALIZE(SurfaceDescriptionInputs, output);

                                                                        // output.WorldSpaceNormal =            normalize(input.tangentToWorld[2].xyz);
                                                                        // output.ObjectSpaceNormal =           mul(output.WorldSpaceNormal, (float3x3) UNITY_MATRIX_M);           // transposed multiplication by inverse matrix to handle normal scale
                                                                        // output.ViewSpaceNormal =             mul(output.WorldSpaceNormal, (float3x3) UNITY_MATRIX_I_V);         // transposed multiplication by inverse matrix to handle normal scale
                                                                        // output.TangentSpaceNormal =          float3(0.0f, 0.0f, 1.0f);
                                                                        // output.WorldSpaceTangent =           input.tangentToWorld[0].xyz;
                                                                        // output.ObjectSpaceTangent =          TransformWorldToObjectDir(output.WorldSpaceTangent);
                                                                        // output.ViewSpaceTangent =            TransformWorldToViewDir(output.WorldSpaceTangent);
                                                                        // output.TangentSpaceTangent =         float3(1.0f, 0.0f, 0.0f);
                                                                        // output.WorldSpaceBiTangent =         input.tangentToWorld[1].xyz;
                                                                        // output.ObjectSpaceBiTangent =        TransformWorldToObjectDir(output.WorldSpaceBiTangent);
                                                                        // output.ViewSpaceBiTangent =          TransformWorldToViewDir(output.WorldSpaceBiTangent);
                                                                        // output.TangentSpaceBiTangent =       float3(0.0f, 1.0f, 0.0f);
                                                                        // output.WorldSpaceViewDirection =     normalize(viewWS);
                                                                        // output.ObjectSpaceViewDirection =    TransformWorldToObjectDir(output.WorldSpaceViewDirection);
                                                                        // output.ViewSpaceViewDirection =      TransformWorldToViewDir(output.WorldSpaceViewDirection);
                                                                        // float3x3 tangentSpaceTransform =     float3x3(output.WorldSpaceTangent,output.WorldSpaceBiTangent,output.WorldSpaceNormal);
                                                                        // output.TangentSpaceViewDirection =   mul(tangentSpaceTransform, output.WorldSpaceViewDirection);
                                                                        // output.WorldSpacePosition =          input.positionRWS;
                                                                        // output.ObjectSpacePosition =         TransformWorldToObject(input.positionRWS);
                                                                        // output.ViewSpacePosition =           TransformWorldToView(input.positionRWS);
                                                                        // output.TangentSpacePosition =        float3(0.0f, 0.0f, 0.0f);
                                                                        // output.AbsoluteWorldSpacePosition =  GetAbsolutePositionWS(input.positionRWS);
                                                                        // output.ScreenPosition =              ComputeScreenPos(TransformWorldToHClip(input.positionRWS), _ProjectionParams.x);
                                                                        output.uv0 = input.texCoord0;
                                                                        // output.uv1 =                         input.texCoord1;
                                                                        // output.uv2 =                         input.texCoord2;
                                                                        // output.uv3 =                         input.texCoord3;
                                                                        // output.VertexColor =                 input.color;
                                                                        // output.FaceSign =                    input.isFrontFace;
                                                                        // output.TimeParameters =              _TimeParameters.xyz; // This is mainly for LW as HD overwrite this value

                                                                        return output;
                                                                    }

                                                                    // existing HDRP code uses the combined function to go directly from packed to frag inputs
                                                                    FragInputs UnpackVaryingsMeshToFragInputs(PackedVaryingsMeshToPS input)
                                                                    {
                                                                        UNITY_SETUP_INSTANCE_ID(input);
                                                                        VaryingsMeshToPS unpacked = UnpackVaryingsMeshToPS(input);
                                                                        return BuildFragInputs(unpacked);
                                                                    }

                                                                    //-------------------------------------------------------------------------------------
                                                                    // END TEMPLATE INCLUDE : SharedCode.template.hlsl
                                                                    //-------------------------------------------------------------------------------------



                                                                    void BuildSurfaceData(FragInputs fragInputs, inout SurfaceDescription surfaceDescription, float3 V, PositionInputs posInput, out SurfaceData surfaceData)
                                                                    {
                                                                        // setup defaults -- these are used if the graph doesn't output a value
                                                                        ZERO_INITIALIZE(SurfaceData, surfaceData);

                                                                        // copy across graph values, if defined
                                                                        surfaceData.color = surfaceDescription.Color;

                                                                #if defined(DEBUG_DISPLAY)
                                                                        if (_DebugMipMapMode != DEBUGMIPMAPMODE_NONE)
                                                                        {
                                                                            // TODO
                                                                        }
                                                                #endif
                                                                    }

                                                                    void GetSurfaceAndBuiltinData(FragInputs fragInputs, float3 V, inout PositionInputs posInput, out SurfaceData surfaceData, out BuiltinData builtinData)
                                                                    {
                                                                        SurfaceDescriptionInputs surfaceDescriptionInputs = FragInputsToSurfaceDescriptionInputs(fragInputs, V);
                                                                        SurfaceDescription surfaceDescription = SurfaceDescriptionFunction(surfaceDescriptionInputs);

                                                                        // Perform alpha test very early to save performance (a killed pixel will not sample textures)
                                                                        // TODO: split graph evaluation to grab just alpha dependencies first? tricky..
                                                                        DoAlphaTest(surfaceDescription.Alpha, surfaceDescription.AlphaClipThreshold);

                                                                        BuildSurfaceData(fragInputs, surfaceDescription, V, posInput, surfaceData);

                                                                        // Builtin Data
                                                                        ZERO_INITIALIZE(BuiltinData, builtinData); // No call to InitBuiltinData as we don't have any lighting
                                                                        builtinData.opacity = surfaceDescription.Alpha;
                                                                    }

                                                                    //-------------------------------------------------------------------------------------
                                                                    // Pass Includes
                                                                    //-------------------------------------------------------------------------------------
                                                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/ShaderPassLightTransport.hlsl"
                                                                    //-------------------------------------------------------------------------------------
                                                                    // End Pass Includes
                                                                    //-------------------------------------------------------------------------------------

                                                                    ENDHLSL
                                                                }

                                                                Pass
                                                                {
                                                                        // based on UnlitPass.template
                                                                        Name "SceneSelectionPass"
                                                                        Tags { "LightMode" = "SceneSelectionPass" }

                                                                        //-------------------------------------------------------------------------------------
                                                                        // Render Modes (Blend, Cull, ZTest, Stencil, etc)
                                                                        //-------------------------------------------------------------------------------------

                                                                        Cull Off


                                                                        ZWrite Off
                                                                    ZTest Always


                                                                        ColorMask 0

                                                                        //-------------------------------------------------------------------------------------
                                                                        // End Render Modes
                                                                        //-------------------------------------------------------------------------------------

                                                                        HLSLPROGRAM

                                                                        #pragma target 4.5
                                                                        #pragma only_renderers d3d11 ps4 xboxone vulkan metal switch
                                                                        //#pragma enable_d3d11_debug_symbols

                                                                        //enable GPU instancing support
                                                                        #pragma multi_compile_instancing

                                                                        //-------------------------------------------------------------------------------------
                                                                        // Variant Definitions (active field translations to HDRP defines)
                                                                        //-------------------------------------------------------------------------------------
                                                                        #define _SURFACE_TYPE_TRANSPARENT 1
                                                                        #define _BLENDMODE_ALPHA 1
                                                                        // #define _BLENDMODE_ADD 1
                                                                        // #define _BLENDMODE_PRE_MULTIPLY 1
                                                                        // #define _ADD_PRECOMPUTED_VELOCITY

                                                                        //-------------------------------------------------------------------------------------
                                                                        // End Variant Definitions
                                                                        //-------------------------------------------------------------------------------------

                                                                        #pragma vertex Vert
                                                                        #pragma fragment Frag

                                                                        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
                                                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderVariables.hlsl"
                                                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/FragInputs.hlsl"
                                                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/ShaderPass.cs.hlsl"

                                                                        //-------------------------------------------------------------------------------------
                                                                        // Defines
                                                                        //-------------------------------------------------------------------------------------
                                                                                #define SHADERPASS SHADERPASS_DEPTH_ONLY
                                                                            #define SCENESELECTIONPASS
                                                                            #pragma editor_sync_compilation
                                                                            // ACTIVE FIELDS:
                                                                            //   AlphaTest
                                                                            //   SurfaceType.Transparent
                                                                            //   BlendMode.Alpha
                                                                            //   SurfaceDescriptionInputs.uv0
                                                                            //   VertexDescriptionInputs.ObjectSpaceNormal
                                                                            //   VertexDescriptionInputs.ObjectSpaceTangent
                                                                            //   VertexDescriptionInputs.ObjectSpacePosition
                                                                            //   SurfaceDescription.Alpha
                                                                            //   SurfaceDescription.AlphaClipThreshold
                                                                            //   FragInputs.texCoord0
                                                                            //   AttributesMesh.normalOS
                                                                            //   AttributesMesh.tangentOS
                                                                            //   AttributesMesh.positionOS
                                                                            //   VaryingsMeshToPS.texCoord0
                                                                            //   AttributesMesh.uv0
                                                                            // Shared Graph Keywords

                                                                        // this translates the new dependency tracker into the old preprocessor definitions for the existing HDRP shader code
                                                                        #define ATTRIBUTES_NEED_NORMAL
                                                                        #define ATTRIBUTES_NEED_TANGENT
                                                                        #define ATTRIBUTES_NEED_TEXCOORD0
                                                                        // #define ATTRIBUTES_NEED_TEXCOORD1
                                                                        // #define ATTRIBUTES_NEED_TEXCOORD2
                                                                        // #define ATTRIBUTES_NEED_TEXCOORD3
                                                                        // #define ATTRIBUTES_NEED_COLOR
                                                                        // #define VARYINGS_NEED_POSITION_WS
                                                                        // #define VARYINGS_NEED_TANGENT_TO_WORLD
                                                                        #define VARYINGS_NEED_TEXCOORD0
                                                                        // #define VARYINGS_NEED_TEXCOORD1
                                                                        // #define VARYINGS_NEED_TEXCOORD2
                                                                        // #define VARYINGS_NEED_TEXCOORD3
                                                                        // #define VARYINGS_NEED_COLOR
                                                                        // #define VARYINGS_NEED_CULLFACE
                                                                        // #define HAVE_MESH_MODIFICATION

                                                                        //-------------------------------------------------------------------------------------
                                                                        // End Defines
                                                                        //-------------------------------------------------------------------------------------


                                                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/Material.hlsl"
                                                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/Unlit/Unlit.hlsl"

                                                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/BuiltinUtilities.hlsl"
                                                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/MaterialUtilities.hlsl"
                                                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderGraphFunctions.hlsl"

                                                                        // Used by SceneSelectionPass
                                                                        int _ObjectId;
                                                                        int _PassValue;

                                                                        //-------------------------------------------------------------------------------------
                                                                        // Interpolator Packing And Struct Declarations
                                                                        //-------------------------------------------------------------------------------------
                                                                        // Generated Type: AttributesMesh
                                                                        struct AttributesMesh
                                                                        {
                                                                            float3 positionOS : POSITION;
                                                                            float3 normalOS : NORMAL; // optional
                                                                            float4 tangentOS : TANGENT; // optional
                                                                            float4 uv0 : TEXCOORD0; // optional
                                                                            #if UNITY_ANY_INSTANCING_ENABLED
                                                                            uint instanceID : INSTANCEID_SEMANTIC;
                                                                            #endif // UNITY_ANY_INSTANCING_ENABLED
                                                                        };
                                                                        // Generated Type: VaryingsMeshToPS
                                                                        struct VaryingsMeshToPS
                                                                        {
                                                                            float4 positionCS : SV_Position;
                                                                            float4 texCoord0; // optional
                                                                            #if UNITY_ANY_INSTANCING_ENABLED
                                                                            uint instanceID : CUSTOM_INSTANCE_ID;
                                                                            #endif // UNITY_ANY_INSTANCING_ENABLED
                                                                            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                                                                            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
                                                                            #endif // defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                                                                        };

                                                                        // Generated Type: PackedVaryingsMeshToPS
                                                                        struct PackedVaryingsMeshToPS
                                                                        {
                                                                            float4 positionCS : SV_Position; // unpacked
                                                                            #if UNITY_ANY_INSTANCING_ENABLED
                                                                            uint instanceID : CUSTOM_INSTANCE_ID; // unpacked
                                                                            #endif // conditional
                                                                            float4 interp00 : TEXCOORD0; // auto-packed
                                                                            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                                                                            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC; // unpacked
                                                                            #endif // conditional
                                                                        };

                                                                        // Packed Type: VaryingsMeshToPS
                                                                        PackedVaryingsMeshToPS PackVaryingsMeshToPS(VaryingsMeshToPS input)
                                                                        {
                                                                            PackedVaryingsMeshToPS output;
                                                                            output.positionCS = input.positionCS;
                                                                            output.interp00.xyzw = input.texCoord0;
                                                                            #if UNITY_ANY_INSTANCING_ENABLED
                                                                            output.instanceID = input.instanceID;
                                                                            #endif // conditional
                                                                            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                                                                            output.cullFace = input.cullFace;
                                                                            #endif // conditional
                                                                            return output;
                                                                        }

                                                                        // Unpacked Type: VaryingsMeshToPS
                                                                        VaryingsMeshToPS UnpackVaryingsMeshToPS(PackedVaryingsMeshToPS input)
                                                                        {
                                                                            VaryingsMeshToPS output;
                                                                            output.positionCS = input.positionCS;
                                                                            output.texCoord0 = input.interp00.xyzw;
                                                                            #if UNITY_ANY_INSTANCING_ENABLED
                                                                            output.instanceID = input.instanceID;
                                                                            #endif // conditional
                                                                            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                                                                            output.cullFace = input.cullFace;
                                                                            #endif // conditional
                                                                            return output;
                                                                        }
                                                                        // Generated Type: VaryingsMeshToDS
                                                                        struct VaryingsMeshToDS
                                                                        {
                                                                            float3 positionRWS;
                                                                            float3 normalWS;
                                                                            #if UNITY_ANY_INSTANCING_ENABLED
                                                                            uint instanceID : CUSTOM_INSTANCE_ID;
                                                                            #endif // UNITY_ANY_INSTANCING_ENABLED
                                                                        };

                                                                        // Generated Type: PackedVaryingsMeshToDS
                                                                        struct PackedVaryingsMeshToDS
                                                                        {
                                                                            #if UNITY_ANY_INSTANCING_ENABLED
                                                                            uint instanceID : CUSTOM_INSTANCE_ID; // unpacked
                                                                            #endif // conditional
                                                                            float3 interp00 : TEXCOORD0; // auto-packed
                                                                            float3 interp01 : TEXCOORD1; // auto-packed
                                                                        };

                                                                        // Packed Type: VaryingsMeshToDS
                                                                        PackedVaryingsMeshToDS PackVaryingsMeshToDS(VaryingsMeshToDS input)
                                                                        {
                                                                            PackedVaryingsMeshToDS output;
                                                                            output.interp00.xyz = input.positionRWS;
                                                                            output.interp01.xyz = input.normalWS;
                                                                            #if UNITY_ANY_INSTANCING_ENABLED
                                                                            output.instanceID = input.instanceID;
                                                                            #endif // conditional
                                                                            return output;
                                                                        }

                                                                        // Unpacked Type: VaryingsMeshToDS
                                                                        VaryingsMeshToDS UnpackVaryingsMeshToDS(PackedVaryingsMeshToDS input)
                                                                        {
                                                                            VaryingsMeshToDS output;
                                                                            output.positionRWS = input.interp00.xyz;
                                                                            output.normalWS = input.interp01.xyz;
                                                                            #if UNITY_ANY_INSTANCING_ENABLED
                                                                            output.instanceID = input.instanceID;
                                                                            #endif // conditional
                                                                            return output;
                                                                        }
                                                                        //-------------------------------------------------------------------------------------
                                                                        // End Interpolator Packing And Struct Declarations
                                                                        //-------------------------------------------------------------------------------------

                                                                        //-------------------------------------------------------------------------------------
                                                                        // Graph generated code
                                                                        //-------------------------------------------------------------------------------------
                                                                                // Shared Graph Properties (uniform inputs)
                                                                                CBUFFER_START(UnityPerMaterial)
                                                                                float _Alpha;
                                                                                CBUFFER_END
                                                                                TEXTURE2D(_MainTex); SAMPLER(sampler_MainTex); float4 _MainTex_TexelSize;
                                                                                TEXTURE2D(Mask); SAMPLER(samplerMask); float4 Mask_TexelSize;
                                                                                SAMPLER(_SampleTexture2D_2D75C97F_Sampler_3_Linear_Repeat);

                                                                                // Pixel Graph Inputs
                                                                                    struct SurfaceDescriptionInputs
                                                                                    {
                                                                                        float4 uv0; // optional
                                                                                    };
                                                                                    // Pixel Graph Outputs
                                                                                        struct SurfaceDescription
                                                                                        {
                                                                                            float Alpha;
                                                                                            float AlphaClipThreshold;
                                                                                        };

                                                                                        // Shared Graph Node Functions

                                                                                            void Unity_Power_float4(float4 A, float4 B, out float4 Out)
                                                                                            {
                                                                                                Out = pow(A, B);
                                                                                            }

                                                                                            void Unity_Subtract_float4(float4 A, float4 B, out float4 Out)
                                                                                            {
                                                                                                Out = A - B;
                                                                                            }

                                                                                            void Unity_Clamp_float4(float4 In, float4 Min, float4 Max, out float4 Out)
                                                                                            {
                                                                                                Out = clamp(In, Min, Max);
                                                                                            }

                                                                                            // Pixel Graph Evaluation
                                                                                                SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
                                                                                                {
                                                                                                    SurfaceDescription surface = (SurfaceDescription)0;
                                                                                                    float4 _SampleTexture2D_2D75C97F_RGBA_0 = SAMPLE_TEXTURE2D(Mask, samplerMask, IN.uv0.xy);
                                                                                                    float _SampleTexture2D_2D75C97F_R_4 = _SampleTexture2D_2D75C97F_RGBA_0.r;
                                                                                                    float _SampleTexture2D_2D75C97F_G_5 = _SampleTexture2D_2D75C97F_RGBA_0.g;
                                                                                                    float _SampleTexture2D_2D75C97F_B_6 = _SampleTexture2D_2D75C97F_RGBA_0.b;
                                                                                                    float _SampleTexture2D_2D75C97F_A_7 = _SampleTexture2D_2D75C97F_RGBA_0.a;
                                                                                                    float _Vector1_3D656997_Out_0 = 0.8;
                                                                                                    float4 _Power_4DAD70D9_Out_2;
                                                                                                    Unity_Power_float4(_SampleTexture2D_2D75C97F_RGBA_0, (_Vector1_3D656997_Out_0.xxxx), _Power_4DAD70D9_Out_2);
                                                                                                    float _Property_6BBE9849_Out_0 = _Alpha;
                                                                                                    float4 _Subtract_57CD835A_Out_2;
                                                                                                    Unity_Subtract_float4(_Power_4DAD70D9_Out_2, (_Property_6BBE9849_Out_0.xxxx), _Subtract_57CD835A_Out_2);
                                                                                                    float4 _Clamp_CF02927_Out_3;
                                                                                                    Unity_Clamp_float4(_Subtract_57CD835A_Out_2, float4(0, 0, 0, 0), float4(1, 1, 1, 1), _Clamp_CF02927_Out_3);
                                                                                                    float _Vector1_9997196B_Out_0 = 0;
                                                                                                    surface.Alpha = (_Clamp_CF02927_Out_3).x;
                                                                                                    surface.AlphaClipThreshold = _Vector1_9997196B_Out_0;
                                                                                                    return surface;
                                                                                                }

                                                                                                //-------------------------------------------------------------------------------------
                                                                                                // End graph generated code
                                                                                                //-------------------------------------------------------------------------------------

                                                                                            // $include("VertexAnimation.template.hlsl")

                                                                                            //-------------------------------------------------------------------------------------
                                                                                                // TEMPLATE INCLUDE : SharedCode.template.hlsl
                                                                                                //-------------------------------------------------------------------------------------

                                                                                                    FragInputs BuildFragInputs(VaryingsMeshToPS input)
                                                                                                    {
                                                                                                        FragInputs output;
                                                                                                        ZERO_INITIALIZE(FragInputs, output);

                                                                                                        // Init to some default value to make the computer quiet (else it output 'divide by zero' warning even if value is not used).
                                                                                                        // TODO: this is a really poor workaround, but the variable is used in a bunch of places
                                                                                                        // to compute normals which are then passed on elsewhere to compute other values...
                                                                                                        output.tangentToWorld = k_identity3x3;
                                                                                                        output.positionSS = input.positionCS;       // input.positionCS is SV_Position

                                                                                                        // output.positionRWS = input.positionRWS;
                                                                                                        // output.tangentToWorld = BuildTangentToWorld(input.tangentWS, input.normalWS);
                                                                                                        output.texCoord0 = input.texCoord0;
                                                                                                        // output.texCoord1 = input.texCoord1;
                                                                                                        // output.texCoord2 = input.texCoord2;
                                                                                                        // output.texCoord3 = input.texCoord3;
                                                                                                        // output.color = input.color;
                                                                                                        #if _DOUBLESIDED_ON && SHADER_STAGE_FRAGMENT
                                                                                                        output.isFrontFace = IS_FRONT_VFACE(input.cullFace, true, false);
                                                                                                        #elif SHADER_STAGE_FRAGMENT
                                                                                                        // output.isFrontFace = IS_FRONT_VFACE(input.cullFace, true, false);
                                                                                                        #endif // SHADER_STAGE_FRAGMENT

                                                                                                        return output;
                                                                                                    }

                                                                                                    SurfaceDescriptionInputs FragInputsToSurfaceDescriptionInputs(FragInputs input, float3 viewWS)
                                                                                                    {
                                                                                                        SurfaceDescriptionInputs output;
                                                                                                        ZERO_INITIALIZE(SurfaceDescriptionInputs, output);

                                                                                                        // output.WorldSpaceNormal =            normalize(input.tangentToWorld[2].xyz);
                                                                                                        // output.ObjectSpaceNormal =           mul(output.WorldSpaceNormal, (float3x3) UNITY_MATRIX_M);           // transposed multiplication by inverse matrix to handle normal scale
                                                                                                        // output.ViewSpaceNormal =             mul(output.WorldSpaceNormal, (float3x3) UNITY_MATRIX_I_V);         // transposed multiplication by inverse matrix to handle normal scale
                                                                                                        // output.TangentSpaceNormal =          float3(0.0f, 0.0f, 1.0f);
                                                                                                        // output.WorldSpaceTangent =           input.tangentToWorld[0].xyz;
                                                                                                        // output.ObjectSpaceTangent =          TransformWorldToObjectDir(output.WorldSpaceTangent);
                                                                                                        // output.ViewSpaceTangent =            TransformWorldToViewDir(output.WorldSpaceTangent);
                                                                                                        // output.TangentSpaceTangent =         float3(1.0f, 0.0f, 0.0f);
                                                                                                        // output.WorldSpaceBiTangent =         input.tangentToWorld[1].xyz;
                                                                                                        // output.ObjectSpaceBiTangent =        TransformWorldToObjectDir(output.WorldSpaceBiTangent);
                                                                                                        // output.ViewSpaceBiTangent =          TransformWorldToViewDir(output.WorldSpaceBiTangent);
                                                                                                        // output.TangentSpaceBiTangent =       float3(0.0f, 1.0f, 0.0f);
                                                                                                        // output.WorldSpaceViewDirection =     normalize(viewWS);
                                                                                                        // output.ObjectSpaceViewDirection =    TransformWorldToObjectDir(output.WorldSpaceViewDirection);
                                                                                                        // output.ViewSpaceViewDirection =      TransformWorldToViewDir(output.WorldSpaceViewDirection);
                                                                                                        // float3x3 tangentSpaceTransform =     float3x3(output.WorldSpaceTangent,output.WorldSpaceBiTangent,output.WorldSpaceNormal);
                                                                                                        // output.TangentSpaceViewDirection =   mul(tangentSpaceTransform, output.WorldSpaceViewDirection);
                                                                                                        // output.WorldSpacePosition =          input.positionRWS;
                                                                                                        // output.ObjectSpacePosition =         TransformWorldToObject(input.positionRWS);
                                                                                                        // output.ViewSpacePosition =           TransformWorldToView(input.positionRWS);
                                                                                                        // output.TangentSpacePosition =        float3(0.0f, 0.0f, 0.0f);
                                                                                                        // output.AbsoluteWorldSpacePosition =  GetAbsolutePositionWS(input.positionRWS);
                                                                                                        // output.ScreenPosition =              ComputeScreenPos(TransformWorldToHClip(input.positionRWS), _ProjectionParams.x);
                                                                                                        output.uv0 = input.texCoord0;
                                                                                                        // output.uv1 =                         input.texCoord1;
                                                                                                        // output.uv2 =                         input.texCoord2;
                                                                                                        // output.uv3 =                         input.texCoord3;
                                                                                                        // output.VertexColor =                 input.color;
                                                                                                        // output.FaceSign =                    input.isFrontFace;
                                                                                                        // output.TimeParameters =              _TimeParameters.xyz; // This is mainly for LW as HD overwrite this value

                                                                                                        return output;
                                                                                                    }

                                                                                                    // existing HDRP code uses the combined function to go directly from packed to frag inputs
                                                                                                    FragInputs UnpackVaryingsMeshToFragInputs(PackedVaryingsMeshToPS input)
                                                                                                    {
                                                                                                        UNITY_SETUP_INSTANCE_ID(input);
                                                                                                        VaryingsMeshToPS unpacked = UnpackVaryingsMeshToPS(input);
                                                                                                        return BuildFragInputs(unpacked);
                                                                                                    }

                                                                                                    //-------------------------------------------------------------------------------------
                                                                                                    // END TEMPLATE INCLUDE : SharedCode.template.hlsl
                                                                                                    //-------------------------------------------------------------------------------------



                                                                                                    void BuildSurfaceData(FragInputs fragInputs, inout SurfaceDescription surfaceDescription, float3 V, PositionInputs posInput, out SurfaceData surfaceData)
                                                                                                    {
                                                                                                        // setup defaults -- these are used if the graph doesn't output a value
                                                                                                        ZERO_INITIALIZE(SurfaceData, surfaceData);

                                                                                                        // copy across graph values, if defined
                                                                                                        // surfaceData.color = surfaceDescription.Color;

                                                                                                #if defined(DEBUG_DISPLAY)
                                                                                                        if (_DebugMipMapMode != DEBUGMIPMAPMODE_NONE)
                                                                                                        {
                                                                                                            // TODO
                                                                                                        }
                                                                                                #endif
                                                                                                    }

                                                                                                    void GetSurfaceAndBuiltinData(FragInputs fragInputs, float3 V, inout PositionInputs posInput, out SurfaceData surfaceData, out BuiltinData builtinData)
                                                                                                    {
                                                                                                        SurfaceDescriptionInputs surfaceDescriptionInputs = FragInputsToSurfaceDescriptionInputs(fragInputs, V);
                                                                                                        SurfaceDescription surfaceDescription = SurfaceDescriptionFunction(surfaceDescriptionInputs);

                                                                                                        // Perform alpha test very early to save performance (a killed pixel will not sample textures)
                                                                                                        // TODO: split graph evaluation to grab just alpha dependencies first? tricky..
                                                                                                        DoAlphaTest(surfaceDescription.Alpha, surfaceDescription.AlphaClipThreshold);

                                                                                                        BuildSurfaceData(fragInputs, surfaceDescription, V, posInput, surfaceData);

                                                                                                        // Builtin Data
                                                                                                        ZERO_INITIALIZE(BuiltinData, builtinData); // No call to InitBuiltinData as we don't have any lighting
                                                                                                        builtinData.opacity = surfaceDescription.Alpha;
                                                                                                    }

                                                                                                    //-------------------------------------------------------------------------------------
                                                                                                    // Pass Includes
                                                                                                    //-------------------------------------------------------------------------------------
                                                                                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/ShaderPassDepthOnly.hlsl"
                                                                                                    //-------------------------------------------------------------------------------------
                                                                                                    // End Pass Includes
                                                                                                    //-------------------------------------------------------------------------------------

                                                                                                    ENDHLSL
                                                                                                }

                                                                                                Pass
                                                                                                {
                                                                                                        // based on UnlitPass.template
                                                                                                        Name "ForwardOnly"
                                                                                                        Tags { "LightMode" = "ForwardOnly" }

                                                                                                        //-------------------------------------------------------------------------------------
                                                                                                        // Render Modes (Blend, Cull, ZTest, Stencil, etc)
                                                                                                        //-------------------------------------------------------------------------------------
                                                                                                        Blend One OneMinusSrcAlpha, One OneMinusSrcAlpha

                                                                                                        Cull Off


                                                                                                        ZWrite Off
                                                                                                    ZTest Always

                                                                                                        // Stencil setup
                                                                                                    Stencil
                                                                                                    {
                                                                                                       WriteMask 3
                                                                                                       Ref  0
                                                                                                       Comp Always
                                                                                                       Pass Replace
                                                                                                    }


                                                                                                        //-------------------------------------------------------------------------------------
                                                                                                        // End Render Modes
                                                                                                        //-------------------------------------------------------------------------------------

                                                                                                        HLSLPROGRAM

                                                                                                        #pragma target 4.5
                                                                                                        #pragma only_renderers d3d11 ps4 xboxone vulkan metal switch
                                                                                                        //#pragma enable_d3d11_debug_symbols

                                                                                                        //enable GPU instancing support
                                                                                                        #pragma multi_compile_instancing

                                                                                                        //-------------------------------------------------------------------------------------
                                                                                                        // Variant Definitions (active field translations to HDRP defines)
                                                                                                        //-------------------------------------------------------------------------------------
                                                                                                        #define _SURFACE_TYPE_TRANSPARENT 1
                                                                                                        #define _BLENDMODE_ALPHA 1
                                                                                                        // #define _BLENDMODE_ADD 1
                                                                                                        // #define _BLENDMODE_PRE_MULTIPLY 1
                                                                                                        // #define _ADD_PRECOMPUTED_VELOCITY

                                                                                                        //-------------------------------------------------------------------------------------
                                                                                                        // End Variant Definitions
                                                                                                        //-------------------------------------------------------------------------------------

                                                                                                        #pragma vertex Vert
                                                                                                        #pragma fragment Frag

                                                                                                        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
                                                                                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderVariables.hlsl"
                                                                                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/FragInputs.hlsl"
                                                                                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/ShaderPass.cs.hlsl"

                                                                                                        //-------------------------------------------------------------------------------------
                                                                                                        // Defines
                                                                                                        //-------------------------------------------------------------------------------------
                                                                                                                #define SHADERPASS SHADERPASS_FORWARD_UNLIT
                                                                                                            #pragma multi_compile _ DEBUG_DISPLAY
                                                                                                            // ACTIVE FIELDS:
                                                                                                            //   AlphaTest
                                                                                                            //   SurfaceType.Transparent
                                                                                                            //   BlendMode.Alpha
                                                                                                            //   SurfaceDescriptionInputs.uv0
                                                                                                            //   VertexDescriptionInputs.ObjectSpaceNormal
                                                                                                            //   VertexDescriptionInputs.ObjectSpaceTangent
                                                                                                            //   VertexDescriptionInputs.ObjectSpacePosition
                                                                                                            //   SurfaceDescription.Color
                                                                                                            //   SurfaceDescription.Alpha
                                                                                                            //   SurfaceDescription.AlphaClipThreshold
                                                                                                            //   FragInputs.texCoord0
                                                                                                            //   AttributesMesh.normalOS
                                                                                                            //   AttributesMesh.tangentOS
                                                                                                            //   AttributesMesh.positionOS
                                                                                                            //   VaryingsMeshToPS.texCoord0
                                                                                                            //   AttributesMesh.uv0
                                                                                                            // Shared Graph Keywords

                                                                                                        // this translates the new dependency tracker into the old preprocessor definitions for the existing HDRP shader code
                                                                                                        #define ATTRIBUTES_NEED_NORMAL
                                                                                                        #define ATTRIBUTES_NEED_TANGENT
                                                                                                        #define ATTRIBUTES_NEED_TEXCOORD0
                                                                                                        // #define ATTRIBUTES_NEED_TEXCOORD1
                                                                                                        // #define ATTRIBUTES_NEED_TEXCOORD2
                                                                                                        // #define ATTRIBUTES_NEED_TEXCOORD3
                                                                                                        // #define ATTRIBUTES_NEED_COLOR
                                                                                                        // #define VARYINGS_NEED_POSITION_WS
                                                                                                        // #define VARYINGS_NEED_TANGENT_TO_WORLD
                                                                                                        #define VARYINGS_NEED_TEXCOORD0
                                                                                                        // #define VARYINGS_NEED_TEXCOORD1
                                                                                                        // #define VARYINGS_NEED_TEXCOORD2
                                                                                                        // #define VARYINGS_NEED_TEXCOORD3
                                                                                                        // #define VARYINGS_NEED_COLOR
                                                                                                        // #define VARYINGS_NEED_CULLFACE
                                                                                                        // #define HAVE_MESH_MODIFICATION

                                                                                                        //-------------------------------------------------------------------------------------
                                                                                                        // End Defines
                                                                                                        //-------------------------------------------------------------------------------------


                                                                                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/Material.hlsl"
                                                                                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/Unlit/Unlit.hlsl"

                                                                                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/BuiltinUtilities.hlsl"
                                                                                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/MaterialUtilities.hlsl"
                                                                                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderGraphFunctions.hlsl"

                                                                                                        // Used by SceneSelectionPass
                                                                                                        int _ObjectId;
                                                                                                        int _PassValue;

                                                                                                        //-------------------------------------------------------------------------------------
                                                                                                        // Interpolator Packing And Struct Declarations
                                                                                                        //-------------------------------------------------------------------------------------
                                                                                                        // Generated Type: AttributesMesh
                                                                                                        struct AttributesMesh
                                                                                                        {
                                                                                                            float3 positionOS : POSITION;
                                                                                                            float3 normalOS : NORMAL; // optional
                                                                                                            float4 tangentOS : TANGENT; // optional
                                                                                                            float4 uv0 : TEXCOORD0; // optional
                                                                                                            #if UNITY_ANY_INSTANCING_ENABLED
                                                                                                            uint instanceID : INSTANCEID_SEMANTIC;
                                                                                                            #endif // UNITY_ANY_INSTANCING_ENABLED
                                                                                                        };
                                                                                                        // Generated Type: VaryingsMeshToPS
                                                                                                        struct VaryingsMeshToPS
                                                                                                        {
                                                                                                            float4 positionCS : SV_Position;
                                                                                                            float4 texCoord0; // optional
                                                                                                            #if UNITY_ANY_INSTANCING_ENABLED
                                                                                                            uint instanceID : CUSTOM_INSTANCE_ID;
                                                                                                            #endif // UNITY_ANY_INSTANCING_ENABLED
                                                                                                            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                                                                                                            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
                                                                                                            #endif // defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                                                                                                        };

                                                                                                        // Generated Type: PackedVaryingsMeshToPS
                                                                                                        struct PackedVaryingsMeshToPS
                                                                                                        {
                                                                                                            float4 positionCS : SV_Position; // unpacked
                                                                                                            #if UNITY_ANY_INSTANCING_ENABLED
                                                                                                            uint instanceID : CUSTOM_INSTANCE_ID; // unpacked
                                                                                                            #endif // conditional
                                                                                                            float4 interp00 : TEXCOORD0; // auto-packed
                                                                                                            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                                                                                                            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC; // unpacked
                                                                                                            #endif // conditional
                                                                                                        };

                                                                                                        // Packed Type: VaryingsMeshToPS
                                                                                                        PackedVaryingsMeshToPS PackVaryingsMeshToPS(VaryingsMeshToPS input)
                                                                                                        {
                                                                                                            PackedVaryingsMeshToPS output;
                                                                                                            output.positionCS = input.positionCS;
                                                                                                            output.interp00.xyzw = input.texCoord0;
                                                                                                            #if UNITY_ANY_INSTANCING_ENABLED
                                                                                                            output.instanceID = input.instanceID;
                                                                                                            #endif // conditional
                                                                                                            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                                                                                                            output.cullFace = input.cullFace;
                                                                                                            #endif // conditional
                                                                                                            return output;
                                                                                                        }

                                                                                                        // Unpacked Type: VaryingsMeshToPS
                                                                                                        VaryingsMeshToPS UnpackVaryingsMeshToPS(PackedVaryingsMeshToPS input)
                                                                                                        {
                                                                                                            VaryingsMeshToPS output;
                                                                                                            output.positionCS = input.positionCS;
                                                                                                            output.texCoord0 = input.interp00.xyzw;
                                                                                                            #if UNITY_ANY_INSTANCING_ENABLED
                                                                                                            output.instanceID = input.instanceID;
                                                                                                            #endif // conditional
                                                                                                            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                                                                                                            output.cullFace = input.cullFace;
                                                                                                            #endif // conditional
                                                                                                            return output;
                                                                                                        }
                                                                                                        // Generated Type: VaryingsMeshToDS
                                                                                                        struct VaryingsMeshToDS
                                                                                                        {
                                                                                                            float3 positionRWS;
                                                                                                            float3 normalWS;
                                                                                                            #if UNITY_ANY_INSTANCING_ENABLED
                                                                                                            uint instanceID : CUSTOM_INSTANCE_ID;
                                                                                                            #endif // UNITY_ANY_INSTANCING_ENABLED
                                                                                                        };

                                                                                                        // Generated Type: PackedVaryingsMeshToDS
                                                                                                        struct PackedVaryingsMeshToDS
                                                                                                        {
                                                                                                            #if UNITY_ANY_INSTANCING_ENABLED
                                                                                                            uint instanceID : CUSTOM_INSTANCE_ID; // unpacked
                                                                                                            #endif // conditional
                                                                                                            float3 interp00 : TEXCOORD0; // auto-packed
                                                                                                            float3 interp01 : TEXCOORD1; // auto-packed
                                                                                                        };

                                                                                                        // Packed Type: VaryingsMeshToDS
                                                                                                        PackedVaryingsMeshToDS PackVaryingsMeshToDS(VaryingsMeshToDS input)
                                                                                                        {
                                                                                                            PackedVaryingsMeshToDS output;
                                                                                                            output.interp00.xyz = input.positionRWS;
                                                                                                            output.interp01.xyz = input.normalWS;
                                                                                                            #if UNITY_ANY_INSTANCING_ENABLED
                                                                                                            output.instanceID = input.instanceID;
                                                                                                            #endif // conditional
                                                                                                            return output;
                                                                                                        }

                                                                                                        // Unpacked Type: VaryingsMeshToDS
                                                                                                        VaryingsMeshToDS UnpackVaryingsMeshToDS(PackedVaryingsMeshToDS input)
                                                                                                        {
                                                                                                            VaryingsMeshToDS output;
                                                                                                            output.positionRWS = input.interp00.xyz;
                                                                                                            output.normalWS = input.interp01.xyz;
                                                                                                            #if UNITY_ANY_INSTANCING_ENABLED
                                                                                                            output.instanceID = input.instanceID;
                                                                                                            #endif // conditional
                                                                                                            return output;
                                                                                                        }
                                                                                                        //-------------------------------------------------------------------------------------
                                                                                                        // End Interpolator Packing And Struct Declarations
                                                                                                        //-------------------------------------------------------------------------------------

                                                                                                        //-------------------------------------------------------------------------------------
                                                                                                        // Graph generated code
                                                                                                        //-------------------------------------------------------------------------------------
                                                                                                                // Shared Graph Properties (uniform inputs)
                                                                                                                CBUFFER_START(UnityPerMaterial)
                                                                                                                float _Alpha;
                                                                                                                CBUFFER_END
                                                                                                                TEXTURE2D(_MainTex); SAMPLER(sampler_MainTex); float4 _MainTex_TexelSize;
                                                                                                                TEXTURE2D(Mask); SAMPLER(samplerMask); float4 Mask_TexelSize;
                                                                                                                SAMPLER(_SampleTexture2D_29C4BEA2_Sampler_3_Linear_Repeat);
                                                                                                                SAMPLER(_SampleTexture2D_2D75C97F_Sampler_3_Linear_Repeat);

                                                                                                                // Pixel Graph Inputs
                                                                                                                    struct SurfaceDescriptionInputs
                                                                                                                    {
                                                                                                                        float4 uv0; // optional
                                                                                                                    };
                                                                                                                    // Pixel Graph Outputs
                                                                                                                        struct SurfaceDescription
                                                                                                                        {
                                                                                                                            float3 Color;
                                                                                                                            float Alpha;
                                                                                                                            float AlphaClipThreshold;
                                                                                                                        };

                                                                                                                        // Shared Graph Node Functions

                                                                                                                            void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
                                                                                                                            {
                                                                                                                                Out = A * B;
                                                                                                                            }

                                                                                                                            void Unity_Power_float4(float4 A, float4 B, out float4 Out)
                                                                                                                            {
                                                                                                                                Out = pow(A, B);
                                                                                                                            }

                                                                                                                            void Unity_Subtract_float4(float4 A, float4 B, out float4 Out)
                                                                                                                            {
                                                                                                                                Out = A - B;
                                                                                                                            }

                                                                                                                            void Unity_Clamp_float4(float4 In, float4 Min, float4 Max, out float4 Out)
                                                                                                                            {
                                                                                                                                Out = clamp(In, Min, Max);
                                                                                                                            }

                                                                                                                            // Pixel Graph Evaluation
                                                                                                                                SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
                                                                                                                                {
                                                                                                                                    SurfaceDescription surface = (SurfaceDescription)0;
                                                                                                                                    float4 _SampleTexture2D_29C4BEA2_RGBA_0 = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, IN.uv0.xy);
                                                                                                                                    float _SampleTexture2D_29C4BEA2_R_4 = _SampleTexture2D_29C4BEA2_RGBA_0.r;
                                                                                                                                    float _SampleTexture2D_29C4BEA2_G_5 = _SampleTexture2D_29C4BEA2_RGBA_0.g;
                                                                                                                                    float _SampleTexture2D_29C4BEA2_B_6 = _SampleTexture2D_29C4BEA2_RGBA_0.b;
                                                                                                                                    float _SampleTexture2D_29C4BEA2_A_7 = _SampleTexture2D_29C4BEA2_RGBA_0.a;
                                                                                                                                    float4 _Multiply_3FD8CB6E_Out_2;
                                                                                                                                    Unity_Multiply_float(_SampleTexture2D_29C4BEA2_RGBA_0, float4(2, 2, 2, 2), _Multiply_3FD8CB6E_Out_2);
                                                                                                                                    float4 _SampleTexture2D_2D75C97F_RGBA_0 = SAMPLE_TEXTURE2D(Mask, samplerMask, IN.uv0.xy);
                                                                                                                                    float _SampleTexture2D_2D75C97F_R_4 = _SampleTexture2D_2D75C97F_RGBA_0.r;
                                                                                                                                    float _SampleTexture2D_2D75C97F_G_5 = _SampleTexture2D_2D75C97F_RGBA_0.g;
                                                                                                                                    float _SampleTexture2D_2D75C97F_B_6 = _SampleTexture2D_2D75C97F_RGBA_0.b;
                                                                                                                                    float _SampleTexture2D_2D75C97F_A_7 = _SampleTexture2D_2D75C97F_RGBA_0.a;
                                                                                                                                    float _Vector1_3D656997_Out_0 = 0.8;
                                                                                                                                    float4 _Power_4DAD70D9_Out_2;
                                                                                                                                    Unity_Power_float4(_SampleTexture2D_2D75C97F_RGBA_0, (_Vector1_3D656997_Out_0.xxxx), _Power_4DAD70D9_Out_2);
                                                                                                                                    float _Property_6BBE9849_Out_0 = _Alpha;
                                                                                                                                    float4 _Subtract_57CD835A_Out_2;
                                                                                                                                    Unity_Subtract_float4(_Power_4DAD70D9_Out_2, (_Property_6BBE9849_Out_0.xxxx), _Subtract_57CD835A_Out_2);
                                                                                                                                    float4 _Clamp_CF02927_Out_3;
                                                                                                                                    Unity_Clamp_float4(_Subtract_57CD835A_Out_2, float4(0, 0, 0, 0), float4(1, 1, 1, 1), _Clamp_CF02927_Out_3);
                                                                                                                                    float _Vector1_9997196B_Out_0 = 0;
                                                                                                                                    surface.Color = (_Multiply_3FD8CB6E_Out_2.xyz);
                                                                                                                                    surface.Alpha = (_Clamp_CF02927_Out_3).x;
                                                                                                                                    surface.AlphaClipThreshold = _Vector1_9997196B_Out_0;
                                                                                                                                    return surface;
                                                                                                                                }

                                                                                                                                //-------------------------------------------------------------------------------------
                                                                                                                                // End graph generated code
                                                                                                                                //-------------------------------------------------------------------------------------

                                                                                                                            // $include("VertexAnimation.template.hlsl")

                                                                                                                            //-------------------------------------------------------------------------------------
                                                                                                                                // TEMPLATE INCLUDE : SharedCode.template.hlsl
                                                                                                                                //-------------------------------------------------------------------------------------

                                                                                                                                    FragInputs BuildFragInputs(VaryingsMeshToPS input)
                                                                                                                                    {
                                                                                                                                        FragInputs output;
                                                                                                                                        ZERO_INITIALIZE(FragInputs, output);

                                                                                                                                        // Init to some default value to make the computer quiet (else it output 'divide by zero' warning even if value is not used).
                                                                                                                                        // TODO: this is a really poor workaround, but the variable is used in a bunch of places
                                                                                                                                        // to compute normals which are then passed on elsewhere to compute other values...
                                                                                                                                        output.tangentToWorld = k_identity3x3;
                                                                                                                                        output.positionSS = input.positionCS;       // input.positionCS is SV_Position

                                                                                                                                        // output.positionRWS = input.positionRWS;
                                                                                                                                        // output.tangentToWorld = BuildTangentToWorld(input.tangentWS, input.normalWS);
                                                                                                                                        output.texCoord0 = input.texCoord0;
                                                                                                                                        // output.texCoord1 = input.texCoord1;
                                                                                                                                        // output.texCoord2 = input.texCoord2;
                                                                                                                                        // output.texCoord3 = input.texCoord3;
                                                                                                                                        // output.color = input.color;
                                                                                                                                        #if _DOUBLESIDED_ON && SHADER_STAGE_FRAGMENT
                                                                                                                                        output.isFrontFace = IS_FRONT_VFACE(input.cullFace, true, false);
                                                                                                                                        #elif SHADER_STAGE_FRAGMENT
                                                                                                                                        // output.isFrontFace = IS_FRONT_VFACE(input.cullFace, true, false);
                                                                                                                                        #endif // SHADER_STAGE_FRAGMENT

                                                                                                                                        return output;
                                                                                                                                    }

                                                                                                                                    SurfaceDescriptionInputs FragInputsToSurfaceDescriptionInputs(FragInputs input, float3 viewWS)
                                                                                                                                    {
                                                                                                                                        SurfaceDescriptionInputs output;
                                                                                                                                        ZERO_INITIALIZE(SurfaceDescriptionInputs, output);

                                                                                                                                        // output.WorldSpaceNormal =            normalize(input.tangentToWorld[2].xyz);
                                                                                                                                        // output.ObjectSpaceNormal =           mul(output.WorldSpaceNormal, (float3x3) UNITY_MATRIX_M);           // transposed multiplication by inverse matrix to handle normal scale
                                                                                                                                        // output.ViewSpaceNormal =             mul(output.WorldSpaceNormal, (float3x3) UNITY_MATRIX_I_V);         // transposed multiplication by inverse matrix to handle normal scale
                                                                                                                                        // output.TangentSpaceNormal =          float3(0.0f, 0.0f, 1.0f);
                                                                                                                                        // output.WorldSpaceTangent =           input.tangentToWorld[0].xyz;
                                                                                                                                        // output.ObjectSpaceTangent =          TransformWorldToObjectDir(output.WorldSpaceTangent);
                                                                                                                                        // output.ViewSpaceTangent =            TransformWorldToViewDir(output.WorldSpaceTangent);
                                                                                                                                        // output.TangentSpaceTangent =         float3(1.0f, 0.0f, 0.0f);
                                                                                                                                        // output.WorldSpaceBiTangent =         input.tangentToWorld[1].xyz;
                                                                                                                                        // output.ObjectSpaceBiTangent =        TransformWorldToObjectDir(output.WorldSpaceBiTangent);
                                                                                                                                        // output.ViewSpaceBiTangent =          TransformWorldToViewDir(output.WorldSpaceBiTangent);
                                                                                                                                        // output.TangentSpaceBiTangent =       float3(0.0f, 1.0f, 0.0f);
                                                                                                                                        // output.WorldSpaceViewDirection =     normalize(viewWS);
                                                                                                                                        // output.ObjectSpaceViewDirection =    TransformWorldToObjectDir(output.WorldSpaceViewDirection);
                                                                                                                                        // output.ViewSpaceViewDirection =      TransformWorldToViewDir(output.WorldSpaceViewDirection);
                                                                                                                                        // float3x3 tangentSpaceTransform =     float3x3(output.WorldSpaceTangent,output.WorldSpaceBiTangent,output.WorldSpaceNormal);
                                                                                                                                        // output.TangentSpaceViewDirection =   mul(tangentSpaceTransform, output.WorldSpaceViewDirection);
                                                                                                                                        // output.WorldSpacePosition =          input.positionRWS;
                                                                                                                                        // output.ObjectSpacePosition =         TransformWorldToObject(input.positionRWS);
                                                                                                                                        // output.ViewSpacePosition =           TransformWorldToView(input.positionRWS);
                                                                                                                                        // output.TangentSpacePosition =        float3(0.0f, 0.0f, 0.0f);
                                                                                                                                        // output.AbsoluteWorldSpacePosition =  GetAbsolutePositionWS(input.positionRWS);
                                                                                                                                        // output.ScreenPosition =              ComputeScreenPos(TransformWorldToHClip(input.positionRWS), _ProjectionParams.x);
                                                                                                                                        output.uv0 = input.texCoord0;
                                                                                                                                        // output.uv1 =                         input.texCoord1;
                                                                                                                                        // output.uv2 =                         input.texCoord2;
                                                                                                                                        // output.uv3 =                         input.texCoord3;
                                                                                                                                        // output.VertexColor =                 input.color;
                                                                                                                                        // output.FaceSign =                    input.isFrontFace;
                                                                                                                                        // output.TimeParameters =              _TimeParameters.xyz; // This is mainly for LW as HD overwrite this value

                                                                                                                                        return output;
                                                                                                                                    }

                                                                                                                                    // existing HDRP code uses the combined function to go directly from packed to frag inputs
                                                                                                                                    FragInputs UnpackVaryingsMeshToFragInputs(PackedVaryingsMeshToPS input)
                                                                                                                                    {
                                                                                                                                        UNITY_SETUP_INSTANCE_ID(input);
                                                                                                                                        VaryingsMeshToPS unpacked = UnpackVaryingsMeshToPS(input);
                                                                                                                                        return BuildFragInputs(unpacked);
                                                                                                                                    }

                                                                                                                                    //-------------------------------------------------------------------------------------
                                                                                                                                    // END TEMPLATE INCLUDE : SharedCode.template.hlsl
                                                                                                                                    //-------------------------------------------------------------------------------------



                                                                                                                                    void BuildSurfaceData(FragInputs fragInputs, inout SurfaceDescription surfaceDescription, float3 V, PositionInputs posInput, out SurfaceData surfaceData)
                                                                                                                                    {
                                                                                                                                        // setup defaults -- these are used if the graph doesn't output a value
                                                                                                                                        ZERO_INITIALIZE(SurfaceData, surfaceData);

                                                                                                                                        // copy across graph values, if defined
                                                                                                                                        surfaceData.color = surfaceDescription.Color;

                                                                                                                                #if defined(DEBUG_DISPLAY)
                                                                                                                                        if (_DebugMipMapMode != DEBUGMIPMAPMODE_NONE)
                                                                                                                                        {
                                                                                                                                            // TODO
                                                                                                                                        }
                                                                                                                                #endif
                                                                                                                                    }

                                                                                                                                    void GetSurfaceAndBuiltinData(FragInputs fragInputs, float3 V, inout PositionInputs posInput, out SurfaceData surfaceData, out BuiltinData builtinData)
                                                                                                                                    {
                                                                                                                                        SurfaceDescriptionInputs surfaceDescriptionInputs = FragInputsToSurfaceDescriptionInputs(fragInputs, V);
                                                                                                                                        SurfaceDescription surfaceDescription = SurfaceDescriptionFunction(surfaceDescriptionInputs);

                                                                                                                                        // Perform alpha test very early to save performance (a killed pixel will not sample textures)
                                                                                                                                        // TODO: split graph evaluation to grab just alpha dependencies first? tricky..
                                                                                                                                        DoAlphaTest(surfaceDescription.Alpha, surfaceDescription.AlphaClipThreshold);

                                                                                                                                        BuildSurfaceData(fragInputs, surfaceDescription, V, posInput, surfaceData);

                                                                                                                                        // Builtin Data
                                                                                                                                        ZERO_INITIALIZE(BuiltinData, builtinData); // No call to InitBuiltinData as we don't have any lighting
                                                                                                                                        builtinData.opacity = surfaceDescription.Alpha;
                                                                                                                                    }

                                                                                                                                    //-------------------------------------------------------------------------------------
                                                                                                                                    // Pass Includes
                                                                                                                                    //-------------------------------------------------------------------------------------
                                                                                                                                        #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/ShaderPassForwardUnlit.hlsl"
                                                                                                                                    //-------------------------------------------------------------------------------------
                                                                                                                                    // End Pass Includes
                                                                                                                                    //-------------------------------------------------------------------------------------

                                                                                                                                    ENDHLSL
                                                                                                                                }

    }
        CustomEditor "UnityEditor.Rendering.HighDefinition.UnlitUI"
                                                                                                                                        FallBack "Hidden/Shader Graph/FallbackError"
}
