﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum eSelectorFunction { Language, VSync };

public class Selector : MonoBehaviour
{
    public Button left;
    public Button right;
    public Text textArea;
    public bool autorefresh = true;

    public eSelectorFunction selectorFunction;

    public int maxValue;
    public string[] texts;

    public int index { get; private set; } = 0;

    private void Start()
    {
        if(selectorFunction == eSelectorFunction.VSync) {
            RefreshFromCurrent();
        }
        else {
            Refresh();
        }
    }

    public void Backward()
    {
        if (index > 0)
            --index;
        else
            index = maxValue - 1;
        Refresh();
    }

    public void Forward()
    {
        if (index == maxValue - 1)
            index = 0;
        else
            ++index;
        Refresh();
    }

    private void Refresh()
    {
        switch (selectorFunction)
        {
            case eSelectorFunction.Language:
                LanguageManager.UpdateLanguage((eLanguage)index);
                break;
            case eSelectorFunction.VSync:
                QualitySettings.vSyncCount = index+1;
                textArea.text = texts[index];
                break;
        }
    }
    private void RefreshFromCurrent()
    {
        switch (selectorFunction) {
            case eSelectorFunction.VSync:
                Debug.Log(QualitySettings.vSyncCount);
                index = QualitySettings.vSyncCount-1;
                textArea.text = texts[index];
                break;
        }
    }
}
