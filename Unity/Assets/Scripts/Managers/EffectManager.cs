﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using Cinemachine;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.Rendering;
using UnityEngine.VFX;
using Random = System.Random;

public class EffectManager : MonoSingleton<EffectManager>
{
    public CinemachineVirtualCamera virtualCamera;
    public float startNoiseAmp;
    public float startNoiseFreq;
    private CinemachineBasicMultiChannelPerlin perlinNoise;
    public Image blackScreen;
    
    private void Start()
    {
        perlinNoise = virtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
    }

    public void WallShake(EffectProfile effectProfile)
    {
        float startAmplitudeGain = perlinNoise.m_AmplitudeGain;
        float startFrequencyGain = perlinNoise.m_FrequencyGain;

        Tween ampTween = null, freqTween = null;
        if (effectProfile.additive) {
            ampTween = DOVirtual.Float(startAmplitudeGain, effectProfile.intensity, effectProfile.duration, arg0 => perlinNoise.m_AmplitudeGain = arg0);
            ampTween.SetEase(effectProfile.curve);

            freqTween = DOVirtual.Float(startFrequencyGain, effectProfile.extraParam1, effectProfile.duration, arg0 => perlinNoise.m_FrequencyGain = arg0);
            freqTween.SetEase(effectProfile.curve2);
        }
        else {
            ampTween = DOVirtual.Float(startAmplitudeGain, startNoiseAmp, effectProfile.duration, arg0 => perlinNoise.m_AmplitudeGain = arg0);
            ampTween.SetEase(effectProfile.curve);

            freqTween = DOVirtual.Float(startFrequencyGain, startNoiseFreq, effectProfile.duration, arg0 => perlinNoise.m_FrequencyGain = arg0);
            freqTween.SetEase(effectProfile.curve2);
        }
    }

    public Tween ScreenShake(EffectProfile effectProfile, bool instantStop = true)
    {
        float startAmplitudeGain = perlinNoise.m_AmplitudeGain;
        float startFrequencyGain = perlinNoise.m_FrequencyGain;

        Tween ampTween = null, freqTween = null;
        switch(effectProfile.profileType)
        {
            case eEFFECT_PROFILE_TYPE.Shake:
                if(effectProfile.additive)
                {
                    ampTween = DOVirtual.Float(startAmplitudeGain, effectProfile.intensity, effectProfile.duration, arg0 => perlinNoise.m_AmplitudeGain = arg0);
                    ampTween.SetEase(effectProfile.curve);

                    if (instantStop) {
                        ampTween.onComplete += () => perlinNoise.m_AmplitudeGain = startNoiseAmp;
                    }

                    freqTween = DOVirtual.Float(startFrequencyGain, effectProfile.extraParam1, effectProfile.duration, arg0 => perlinNoise.m_FrequencyGain = arg0);
                    freqTween.SetEase(effectProfile.curve2);

                    if (instantStop)
                        freqTween.SetLoops(2, LoopType.Yoyo);
                }else {
                    ampTween = DOVirtual.Float(startAmplitudeGain, startNoiseAmp, effectProfile.duration, arg0 => perlinNoise.m_AmplitudeGain = arg0);
                    ampTween.SetEase(effectProfile.curve);

                    freqTween = DOVirtual.Float(startFrequencyGain, startNoiseFreq, effectProfile.duration, arg0 => perlinNoise.m_FrequencyGain = arg0);
                    freqTween.SetEase(effectProfile.curve2);
                }
                break;
            case eEFFECT_PROFILE_TYPE.WallShake:
                if(effectProfile.additive)
                {
                    ampTween = DOVirtual.Float(startAmplitudeGain, effectProfile.intensity, effectProfile.duration, arg0 => perlinNoise.m_AmplitudeGain = arg0);
                    ampTween.SetEase(effectProfile.curve);

                    freqTween = DOVirtual.Float(startFrequencyGain, effectProfile.extraParam1, effectProfile.duration, arg0 => perlinNoise.m_FrequencyGain = arg0);
                    freqTween.SetEase(effectProfile.curve2);
                }
                else
                {
                    ampTween = DOVirtual.Float(startAmplitudeGain, startAmplitudeGain - effectProfile.intensity, effectProfile.duration, arg0 => perlinNoise.m_AmplitudeGain = arg0);
                    ampTween.SetEase(effectProfile.curve);

                    freqTween = DOVirtual.Float(startFrequencyGain, startFrequencyGain- effectProfile.extraParam1, effectProfile.duration, arg0 => perlinNoise.m_FrequencyGain = arg0);
                    freqTween.SetEase(effectProfile.curve2);
                }
                break;
        }
        return ampTween;
    }

    private Coroutine cutGlitchCo;
    
    public void CutGlitch(EffectProfile effectProfile)
    {
        cutGlitchCo = StartCoroutine(CutGlitchCo(effectProfile));
    }

    IEnumerator CutGlitchCo(EffectProfile effectProfile)
    {
        float cutGlitchTime = Time.time;
        float percent = (Time.time - cutGlitchTime) / effectProfile.duration;

        while (percent <= 1f)
        {
            bool wasBlack = blackScreen.color == Color.black;
            float randomValue = UnityEngine.Random.Range(0, 1f);
            float curveValue = effectProfile.curve.Evaluate(percent);

            percent = (Time.time - cutGlitchTime) / effectProfile.duration;
            
            if (!wasBlack && randomValue < curveValue)
            {
                blackScreen.color = Color.black;
                SoundManager.instance.CutGlitch();
                yield return new WaitForSeconds(effectProfile.extraParam1 * effectProfile.curve2.Evaluate(1f-percent));// * (1f-curveValue));
            } else {
                blackScreen.color = Color.clear;
                yield return new WaitForSeconds(effectProfile.extraParam1 * effectProfile.curve2.Evaluate(1f - percent));// * effectProfile.extraParam2 * curveValue);
            }
        }

        blackScreen.color = Color.black;

        yield return null;
    }

    public void BlackScreenAlpha(float alpha)
    {
        blackScreen.color = new Color(0,0,0,alpha);
    }
    
    public void UpdateVertigoOut(float percent)
    {
        virtualCamera.transform.localPosition = new Vector3(virtualCamera.transform.localPosition.x, virtualCamera.transform.localPosition.y, -percent);
        virtualCamera.m_Lens.FieldOfView = Mathf.Lerp(60f, 35f, percent);
    }

    public void StopVertigoOut()
    {
        Pawn.controlledPawn.VertigoOut(false);
    }
    
    public void FadeOut(float duration)
    {
        if(cutGlitchCo != null)
            StopCoroutine(cutGlitchCo);
        blackScreen.color = Color.black;
        blackScreen.DOFade(0f, duration);
    }
    
    public Tweener FadeIn(float duration)
    {
        Tweener tweener = blackScreen.DOFade(1f, duration);
        if(cutGlitchCo != null)
            StopCoroutine(cutGlitchCo);
        return tweener;
    }
}
