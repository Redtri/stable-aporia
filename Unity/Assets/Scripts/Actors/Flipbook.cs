﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flipbook : MonoBehaviour
{
    public List<Texture> textures;
    public float spf;
    public Renderer render;
    private int index;

    public void Start()
    {
        StartCoroutine(Flip());
    }

    IEnumerator Flip()
    {
        render.material.SetTexture("_MainTex", textures[index]);
        while (true) {
            yield return new WaitForSeconds(spf);
            index = Random.Range(0,textures.Count);
            render.material.SetTexture("_MainTex", textures[index]);
        }
    }
}
