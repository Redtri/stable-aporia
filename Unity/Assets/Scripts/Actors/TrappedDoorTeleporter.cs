﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrappedDoorTeleporter : MonoBehaviour
{
    //Parameters
    public Vector2 DelayRange;
    public float minDistToPlayer;

    //State
    private bool active;

    //References
    private DoorManager doorManager;
    private Door currentDoor;

    private void Start()
    {
        doorManager = GetComponent<DoorManager>();
    }

    public void Trigger()
    {
        StartCoroutine(Teleporting());
    }

    public void Stop()
    {
        active = false;
    }

    private void OnDisable()
    {
        Stop();
    }

    private IEnumerator Teleporting()
    {
        active = true;
        while(active)
        {
            CleanCurrentDoor();

            currentDoor = PickDoor();
            while (currentDoor.doorState != eDOOR_STATE.CLOSED)
                yield return new WaitForSeconds(1f);

            currentDoor.Lock(true);
            currentDoor.Trap();

            yield return new WaitForSeconds(Random.Range(DelayRange.x, DelayRange.y));
        }
        yield return null;
    }

    private void CleanCurrentDoor()
    {
        if (!currentDoor)
            return;

        currentDoor.UnTrap();
    }

    private Door PickDoor()
    {
        Door pickedDoor = null;
        // Picking extreme value
        float closestMatch = minDistToPlayer * minDistToPlayer;
        
        for(int i = 0; i < doorManager.roomDoors.Count; ++i) {
            Door iDoor = doorManager.roomDoors[i];

            // Avoiding the constant exit door
            if(iDoor.overrideLoop && iDoor.travelMode == eDOOR_TRAVELMODE.SimpleToNextRoom)
                continue;

            float distToCurrentDoor = Vector3.Distance(iDoor.transform.position, Pawn.controlledPawn.transform.position);
            
            if (distToCurrentDoor < closestMatch) {
                closestMatch = distToCurrentDoor;
                pickedDoor = iDoor;
            }
        }
        return pickedDoor;
    }
}
